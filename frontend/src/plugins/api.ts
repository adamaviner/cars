import axios from "axios";
import _Vue from "vue";
import { Auth } from "@/services/auth";

export function ApiPlugin(Vue: typeof _Vue): void {
  Vue.prototype.$api = axios.create({
    baseURL: process.env.VUE_APP_API_URL,
  });

  const cache = new CacheService();
  Vue.prototype.$cache = cache;

  Vue.prototype.$auth = new Auth(Vue.prototype.$api);
  Vue.prototype.$cars = new CarService(Vue.prototype.$api);
  Vue.prototype.$trips = new TripService(Vue.prototype.$api, cache);
  Vue.prototype.$payment = new PaymentService(Vue.prototype.$api);
  Vue.prototype.$text = new TextService(Vue.prototype.$api);
  Vue.prototype.$locations = new LocationService(Vue.prototype.$api);
}

import { AxiosStatic } from "axios";
import { CarService } from "@/services/CarService";
import { TripService } from "@/services/TripService";
import { CacheService } from "@/services/CacheService";
import { PaymentService } from "@/services/PaymentService";
import { TextService } from "@/services/TextService";
import { LocationService } from "@/services/LocationService";

declare module "vue/types/vue" {
  interface Vue {
    $api: AxiosStatic;
    $auth: Auth;
    $cars: CarService;
    $trips: TripService;
    $cache: CacheService;
    $payment: PaymentService;
    $text: TextService;
    $locations: LocationService;
  }
}
