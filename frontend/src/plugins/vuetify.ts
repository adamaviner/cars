import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  breakpoint: {
    //TODO proper breakpoint??
    mobileBreakpoint: "sm", // This is equivalent to a value of 960
  },
  theme: {
    themes: {
      light: {
        primary: "#009688",
        secondary: "#00bcd4",
        accent: "#03a9f4",
        error: "#FF5252",
        warning: "#ffc107",
        info: "#2196f3",
        success: "#8bc34a",
      },
    },
  },
});
// light: {
//   primary: "#ee44aa",
//   secondary: "#424242",
//   accent: "#82B1FF",
//   error: "#FF5252",
//   info: "#2196F3",
//   success: "#4CAF50",
//   warning: "#FFC107"
// }
