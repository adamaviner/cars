import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import { ApiPlugin } from "./plugins/api";
// import * as css from "@/assets/styles/main.scss";

// const isProduction = process.env.NODE_ENV === "production";
Vue.config.productionTip = false;
Vue.use(ApiPlugin);

// const options = {
//   isEnabled: true,
//   logLevel: isProduction ? "error" : "debug",
//   stringifyArguments: false,
//   showLogLevel: true,
//   showMethodName: true,
//   separator: "|",
//   showConsoleColors: true,
// };

new Vue({
  router,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
