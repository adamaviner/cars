import { VinDetails, VinResult } from "@/types/VinDetails";
import { AxiosInstance, AxiosResponse } from "axios";

export class VinService {
  private root = "https://vpic.nhtsa.dot.gov/api/vehicles/DecodeVinValues";
  constructor(private http: AxiosInstance) {}

  getDetails(vin: string): Promise<AxiosResponse<VinResult>> {
    const requestUrl = encodeURI(`${this.root}/${vin}?format=json`);
    // TODO show all results, for now take first or empty
    return this.http.get<VinResult>(requestUrl);
  }
}
