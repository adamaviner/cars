import { Car } from "@/types/CarTypes";
import { Trip } from "@/types/TripTypes";

type Dictionary<T> = { [id: string]: T };
export class CacheService {
  public cars: Dictionary<Car> = {};
  public trips: Dictionary<Trip> = {};

  public car(id: string): Car | undefined {
    return this.cars[id];
  }

  public trip(id: string): Trip | undefined {
    return this.trips[id];
  }

  public cacheCar(car: Car): void {
    this.cars[car.vin] = car;
  }

  public cacheTrip(trip: Trip): void {
    this.trips[trip.id] = trip;
  }
}
