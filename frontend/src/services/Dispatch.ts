type Dictionary<T> = { [id: string]: T };
export class Dispatch {
  public messages: Dictionary<string> = {};

  public get(channel: string): string | undefined {
    return this.messages[channel];
  }

  public clear(channel: string): void {
    this.messages[channel] = "";
  }

  public post(channel: string, msg: string): void {
    this.messages[channel] = msg;
  }
}

export const dispatch = new Dispatch();
