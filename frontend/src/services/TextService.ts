import { CommonTexts, Text } from "@/types/TextTypes";
import { AxiosInstance, AxiosResponse } from "axios";

export class TextService {
  constructor(private http: AxiosInstance) {}

  private commonsPromise?: Promise<AxiosResponse<CommonTexts>> = undefined;

  public getCommons(): Promise<AxiosResponse<CommonTexts>> {
    if (!this.commonsPromise) this.commonsPromise = this.fetchCommons();
    return this.commonsPromise;
  }

  async fetchCommons(): Promise<AxiosResponse<CommonTexts>> {
    const requestUrl = encodeURI(`/texts/commons`);
    const resp = await this.http.get<CommonTexts>(requestUrl);
    const res = Object.assign(new CommonTexts(), resp.data);
    resp.data = res;
    return resp;
  }

  setCommons(texts: CommonTexts): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI(`/texts/commons`);
    return this.http.put<void>(requestUrl, texts);
  }

  get(id: string): Promise<AxiosResponse<string>> {
    const requestUrl = encodeURI(`/texts/get/${id}`);
    return this.http.get<string>(requestUrl);
  }

  set(text: Text): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI(`/texts/set`);
    return this.http.put<void>(requestUrl, text);
  }
}
