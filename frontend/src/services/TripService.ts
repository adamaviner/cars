import { DateRange, MockAxiosResponse, progressFormat } from "@/helpers/Utils";
import { Insurance } from "@/types/CarTypes";
import { ChargeResponse } from "@/types/paymentTypes";
import {
  Incidental,
  MaintenanceTrip,
  Trip,
  TripCalculateRequest,
  TripDates,
  TripInspection,
  TripInspectionType,
  TripPrice,
  TripRequest,
} from "@/types/TripTypes";
import { AxiosInstance, AxiosResponse } from "axios";
import { Auth } from "./auth";
import { CacheService } from "./CacheService";

export class TripService {
  constructor(private http: AxiosInstance, private cache: CacheService) {}

  listAllUsers(
    queryString = "",
    offset = 0,
    limit = 10000
  ): Promise<AxiosResponse<Trip[]>> {
    const requestUrl = encodeURI(
      `/trips/admin/list?offset=${offset}&limit=${limit}${queryString}`
    );
    return this.http.get<Trip[]>(requestUrl).then((resp) => {
      const res = resp.data.map((trip) => Object.assign(new Trip(), trip));
      resp.data = res;
      return resp;
    });
  }

  userTrips(past = false): Promise<AxiosResponse<Trip[]>> {
    const requestUrl = encodeURI(`/trips/list/${past ? "past" : "future"}`);
    return this.http.get<Trip[]>(requestUrl);
  }

  async getTripsToInspect(
    inspectionType: TripInspectionType
  ): Promise<AxiosResponse<Trip[]>> {
    const requestUrl = encodeURI(`/trips/getTripsTo${inspectionType}Inspect`);
    const resp = await this.http.get<Trip[]>(requestUrl);
    resp.data = resp.data.map((d) => Object.assign(new Trip(), d));
    return resp;
  }

  async submitTripInspection(
    inspection: TripInspection,
    images: File[]
  ): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI("/trips/inspection");
    await this.http.post(requestUrl, inspection);
    return await this.uploadInspectionImages(
      inspection.tripID,
      inspection.inspectionType,
      images
    );
  }

  uploadInspectionImages(
    tripID: string,
    inspectionType: TripInspectionType,
    images: File[]
  ): Promise<AxiosResponse<void>> {
    const formData = new FormData();
    images.forEach((image) => formData.append("files", image));
    const requestUrl = encodeURI(
      `/trips/inspection/upload/${inspectionType}/${tripID}`
    );
    return this.http.post(requestUrl, formData, {
      headers: { "Content-Type": "multipart/form-data" },
    });
  }

  getInspection(
    tripID: string,
    inspectionType: TripInspectionType
  ): Promise<AxiosResponse<TripInspection>> {
    const requestUrl = encodeURI(
      `/trips/inspection/${inspectionType}/${tripID}`
    );
    return this.http.get<TripInspection>(requestUrl).then((resp) => {
      resp.data = Object.assign(new TripInspection(), resp.data);
      resp.data.incidentals = resp.data.incidentals.map((_) =>
        Object.assign(new Incidental(), _)
      );
      return resp;
    });
  }

  uploadInspectionPhoto(
    tripID: string,
    postTrip = false,
    imgData: Blob,
    onUploadProgress: (percent: number) => void
  ): Promise<AxiosResponse<unknown>> {
    const formData = new FormData();
    formData.set("file", imgData, "img.png");
    const preOrPost = postTrip ? "post" : "pre";
    const requestUrl = encodeURI(`/trip/inspection/${preOrPost}/${tripID}`);
    return this.http.post(requestUrl, formData, {
      onUploadProgress: progressFormat(onUploadProgress),
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  }

  async get(tripID: string, forceUpdate = false): Promise<AxiosResponse<Trip>> {
    if (!forceUpdate) {
      const cachedTrip = this.cache.trip(tripID);
      if (cachedTrip) {
        console.log("get: from cache: " + cachedTrip);
        return new MockAxiosResponse<Trip>(cachedTrip);
      }
    }

    const requestUrl = encodeURI("/trips/get/" + tripID);
    const resp = await this.http.get<Trip>(requestUrl);
    resp.data = Object.assign(new Trip(), resp.data);
    this.cache.cacheTrip(resp.data);
    return resp;
  }

  async add(trip: Trip): Promise<AxiosResponse<Trip>> {
    const requestUrl = encodeURI("/trips/add");
    const resp = await this.http.post<Trip>(requestUrl, trip);
    resp.data = Object.assign(new Trip(), resp.data);
    return resp;
  }

  delete(id: string): Promise<AxiosResponse<unknown>> {
    const requestUrl = encodeURI("/trips/delete/" + id);
    return this.http.delete(requestUrl);
  }

  cancel(id: string): Promise<AxiosResponse<unknown>> {
    const requestUrl = encodeURI("/trips/cancel/" + id);
    return this.http.delete(requestUrl);
  }

  async update(trip: Trip): Promise<AxiosResponse<Trip>> {
    const requestUrl = encodeURI("/trips/update");
    const resp = await this.http.put<Trip>(requestUrl, trip);
    resp.data = Object.assign(new Trip(), resp.data);
    return resp;
  }

  async request(request: TripRequest): Promise<AxiosResponse<Trip>> {
    const requestUrl = encodeURI("/trips/request");
    const resp = await this.http.post(requestUrl, request);
    resp.data = Object.assign(new Trip(), resp.data);
    return resp;
  }

  async updateRequest(
    request: TripRequest
  ): Promise<AxiosResponse<ChargeResponse>> {
    const requestUrl = encodeURI("/trips/request/update");
    const resp = await this.http.put<ChargeResponse>(requestUrl, request);
    return resp;
  }

  async scheduleMaintenance(
    req: MaintenanceTrip
  ): Promise<AxiosResponse<Trip>> {
    const requestUrl = encodeURI("/trips/maintenance");
    const resp = await this.http.post<Trip>(requestUrl, req);
    resp.data = Object.assign(new Trip(), resp.data);
    return resp;
  }

  async updateMaintenance(
    request: MaintenanceTrip
  ): Promise<AxiosResponse<Trip>> {
    const requestUrl = encodeURI("/trips/maintenance");
    const resp = await this.http.put<Trip>(requestUrl, request);
    resp.data = Object.assign(new Trip(), resp.data);
    return resp;
  }

  async allTrips(): Promise<AxiosResponse<Trip[]>> {
    const requestUrl = encodeURI("/trips/allTrips");
    const resp = await this.http.get<Trip[]>(requestUrl);
    resp.data = resp.data.map((trip) => Object.assign(new Trip(), trip));
    return resp;
  }

  async allTripsCSV(): Promise<AxiosResponse<string>> {
    const requestUrl = encodeURI("/trips/allTripsCSV");
    const resp = await this.http.get<string>(requestUrl);
    return resp;
  }

  async allInspections(): Promise<AxiosResponse<TripInspection[]>> {
    const requestUrl = encodeURI("/trips/allInspections");
    const resp = await this.http.get<TripInspection[]>(requestUrl);
    resp.data = resp.data.map((inspection) =>
      Object.assign(new TripInspection(), inspection)
    );
    return resp;
  }

  async allInspectionsCSV(): Promise<AxiosResponse<string>> {
    const requestUrl = encodeURI("/trips/allInspectionsCSV");
    const resp = await this.http.get<string>(requestUrl);
    return resp;
  }

  calculateTrip(
    vin: string,
    dates: DateRange,
    tripID?: string,
    insurance?: Insurance
  ): Promise<AxiosResponse<TripPrice>> {
    const req = new TripCalculateRequest(
      dates.startDate.unix(),
      dates.endDate.unix(),
      tripID ? tripID : "",
      insurance ? insurance : Insurance.none
    );
    const endpoint = Auth.isLoggedIn() ? "calculateFull" : "calculate";
    const requestUrl = encodeURI(`/trips/${endpoint}/${vin}`);
    return this.http.put(requestUrl, req);
  }

  booked(vin: string): Promise<AxiosResponse<TripDates[]>> {
    const requestUrl = encodeURI(`/trips/booked/${vin}`);
    return this.http.get<TripDates[]>(requestUrl);
  }
}
