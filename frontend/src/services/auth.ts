import {
  LicenseVerification,
  LoginRequest,
  SignupRequest,
} from "@/types/authTypes";
import { Driver, Role, User } from "@/types/UserTypes";
import { AxiosInstance, AxiosResponse } from "axios";
import { bus } from "@/EventBus";

export class Auth {
  private onFinishLoginUrl = "/";
  private loginCallbacks: ((_: boolean) => void)[] = [];
  private userChangeCallbacks: ((_: User) => void)[] = [];
  private _user?: User;

  constructor(private http: AxiosInstance) {
    if (this.isAuthenticated()) {
      try {
        this._user = Object.assign(new User(), JSON.parse(localStorage.user));
      } catch (e) {
        console.error(e);
        this.fetchUserSelf();
      }

      this.attachCredentialsToHttp(localStorage.token);
      this.clearExpiredCredentials();
    }
  }

  private clearExpiredCredentials(): void {
    if (!this.isAuthenticated) return;
    this.fetchUserSelf().catch((err) => {
      if (err.response?.status === 401) {
        this.logout();
        window.location.reload();
      }
    });
  }

  get user(): User | undefined {
    return this._user;
  }

  attachCredentialsToHttp(token: string): void {
    console.log("attach");
    this.http.defaults.headers.common["Authorization"] = token;
  }

  isAuthenticated(): boolean {
    return !(localStorage.token == null);
  }

  get role(): Role {
    return localStorage.role;
  }

  isAuthorized(role: Role): boolean {
    return Auth.isAuthorized(role);
  }

  static isAuthorized(role: Role): boolean {
    if (!localStorage.role) return false;
    const userRole: Role = localStorage.role;
    if (role == Role.Admin) return userRole == Role.Admin;
    if (role == Role.Custodian)
      return userRole == Role.Admin || userRole == Role.Custodian;
    if (role == Role.User)
      return (
        userRole == Role.Admin ||
        userRole == Role.Custodian ||
        userRole == Role.User
      );
    return userRole == role;
  }

  static isLoggedIn(): boolean {
    if (!localStorage.user) return false;
    return true;
  }

  static isApproved(): boolean {
    if (!localStorage.user) return false;
    try {
      const user: User = Object.assign(
        new User(),
        JSON.parse(localStorage.user)
      );
      return user.approved;
    } catch (err) {
      console.error("isApproved: " + err);
    }
    return false;
  }

  attemptLogin(loginRequest: LoginRequest): Promise<AxiosResponse<User>> {
    const requestUrl = encodeURI("/users/login");
    const request = this.http.post<User>(requestUrl, loginRequest);
    request
      .then((response) => {
        this.loggedIn(response);
      })
      .catch((err) => {
        console.log("err: " + err);
      });
    return request;
  }

  attemptSignup(signupRequest: SignupRequest): Promise<AxiosResponse<User>> {
    const requestUrl = encodeURI("/users/signup");
    const request = this.http.post<User>(requestUrl, signupRequest);
    request
      .then((response) => this.loggedIn(response))
      .catch((err) => console.log("err: " + err));
    return request;
  }

  requestPasswordReset(email: string): Promise<AxiosResponse<unknown>> {
    const requestUrl = encodeURI("/users/password/reset/" + email);
    return this.http.post(requestUrl);
  }

  consumePasswordReset(
    token: string,
    password: string
  ): Promise<AxiosResponse<unknown>> {
    const requestUrl = encodeURI("/users/password/reset/consume");
    return this.http.put(requestUrl, { token, password });
  }

  loggedIn(response: AxiosResponse<User>): User {
    //TODO type
    localStorage.token = response.headers.authorization;
    const user = this.saveLocalUser(response);
    this.attachCredentialsToHttp(localStorage.token);
    return user;
  }

  private saveLocalUser(response: AxiosResponse<User>): User {
    const user = response.data;
    this._user = Object.assign(new User(), user);
    localStorage.role = user.role;
    localStorage.user = JSON.stringify(user);
    this.userChanged(user);
    return user;
  }

  private userChanged(user?: User): void {
    if (user) this.userChangeCallbacks.forEach((cb) => cb(user));

    this.loginCallbacks.forEach((cb) => cb(user ? true : false));
  }

  addLoginCallback(callback: (_: boolean) => void): void {
    if (this.isAuthenticated()) {
      // if already logged in, just fire callback now
      callback(true);
    }
    this.loginCallbacks.push(callback);
  }

  listenForUser(callback: (_: User) => void): void {
    this.userChangeCallbacks.push(callback);
    if (this._user) callback(this._user);
  }

  logout(): void {
    localStorage.removeItem("token");
    localStorage.removeItem("role");
    localStorage.removeItem("avatarUrl");
    localStorage.removeItem("user");
    bus.$emit("logout");
    this.userChanged(undefined);
    // this.http
    //   .post(encodeURI("/users/signout"))
    //   .catch((err) => console.error("signout err: " + err));
  }

  userToken(): string {
    return localStorage.token;
  }

  //TODO move to user service?
  private progressFormat(cb: (_: number) => void) {
    return (e: ProgressEvent) => cb(Math.round((e.loaded * 100) / e.total));
  }

  uploadLicense(
    imgData: Blob,
    onUploadProgress: (percent: number) => void
  ): Promise<AxiosResponse<User[]>> {
    const formData = new FormData();
    formData.set("file", imgData, "license.png");

    const requestUrl = encodeURI("/users/license");
    return this.http.post<User[]>(requestUrl, formData, {
      onUploadProgress: this.progressFormat(onUploadProgress),
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  }

  streamLicense(
    userID: string,
    onProgress: (percent: number) => void
  ): Promise<AxiosResponse<unknown>> {
    const requestUrl = encodeURI("/users/admin/license/" + userID);
    return this.http.get(requestUrl, {
      onDownloadProgress: this.progressFormat(onProgress),
      responseType: "blob",
    });
  }

  async fetchUser(id: string): Promise<AxiosResponse<User>> {
    const requestUrl = encodeURI("/users/fetch/" + id);
    const resp = await this.http.get<User>(requestUrl);
    resp.data = Object.assign(new User(), resp.data);
    return resp;
  }

  fetchUserSelf(): Promise<AxiosResponse<User>> {
    const requestUrl = encodeURI("/users/me");
    const resp = this.http.get<User>(requestUrl);
    resp.then((resp) => this.saveLocalUser(resp));
    return resp;
  }

  updateSelf(update: User): Promise<AxiosResponse<User>> {
    const requestUrl = encodeURI("/users/update");
    const resp = this.http.put<User>(requestUrl, update);
    resp.then((resp) => this.saveLocalUser(resp));
    return resp;
  }

  updatePhone(phone: string): Promise<AxiosResponse<User>> {
    const requestUrl = encodeURI("/users/phone/" + phone);
    const resp = this.http.put<User>(requestUrl);
    resp.then((resp) => this.saveLocalUser(resp));
    return resp;
  }

  async listUsers(): Promise<AxiosResponse<User[]>> {
    const requestUrl = encodeURI("/users/list");
    const resp = await this.http.get<User[]>(requestUrl);
    resp.data = resp.data.map((ls) => Object.assign(new User(), ls));
    return resp;
  }

  deleteUser(): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI("/users/delete");
    return this.http.delete(requestUrl);
  }

  deleteUsers(ids: string[]): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI("/users/admin/delete");
    return this.http.post(requestUrl, ids);
  }

  adminUpdateUser(user: User): Promise<AxiosResponse<User>> {
    const requestUrl = encodeURI("/users/admin/update");
    return this.http.put<User>(requestUrl, user);
  }

  sendEmailVerification(): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI("/users/email/sendVerification");
    return this.http.get(requestUrl);
  }

  verifyMailToken(token: string): Promise<AxiosResponse<string>> {
    const requestUrl = encodeURI("/users/email/verify/" + token);
    return this.http.put<string>(requestUrl);
  }

  sendSmsToken(): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI("/users/sms/sendVerification");
    return this.http.post(requestUrl);
  }

  verifySmsToken(token: string): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI("/users/sms/verification/consume/" + token);
    return this.http.post(requestUrl);
  }

  verifyLicense(request: Driver): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI("/users/license/verify");
    return this.http.post(requestUrl, request);
  }

  licenseVerificationSession(): Promise<AxiosResponse<string>> {
    const requestUrl = encodeURI("/users/license/verificationSession");
    return this.http.post(requestUrl);
  }

  getLicenseVerification(): Promise<AxiosResponse<LicenseVerification>> {
    const requestUrl = encodeURI("/users/license/verificationSession");
    return this.http.get<LicenseVerification>(requestUrl);
  }
}
