import { Car, CarListing } from "@/types/CarTypes";
import { CarTrip, Trip } from "@/types/TripTypes";
import { AxiosInstance, AxiosResponse } from "axios";

export class CarService {
  constructor(private http: AxiosInstance) {}

  async list(
    queryString = "",
    offset = 0,
    limit = 10000
  ): Promise<AxiosResponse<CarListing[]>> {
    const requestUrl = encodeURI(
      `/cars/listAll?offset=${offset}&limit=${limit}${queryString}`
    );
    const resp = await this.http.get<CarListing[]>(requestUrl);
    resp.data = resp.data.map((ls) => Object.assign(new CarListing(), ls));
    return resp;
  }

  async listDisabled(): Promise<AxiosResponse<CarListing[]>> {
    const requestUrl = encodeURI(`/cars/list/disabled`);
    const resp = await this.http.get<CarListing[]>(requestUrl);
    resp.data = resp.data.map((ls) =>
      Object.assign(new CarListing(), ls, { disabled: true })
    );

    return resp;
  }

  async listAvailable(
    params = {},
    offset = 0,
    limit = 10000
  ): Promise<AxiosResponse<CarListing[]>> {
    const requestUrl = encodeURI(`/cars/list?offset=${offset}&limit=${limit}`);
    const resp = await this.http.put<CarListing[]>(requestUrl, params);
    resp.data = resp.data.map((ls) => Object.assign(new CarListing(), ls));
    return resp;
  }

  async getDetails(vin: string): Promise<AxiosResponse<Car>> {
    const requestUrl = encodeURI("/cars/get/" + vin);
    const resp = await this.http.get<Car>(requestUrl);
    const res = Object.assign(new Car(), resp.data);
    resp.data = res;
    return resp;
  }

  async getListing(vin: string): Promise<AxiosResponse<CarListing>> {
    const requestUrl = encodeURI("/cars/listing/get/" + vin);
    const resp = await this.http.get<CarListing>(requestUrl);
    resp.data = Object.assign(new CarListing(), resp.data);
    return resp;
  }

  async getListings(vins: string[]): Promise<AxiosResponse<CarListing[]>> {
    const query = vins.reduce((total, id) => total + "&id=" + id, "");
    const requestUrl = encodeURI("/cars/listings?null" + query);
    const resp = await this.http.get<CarListing[]>(requestUrl);
    resp.data = resp.data.map((ls) => Object.assign(new CarListing(), ls));
    return resp;
  }

  async getListingsForTrips(trips: Trip[]): Promise<CarTrip[]> {
    const vins = trips.map((trip) => trip.vin);
    const uniqueVins = [...new Set(vins)];
    const resp = await this.getListings(uniqueVins);
    const cars = resp.data;
    const carTrips = trips
      .map((trip) => {
        return { trip: trip, car: cars.find((_) => _.vin === trip.vin) };
      })
      .filter((ct) => ct.car)
      .map((ct) => {
        return new CarTrip(ct.trip, ct.car!);
      });
    return carTrips;
  }

  add(car: Car): Promise<AxiosResponse<Car>> {
    const requestUrl = encodeURI("/cars/add");
    return this.http.post(requestUrl, car);
  }

  disable(vin: string): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI("/cars/disable/" + vin);
    return this.http.put(requestUrl);
  }

  update(car: Car): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI("/cars/update");
    return this.http.put(requestUrl, car);
  }

  uploadPhotos(
    vin: string,
    formData: FormData
  ): Promise<AxiosResponse<string[]>> {
    return this.http.post<string[]>("/cars/uploadPhotos/" + vin, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  }

  favorites(): Promise<AxiosResponse<CarListing[]>> {
    const requestUrl = encodeURI(`/cars/favorites`);
    return this.http.get<CarListing[]>(requestUrl);
  }
}
