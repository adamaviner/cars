import {
  CardDetails,
  ChargeRequest,
  ChargeResponse,
  PaymentAction,
} from "@/types/paymentTypes";
import { AxiosInstance, AxiosResponse } from "axios";

export class PaymentService {
  constructor(private http: AxiosInstance) {}

  setup(): Promise<AxiosResponse<string>> {
    const requestUrl = encodeURI(`/payments/setup`);
    return this.http.get<string>(requestUrl);
  }

  async cards(): Promise<AxiosResponse<CardDetails[]>> {
    const requestUrl = encodeURI(`/payments/cards`);
    const resp = await this.http.get<CardDetails[]>(requestUrl);
    resp.data = resp.data.map((ls) => Object.assign(new CardDetails(), ls));
    return resp;
  }

  charge(req: ChargeRequest): Promise<AxiosResponse<ChargeResponse>> {
    const requestUrl = encodeURI(`/payments/charge`);
    return this.http.post<ChargeResponse>(requestUrl, req);
  }

  failed(tripID: string): Promise<AxiosResponse<unknown>> {
    const requestUrl = encodeURI(`/payments/failed/${tripID}`);
    return this.http.post(requestUrl);
  }

  confirmCharge(tripID: string): Promise<AxiosResponse<unknown>> {
    const requestUrl = encodeURI(`/payments/confirm/${tripID}`);
    return this.http.post(requestUrl);
  }

  authorize(tripID: string): Promise<AxiosResponse<PaymentAction[]>> {
    const requestUrl = encodeURI(`/payments/authorize/${tripID}`);
    return this.http.get<PaymentAction[]>(requestUrl);
  }

  removeCard(methodId: string): Promise<AxiosResponse<unknown>> {
    const requestUrl = encodeURI(`/payments/method/remove/${methodId}`);
    return this.http.post(requestUrl);
  }

  chargeIncidental(
    tripID: string,
    amount: number,
    description: string
  ): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI(`/payments/chargeIncidental`);
    const data = { tripID, amount, description };
    return this.http.post(requestUrl, data);
  }
}
