import { Location } from "@/types/LocationTypes";
import { AxiosInstance, AxiosResponse } from "axios";

export class LocationService {
  constructor(private http: AxiosInstance) {}

  add(location: Location): Promise<AxiosResponse<Location>> {
    const requestUrl = encodeURI("/locations/add");
    return this.http.post<Location>(requestUrl, location);
  }

  update(location: Location): Promise<AxiosResponse<Location>> {
    const requestUrl = encodeURI("/locations/update");
    return this.http.put<Location>(requestUrl, location);
  }

  delete(id: string): Promise<AxiosResponse<void>> {
    const requestUrl = encodeURI("/locations/delete/" + id);
    return this.http.delete(requestUrl);
  }

  async get(id: string): Promise<AxiosResponse<Location>> {
    const requestUrl = encodeURI("/locations/get/" + id);
    const resp = await this.http.get<Location>(requestUrl);
    const res = Object.assign(new Location(), resp.data);
    resp.data = res;
    return resp;
  }

  async list(): Promise<AxiosResponse<Location[]>> {
    const requestUrl = encodeURI("/locations/list");
    const resp = await this.http.get<Location[]>(requestUrl);
    resp.data = resp.data.map((ls) => Object.assign(new Location(), ls));
    return resp;
  }
}
