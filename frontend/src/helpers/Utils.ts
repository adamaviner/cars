import { AxiosResponse } from "axios";
import dayjs from "dayjs";
import isBetween from "dayjs/plugin/isBetween";
dayjs.extend(isBetween);
import relativeTime from "dayjs/plugin/relativeTime";
dayjs.extend(relativeTime);

export class MockAxiosResponse<T = unknown> implements AxiosResponse {
  constructor(
    public data: T,
    public status = 200,
    public statusText = "",
    public headers = "",
    public config = {},
    public request = undefined
  ) {}
}

export function progressFormat(cb: (_: number) => void) {
  return (e: ProgressEvent): void => cb(Math.round((e.loaded * 100) / e.total));
}

function extractFileName(url: string, vin: string): string {
  const reg = `(${vin}%2F)(.+?[.]\\w+)`;
  const res = url.match(new RegExp(reg));
  if (res) {
    return res[2];
  }
  return "";
}

export function createImageKitUrl(url: string, vin: string): string {
  const name = extractFileName(url, vin);
  const base = "https://ik.imagekit.io/hkb6txc7g/";
  const transform = "?tr=h-48,w=48,bl-2";
  return base + `${vin}/${name}` + transform;
}

export function arrayEquals(a: number[], b: number[]): boolean {
  if (a.length != b.length) return false;
  for (let i = 0; i < a.length; i++) if (a[i] != b[i]) return false;
  return true;
}

export function formatTimestamp(seconds: number): string {
  const date = new Date(seconds * 1000);
  return new Intl.DateTimeFormat().format(date);
}

export function formatDate(date: Date, day = true, hour = true): string {
  const options: Intl.DateTimeFormatOptions = {};
  if (day) {
    options.month = "short";
    options.day = "numeric";
  }
  if (hour) {
    options.hour = "numeric";
    options.minute = "numeric";
  }
  return new Intl.DateTimeFormat("en-US", options).format(date);
}

export function formatTime(date: Time, day = true, hour = true): string {
  const options: Intl.DateTimeFormatOptions = {};
  if (day) {
    options.month = "short";
    options.day = "numeric";
  }
  if (hour) {
    options.hour = "numeric";
    options.minute = "numeric";
  }
  return new Intl.DateTimeFormat("en-US", options).format(date.toDate());
}

export function dateSetHour(d: Date, hour: number): Date {
  const res = new Date(d);
  res.setHours(hour, 0, 0, 0);
  return res;
}

export function daysBetween(
  startDate: Date,
  endDate: Date,
  float?: boolean
): number {
  const startDateDay = dayjs(startDate);
  const endDateDay = dayjs(endDate);
  const days = endDateDay.diff(startDateDay, "day", true);
  return float ? days : Math.ceil(days);
}

export function isSameDay(d1: Date, d2: Date): boolean {
  return TimeIsSameDay(dayjs(d1), dayjs(d2));
}

export function TimeIsSameDay(d1: Time, d2: Time): boolean {
  const comparisonTemplate = "YYYY-MM-DD";
  return d1.format(comparisonTemplate) === d2.format(comparisonTemplate);
}

export function hoursBetween(startDate: Date, endDate: Date): number {
  const startDateDay = dayjs(startDate);
  const endDateDay = dayjs(endDate);
  return endDateDay.diff(startDateDay, "hour", true);
}

export function secondsBetween(startDate: Date, endDate: Date): number {
  return dayjs(startDate).diff(dayjs(endDate), "second");
}

export function rangeDays(range: DateRange): number {
  return range.endDate.diff(range.startDate, "day");
  // return daysBetween(range.startDate, range.endDate);
}

export function datePlusDays(date: Date, days: number): Date {
  const res = new Date(date);
  res.setDate(res.getDate() + days);
  return res;
}

export type Time = dayjs.Dayjs;

export class DateRange {
  private readonly defaultHours = 10;
  public startDate: Time = this.setHour(dayjs().add(1, "day"));
  public endDate: Time = this.setHour(dayjs().add(8, "day"));

  constructor(start?: number, end?: number) {
    if (start) this.startDate = dayjs.unix(start);
    if (end) this.endDate = dayjs.unix(end);
  }

  public clone(): DateRange {
    const res = new DateRange();
    res.startDate = this.startDate.clone();
    res.endDate = this.endDate.clone();
    return res;
  }

  public isSame(dateRange: DateRange): boolean {
    return (
      this.startDate.isSame(dateRange.startDate) &&
      this.endDate.isSame(dateRange.endDate)
    );
  }

  public contains(d: Time): boolean {
    const date = dayjs(d);
    return date.isBetween(this.startDate, this.endDate);
  }

  private setHour(d: Time, hour = this.defaultHours): Time {
    return d.hour(hour).minute(0).second(0).millisecond(0);
  }

  get asArray(): number[] {
    return [this.startDate.unix(), this.endDate.unix()];
  }

  daysLength(): number {
    return this.endDate.diff(this.startDate, "day", false);
    // return daysBetween(this.startDate, this.endDate);
  }

  tripLength(): number {
    return Math.ceil(this.endDate.diff(this.startDate, "day", true));
  }
}

export type Header = {
  text: string;
  value: string;
  sortable?: boolean;
};
