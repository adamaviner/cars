import { Filters } from "@/types/FilterTypes";
import { Dictionary } from "vue-router/types/router";
import { arrayEquals, DateRange } from "@/helpers/Utils";

export interface QueryMap {
  [key: string]: string;
}

export function stringifyQuery(
  query: Dictionary<string | (string | null)[]>
): string {
  let res = "";
  Object.keys(query).forEach((key) => (res = "&" + query[key]));
  return res;
}

export function mergeQuery(query: QueryMap, current: any) {
  const keys = new Set([...Object.keys(query), ...Object.keys(current)]);
  const merged: { [key: string]: string } = {};
  keys.forEach((key) => {
    const val = query[key] || current[key];
    if (val) merged[key] = val;
  });
  return merged;
}

export class FilterQuery {
  // private readonly _makeDefaultFilters: () => Filters;
  private readonly _defaultFilters: Filters;

  // constructor(makeDefaultFilters: () => Filters) {
  //   this._makeDefaultFilters = makeDefaultFilters;
  //   this._defaultFilters = makeDefaultFilters();
  // }

  constructor(defaultFilters: Filters) {
    this._defaultFilters = defaultFilters;
  }

  filtersFromQuery(query: Dictionary<string | (string | null)[]>) {
    const filters = { ...this._defaultFilters };
    Object.keys(query).forEach((key) => {
      if (key.includes("max")) return; //just use keys with min, to avoid duplicates
      if (key.includes("min")) {
        const { pureKey, arr } = this.parseArrQuery(key, query);
        if (this.isNumberArray(filters[pureKey])) {
          (filters[pureKey] as number[]) = arr;
        }
      } else {
        const { pureKey, val } = this.parseStringQuery(key, query);
        if (typeof filters[pureKey] === "string") {
          (filters[pureKey] as string) = val;
        }
      }
    });
    return filters;
  }

  private parseArrQuery(
    key: string,
    query: Dictionary<string | (string | null)[]>
  ) {
    const pureKey = reverseCapitalize(key.slice(3)) as keyof Filters;
    const rawArr = [
      query["min" + capitalize(pureKey)],
      query["max" + capitalize(pureKey)],
    ];

    const arr = rawArr.map(headOrEmpty).map((_) => parseInt(_));
    return { pureKey, arr };
  }

  private parseStringQuery(
    key: string,
    query: Dictionary<string | (string | null)[]>
  ) {
    const pureKey = key as keyof Filters;
    return { pureKey: pureKey, val: headOrEmpty(query[key]) };
  }

  public queryFromFilters(filters: Filters): QueryMap {
    let query: QueryMap = {};
    Object.keys(filters).forEach(
      (key) => (query = this.parse(query, key as keyof Filters, filters))
    );
    return query;
  }

  public queryFromRange(range: DateRange): QueryMap {
    return {
      minDate: "" + range.startDate.unix(),
      maxDate: "" + range.endDate.unix(),
    };
  }

  private parse(query: QueryMap, key: keyof Filters, filters: Filters) {
    const filter = filters[key];
    if (typeof filter === "string") {
      if (filter == this._defaultFilters[key]) return query;
      query[key] = filter;
      return query;
    } else if (this.isNumberArray(filter)) {
      if (arrayEquals(filter, this._defaultFilters[key] as number[]))
        return query;
      const queryKey = capitalize(key);
      query["min" + queryKey] = "" + filter[0];
      query["max" + queryKey] = "" + filter[1];
      return query;
    }
    return query;
  }

  private isNumberArray(filter: string | number[]): filter is number[] {
    return (filter as number[]).length === 2;
  }
}

function isStringArray(v: string | number[]): v is number[] {
  return (v as number[]) !== undefined;
}

function headOrEmpty(v: string | (string | null)[]) {
  if (typeof v === "string") return v;
  else return v[0] ? v[0] : "";
}

function capitalize(s: string) {
  if (typeof s !== "string") return "";
  return s.charAt(0).toUpperCase() + s.slice(1);
}

function reverseCapitalize(s: string) {
  if (typeof s !== "string") return "";
  return s.charAt(0).toLowerCase() + s.slice(1);
}
