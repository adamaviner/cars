export class Text {
  constructor(public name = "", public value = "") {}
}

export class CommonTexts {
  constructor(
    public protectionFullTooltip = "",
    public protectionPartialTooltip = "",
    public protectionWaiverTooltip = "",
    public chargeAgreement = "",
    public chargeExplanation = "",
    public chargeExplanationNoHold = ""
  ) {}

  public asTexts(): Text[] {
    return Object.keys(this)
      .map((key) => {
        const v = this[key as keyof CommonTexts];
        if (typeof v === "string") return new Text(key, v);
        else return new Text("remove", "");
      })
      .filter((_) => _.name != "remove");
  }
}
