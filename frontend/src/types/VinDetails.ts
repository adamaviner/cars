export interface VinResult {
  Results: VinDetails[];
}

export class VinDetails {
  public Make?: string;
  public Model?: string;
  public ModelYear?: number;
  public Doors?: number;
  public Seats?: number;
}
