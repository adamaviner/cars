import { DateRange } from "@/helpers/Utils";
import dayjs from "dayjs";
import { CarListing, Insurance } from "./CarTypes";
import { ChargeRequest } from "./paymentTypes";
import { User } from "./UserTypes";

export class CarTrip {
  constructor(public trip = new Trip(), public car = new CarListing()) {}
}

export class CarTripUser {
  constructor(
    public car = new CarListing(),
    public trip = new Trip(),
    public user = new User()
  ) {}
}

export type SomeTrip = Trip | TripRequest | MaintenanceTrip;

export class BaseTrip {
  public vin = "";
  public startDate = 0;
  public endDate = 0;
  public canceled = false;
  public insurance = Insurance.standard;

  get tripLength(): number {
    return new DateRange(this.startDate, this.endDate).tripLength();
  }

  get startMoment(): dayjs.Dayjs {
    return dayjs.unix(this.startDate);
  }
  get endMoment(): dayjs.Dayjs {
    return dayjs.unix(this.endDate);
  }

  get startDateFormatted(): string {
    return this.formatDate(this.startDate);
  }

  get endDateFormatted(): string {
    return this.formatDate(this.endDate);
  }

  private formatDate(seconds: number) {
    const date = new Date(seconds * 1000);
    return new Intl.DateTimeFormat().format(date);
  }

  public static isTrip(t: BaseTrip): t is Trip {
    return (t as Trip).wasPaid !== undefined;
  }

  public static isTripRequest(t: BaseTrip): t is TripRequest {
    return (t as TripRequest).userID !== undefined;
  }

  public static isMaintenanceTrip(t: BaseTrip): t is MaintenanceTrip {
    return (t as MaintenanceTrip).indefinite !== undefined;
  }
}

export class Trip extends BaseTrip {
  constructor(
    public id = "",
    public userID = "",
    public price = 0,
    public wasPaid = false,
    public booked = false,
    public timestamp = 0, // public inspection: TripInspection = new TripInspection()
    public postInspected = false
  ) {
    super();
  }

  public updateFee(fee: number): number {
    if (!this.booked) return 0;
    const inOneDay = dayjs().add(1, "day");
    return inOneDay.isBefore(this.startMoment) ? 0 : fee;
  }

  get formattedPrice(): string {
    return "$" + this.price / 100;
  }

  public equals(trip: Trip): boolean {
    // if (this.price !== trip.price) return false;
    if (this.startDate !== trip.startDate) return false;
    if (this.endDate !== trip.endDate) return false;
    if (this.insurance !== trip.insurance) return false;
    return true;
  }
}

export class TripRequest extends BaseTrip {
  constructor(public userID = "", public price = 0) {
    super();
  }
}

export class TripCalculateRequest {
  constructor(
    public startDate = 0,
    public endDate = 0,
    public tripId?: string,
    public insurance?: Insurance
  ) {}
}

export class TripUpdateRequest extends TripRequest {
  public chargeRequest?: ChargeRequest;

  constructor(public id = "", req: Trip, paymentMethodID?: string) {
    super();
    this.userID = req.userID;
    this.vin = req.vin;
    this.startDate = req.startDate;
    this.endDate = req.endDate;
    this.price = req.price;
    this.insurance = req.insurance;
    this.chargeRequest = paymentMethodID
      ? new ChargeRequest(id, paymentMethodID)
      : undefined;
  }
}

export enum TripType {
  Maintanence = "Maintanence",
  Turo = "Turo",
  Manual = "Manual",
  Other = "Other",
}
export class MaintenanceTrip extends BaseTrip {
  public id = "";
  public indefinite = false;
  public tripType = TripType.Manual;
  public manualDescription = "";
}

export class TripPrice {
  constructor(
    public basePrice = 0,
    public discounts: Discount[] = [],
    public deposit: number = 0,
    public discount: number = 0,
    public protection: number = 0,
    public total: number = 0,
    public taxable: number = 0,
    public tax: number = 0
  ) {}
}

export class Discount {
  public title = "";
  public amount = 0;
}

export class TripDates {
  public startDate = 0;
  public endDate = 0;
  public id = "";

  public toDateRange(): DateRange {
    return new DateRange(this.startDate, this.endDate);
  }
}

export enum FuelLevel {
  Full = "Full",
  ThreeQtrs = "ThreeQtrs",
  Half = "Half",
  Qrtr = "Qrtr",
  NearEmpty = "NearEmpty",
  Empty = "Empty",
  Undefined = "Undefined",
}

export enum TripInspectionType {
  Pre = "Pre",
  Post = "Post",
}
export class TripInspection {
  public tripID = "";
  public currentMiles = 0;
  public gasLevel = FuelLevel.Undefined;
  public gasRefill = 0; // in gallons
  public cleaning = false;
  public incidentals: Incidental[] = [];
  public description = "";
  public timestamp = 0;
  public inspectionType = TripInspectionType.Post;
  public photos: string[] = [];
}

export enum IncidentalCause {
  Miles = "Miles",
  Gas = "Gas",
  Clean = "Clean",
  Manual = "Manual",
}
export class Incidental {
  constructor(
    public cause: IncidentalCause = IncidentalCause.Miles,
    public amount = 0,
    public description = "",
    public overage = 0
  ) {}

  get label(): string {
    return `$${this.amount / 100} - ${this.description}`;
  }

  get id(): string {
    return this.label;
  }

  static get maxIncidentalCharge(): number {
    return 30000;
  }
}
