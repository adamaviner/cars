export class DiscountRules {
  public vin = "";
  public daysRules: DiscountDaysRule[] = [];
}

export class DiscountDaysRule {
  public from = 0;
  public to = 0;
  public discount = 0;

  public discountFor(days: number): number {
    if (days >= this.from && days <= this.to) return this.discount;
    return 0;
  }
}
