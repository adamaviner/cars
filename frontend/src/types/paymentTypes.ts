export class CardDetails {
  public id = "";
  public brand = "";
  public last4 = "";
  public expMonth = 0;
  public expYear = 0;
  get label(): string {
    return `${this.brand} •••• •••• •••• ${this.last4}`;
  }
}

export class PaymentAction {
  public secret = "";
  public requiresAuthentication = true;
  public requiresPaymentMethod = false;
}

export class ChargeResponse {
  public chargeTime = 0;
  public paymentAction: PaymentAction | undefined = undefined;
}

export class ChargeRequest {
  constructor(public tripID = "", public paymentMethodID?: string) {}
}
