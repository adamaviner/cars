import dayjs from "dayjs";

export enum Role {
  User = "User",
  Admin = "Admin",
  Custodian = "Custodian",
  Maintainer = "Maintainer",
}

export class User {
  id = "";
  email = "";
  phone = "";
  firstName = "";
  lastName = "";
  birthDate = "";
  licenseURL? = "";
  validLicense = false;
  validPhone = false;
  validEmail = false;
  role = Role.User;
  password?: string; // to update password

  get name(): string {
    return `${this.firstName} ${this.lastName}`;
  }

  get approved(): boolean {
    return this.validEmail && this.validPhone && this.validLicense;
  }

  public youngDriver(): boolean {
    return dayjs().diff(dayjs(this.birthDate), "year") < 24;
  }
}

export function emptyUser(): User {
  return new User();
}

export class Driver {
  phone = "";
  firstName = "";
  middleName = "";
  lastName = "";
  licenseNumber = "";
  expDate = "";
  birthDate = "";
  country = "United States";
  state = "";
}
