export class Rules {
  static notEmptyRule = (v: any): boolean | string => !!v || "Cannot be empty!";

  static notEmptyRuleShort = (v: any): boolean | string => !!v || "Empty!";

  static rangeRule(
    from: number,
    to: number,
    short = false
  ): (v: number) => boolean | string {
    const err = short ? `${from}-${to}` : `Must be between ${from} and ${to}`;
    return (v: number) => (v && v >= from && v <= to) || err;
  }

  static maxRule(max: number) {
    return (v: number): boolean | string => (v && v <= max) || `Max ${max}`;
  }

  static minRule(min: number) {
    return (v: number): boolean | string => (v && v >= min) || `Min ${min}`;
  }

  static RadioSelectedRule() {
    return (v: any): boolean | string =>
      (v && v === null) || "Select an option.";
  }

  static lengthRule(min = 2) {
    return (v: string): boolean | string =>
      (v && v.length >= min) || "Must be more than " + min + " characters";
  }

  static nameRules = [Rules.notEmptyRule, Rules.lengthRule(2)];

  static vinRules = [
    Rules.notEmptyRule,
    (v: string): boolean | string =>
      (v && v.length == 17) || "Must be exactly 17 characters",
  ];

  static passRules = [Rules.notEmptyRule, Rules.lengthRule(8)];

  static maxCarYear = new Date().getFullYear() + 1;
  static yearRules = [
    Rules.notEmptyRule,
    (v: number): boolean | string =>
      (v && v >= 1900 && v <= Rules.maxCarYear) ||
      "Must be between 1900 and " + Rules.maxCarYear,
  ];

  static emailRules = [
    Rules.notEmptyRule,
    (v: string): boolean | string => /.+@.+\..+/.test(v) || "Invalid Email",
  ];

  static phoneRules = [
    Rules.notEmptyRule,
    (v: string): boolean | string =>
      /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(v) ||
      "Invlid phone number",
  ];
}
