export class Location {
  id = "";
  label = "";
  address = "";

  get searchable(): string {
    return `${this.label} - ${this.address}`;
  }
}
