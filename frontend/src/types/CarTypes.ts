import { DiscountRules } from "./DiscountRules";
import { VinDetails } from "./VinDetails";

export class CarListing {
  constructor(
    public vin = "",
    public make = "",
    public model = "",
    public year = 0,
    public price = 0,
    public fullPrice = 0,
    public imageURL = "",
    public disabled = false
  ) {}

  get title(): string {
    return `${this.make} ${this.model} ${this.year}`;
  }

  get searchable(): string {
    return `${this.make} ${this.model} ${this.year} - ${this.vin} `;
  }

  get formattedPrice(): number {
    return this.price / 100;
  }

  get formattedFullPrice(): number {
    return this.fullPrice / 100;
  }
}

export enum Transmission {
  auto = "Automatic",
  manual = "Manual",
}

export class Feature {
  public title = "";
  public field: keyof Car = "vin";
  constructor(title: string, field: keyof Car) {
    this.title = title;
    this.field = field;
  }
}

export class InsuranceCost {
  public minimal = 0;
  public standard = 0;
}

export class InsuranceCosts {
  public young = new InsuranceCost();
  public adult = new InsuranceCost();
}

export class InsuranceOption {
  constructor(public type = Insurance.standard, public cost = 0) {}

  public get label(): string {
    return this.type + " - $" + this.cost + "/day";
  }
}

export enum Insurance {
  standard = "Standard",
  minimal = "Minimal",
  none = "None",
}

export class Car {
  public vin = "";
  public make = "";
  public model = "";
  public powerWindows = false;
  public powerLocks = false;
  public remoteStart = false;
  public bluetooth = false;
  public sunroof = false;
  public usbCharger = false;
  public keylessEntry = false;
  public backupCamera = false;
  public androidAuto = false;
  public appleCarPlay = false;
  public transmission = Transmission.auto;
  public licensePlate = "";
  public imageURLs: string[] = [];
  public seats = 4;
  public doors = 4;
  public mpg = 0;
  public miles = 0;
  public includedMiles = 200;
  public year = 0;
  public price = 0;
  public fullPrice = 0;
  public description = "";
  public locationID = "";
  public discountRules = new DiscountRules();
  public insurance = new InsuranceCosts();
  public disabled = false;

  get title(): string {
    return `${this.make} ${this.model} ${this.year}`;
  }

  get imageURL(): string {
    return this.imageURLs[0];
  }

  get preloadImages(): string[] {
    return this.imageURLs.map((url) => this.createImageKitUrl(url));
  }

  get distanceOverageFee(): number {
    return Math.max((this.price / this.includedMiles) * 1.5, 25);
  }

  get displayDistanceOverageFee(): number {
    const round = (num: number) => Math.round(num + Number.EPSILON) / 100;
    return round(this.distanceOverageFee);
  }

  public fillDataFromVin(vinDetails: VinDetails): Car {
    const clone = Object.assign(new Car(), this);
    if (vinDetails.Make) clone.make = vinDetails.Make;
    if (vinDetails.Model) clone.model = vinDetails.Model;
    if (vinDetails.ModelYear) clone.year = vinDetails.ModelYear;
    if (vinDetails.Doors) clone.doors = vinDetails.Doors;
    if (vinDetails.Seats) clone.seats = vinDetails.Seats;
    return clone;
  }

  get carListing(): CarListing {
    return carListing(this);
  }

  get formattedPrice(): number {
    return this.carListing.formattedPrice;
  }

  get formattedFullPrice(): number {
    return this.carListing.formattedFullPrice;
  }

  get computedBooleanFeatures(): Feature[] {
    if (!this) {
      console.error("computedBooleanFeatures: no this");
      return [];
    }
    return Object.keys(this)
      .map((key) => key as keyof Car)
      .filter((key) => typeof this[key] === "boolean")
      .map((bk) => new Feature(this.camelCaseFormat(bk), bk));
  }

  private camelCaseFormat(camelCase: string) {
    const result = camelCase.replace(/([A-Z])/g, " $1");
    const finalResult = result.charAt(0).toUpperCase() + result.slice(1);
    return finalResult.trim();
  }

  private createImageKitUrl(url: string): string {
    const name = this.extractFileName(url);
    const base = "https://ik.imagekit.io/hkb6txc7g/";
    const transform = "?tr=h-48,w=48,bl-2";
    return base + `${this.vin}/${name}` + transform;
  }

  private extractFileName(url: string): string {
    const reg = `(${this.vin}%2F)(.+?[.]\\w+)`;
    const res = url.match(new RegExp(reg));
    if (res) {
      return res[2];
    }
    return "";
  }
}

export function carListing(car: Car): CarListing {
  return new CarListing(
    car.vin,
    car.make,
    car.model,
    car.year,
    car.price,
    car.fullPrice,
    car.imageURLs[0]
  );
}

export function emptyCarListing(vin: string): CarListing {
  return new CarListing(vin);
}

export function emptyCar(): Car {
  return new Car();
}
