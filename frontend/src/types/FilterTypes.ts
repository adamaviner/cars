import { arrayEquals } from "@/helpers/Utils";
import { Transmission } from "./CarTypes";

enum Sort {
  Relevance = "Relevance",
  PriceAsc = "PriceAsc",
  PriceDesc = "PriceDesc",
}

export enum FilterType {
  Option,
  Range,
  MultiOption,
}

export class OptionEntry {
  public value: string;
  public label: string;

  constructor(value: string, label: string) {
    this.value = value;
    this.label = label;
  }
}

type Dictionary<T> = { [key: string]: T };

export const FilterOptions = {
  sort: [
    new OptionEntry(Sort.Relevance, "Relevance"),
    new OptionEntry(Sort.PriceAsc, "Price: low to high"),
    new OptionEntry(Sort.PriceDesc, "Price: high to low"),
  ],
  transmission: [
    new OptionEntry(Transmission.auto, "Automatic"),
    new OptionEntry(Transmission.manual, "Manual"),
  ],
};

export class Filters {
  public sort = FilterOptions.sort[0].value;
  public transmission = FilterOptions.transmission[0].value;
  public price = [0, 300]; //TODO max price
  public year = [1950, 2022]; //TODO max year
  public date = [0, 0];
}

export class FilterDescription {
  constructor(
    public label = "",
    public type = FilterType.Range,
    public field = "",
    public formatter: undefined | ((v: number[]) => string) = undefined
  ) {}
}

export function defaultFilters(): Filters {
  return new Filters();
}

export class DefaultFunctions {
  static priceFormatter(range: number[]): string {
    return `$${range[0]} - $${range[1]}/day`;
  }
}

export class FiltersImpl {
  public static eq(a: Filters, b: Filters, field: keyof Filters): boolean {
    const aField = a[field];
    const bField = b[field];
    if (typeof aField === "string") {
      return aField == bField;
    }
    if (this.isNumberArray(aField)) {
      return arrayEquals(aField, bField as number[]);
    }
    return false;
  }

  public static assign(a: Filters, b: Filters, field: keyof Filters): Filters {
    if (typeof a[field] === "string") {
      (a[field] as string) = b[field] as string;
      return a;
    }
    if (this.isNumberArray(a[field])) {
      (a[field] as number[]) = b[field] as number[];
      return a;
    }
    return a;
  }

  public static changed(
    a: Filters,
    b: Filters,
    ignore: (keyof Filters)[] = ["date"]
  ): (keyof Filters)[] {
    return Object.keys(a)
      .map((_) => _ as keyof Filters)
      .filter((field) => !ignore.includes(field) && !this.eq(a, b, field));
  }

  public static nonDefault(a: Filters, b: Filters): any {
    const changedKeys = this.changed(a, b, []);
    const res: Dictionary<any> = {};
    changedKeys.forEach((key) => (res[key] = a[key]));
    return res;
  }

  public static formatDetails(
    a: Filters,
    field: keyof Filters,
    formatter: ((v: number[]) => string) | undefined = undefined
  ): string {
    const value = a[field];
    if (typeof value === "string") {
      return value;
    }
    if (this.isNumberArray(value)) {
      if (formatter) return formatter(value);
      return `${value[0]} - ${value[1]}`;
    }
    return "";
  }

  public static isNumberArray(filter: string | number[]): filter is number[] {
    return (filter as number[]).length === 2;
  }
}
