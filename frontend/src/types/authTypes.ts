export interface SignupRequest {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  password: string;
  birthDate: string;
}

export interface LoginRequest {
  email: string;
  password: string;
}

export class LicenseVerification {
  public status = "pending";
  public error = "";
}
