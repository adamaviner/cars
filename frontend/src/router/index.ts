import Vue from "vue";
import VueRouter, { NavigationGuardNext, Route, RouteConfig } from "vue-router";
import HomeView from "../views/HomeView.vue";
import { Auth } from "@/services/auth";
import { Role } from "@/types/UserTypes";
import { dispatch } from "@/services/Dispatch";
Vue.use(VueRouter);

function navigationGuard(role: Role) {
  return (to: Route, from: Route, next: NavigationGuardNext) => {
    if (!Auth.isAuthorized(role)) {
      dispatch.post("Home", role + " required to view: " + to.path);
      next({
        name: "Home",
      });
    } else next();
  };
}

function loginRequired() {
  return (to: Route, from: Route, next: NavigationGuardNext) => {
    if (!Auth.isLoggedIn()) {
      const nextAddr = to.fullPath;
      next({ name: "Login", query: { next: nextAddr } });
    } else next();
  };
}

function isApproved() {
  return (to: Route, from: Route, next: NavigationGuardNext) => {
    const nextAddr = to.fullPath;
    if (!Auth.isLoggedIn()) {
      next({ name: "Login", query: { next: nextAddr } });
    } else if (!Auth.isApproved()) next({ name: "Approval" });
    else next();
  };
}

const routes: Array<RouteConfig> = [
  // {
  //   path: "/soon",
  //   name: "ComingSoon",
  //   component: ComingSoon,
  // },
  {
    path: "/",
    name: "Home",
    props: { redirectMessage: "" },
    component: HomeView,
    children: [
      {
        name: "About",
        path: "about",
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/cleric/About.vue"),
      },
      {
        name: "Login",
        path: "login",
        component: () =>
          import(
            /* webpackChunkName: "login" */ "../views/account/LoginView.vue"
          ),
      },
      {
        name: "Signup",
        path: "signup",
        component: () =>
          import(
            /* webpackChunkName: "signup" */ "../views/account/SignupView.vue"
          ),
      },
      {
        name: "Protection",
        path: "protection",
        component: () =>
          import(
            /* webpackChunkName: "protection" */ "../views/cleric/Protection.vue"
          ),
      },
      {
        name: "Support",
        path: "support",
        component: () =>
          import(
            /* webpackChunkName: "support" */ "../views/cleric/Support.vue"
          ),
      },
      {
        name: "Privacy",
        path: "privacy",
        component: () =>
          import(
            /* webpackChunkName: "privacy" */ "../views/cleric/Privacy.vue"
          ),
      },
      {
        name: "Terms",
        path: "terms",
        component: () =>
          import(/* webpackChunkName: "terms" */ "../views/cleric/Terms.vue"),
      },
      {
        name: "PasswordReset",
        path: "passwordReset/:token",
        component: () =>
          import(
            /* webpackChunkName: "passwordReset" */ "../views/account/PasswordReset.vue"
          ),
      },
      {
        name: "EmailVerification",
        path: "email/verify/:token",
        component: () =>
          import(
            /* webpackChunkName: "emailVerification" */ "../views/account/EmailVerification.vue"
          ),
      },
      {
        path: "/car/:id",
        name: "Car",
        component: () =>
          import(
            /* webpackChunkName: "carDetailsView" */ "../views/CarDetailsView.vue"
          ),
      },
      {
        name: "Checkout",
        path: "checkout",
        beforeEnter: loginRequired(),
        component: () =>
          import(/* webpackChunkName: "checkout" */ "../views/Checkout.vue"),
      },
      {
        name: "Book",
        path: "book/:id",
        beforeEnter: isApproved(),
        component: () =>
          import(/* webpackChunkName: "book" */ "../views/Book.vue"),
      },
      {
        name: "Trip",
        path: "trip/:id",
        beforeEnter: loginRequired(),
        component: () =>
          import(/* webpackChunkName: "trip" */ "../views/TripView.vue"),
      },
      {
        name: "Trips",
        path: "trips",
        beforeEnter: loginRequired(),
        component: () =>
          import(/* webpackChunkName: "trips" */ "../views/Trips.vue"),
      },
      {
        name: "Approval",
        path: "account/approval",
        beforeEnter: loginRequired(),
        props: (route) => ({ redirect: route.query.redirect }),
        // props: true,
        component: () =>
          import(
            /* webpackChunkName: "approval" */ "../views/account/Approval.vue"
          ),
      },
      {
        path: "account",
        name: "Account",
        beforeEnter: loginRequired(),
        component: () =>
          import(
            /* webpackChunkName: "account" */ "../views/account/Account.vue"
          ),
      },
    ],
  },
  {
    path: "/search",
    name: "Search",
    component: () =>
      import(/* webpackChunkName: "search" */ "../views/cleric/Search.vue"),
  },

  {
    path: "/admin",
    name: "Admin",
    beforeEnter: navigationGuard(Role.Custodian),
    component: () =>
      import(/* webpackChunkName: "admin" */ "../views/admin/Admin.vue"),
    children: [
      {
        name: "Dashboard",
        path: "dashboard",
        component: () =>
          import(
            /* webpackChunkName: "dashboard" */ "../views/admin/Dashboard.vue"
          ),
      },
      {
        name: "Users",
        path: "users",
        component: () =>
          import(/* webpackChunkName: "users" */ "../views/admin/Users.vue"),
      },
      {
        name: "Cars",
        path: "cars",
        component: () =>
          import(
            /* webpackChunkName: "cars" */ "../views/admin/CarInventory.vue"
          ),
      },
      {
        name: "TripManager",
        path: "trips",
        component: () =>
          import(
            /* webpackChunkName: "tripManager" */ "../views/admin/TripManager.vue"
          ),
      },
      {
        name: "TextManager",
        path: "texts",
        beforeEnter: navigationGuard(Role.Admin),
        component: () =>
          import(
            /* webpackChunkName: "textManager" */ "../views/admin/TextManager.vue"
          ),
      },
      {
        name: "LocationManager",
        path: "locations",
        beforeEnter: navigationGuard(Role.Admin),
        component: () =>
          import(
            /* webpackChunkName: "locationManager" */ "../views/admin/LocationManager.vue"
          ),
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
