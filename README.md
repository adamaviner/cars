This is a project that was written for a local car rental company,
they requested a webapp for users to rent cars and an admin interface to manage inventory and inspections

The timeline was crunched and I was the solo programmer, as a result the code isn't perfect,
and there is no abundence of comments, but the attempt was to keep it simple and self documenting.

frontend can be run with yarn serve, though it needs an instance of backend for meaningful interaction.
backend with sbt run, but it fails without a firebase service account in backend/src/main/resources/serviceAccount.json
