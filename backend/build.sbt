
scalaVersion := "2.13.7"

name := "cars"
organization := "com.carrentals"
version := "1.0"

//TODO circe generic, circe literal??
scalacOptions ++= Seq(
  "-Ymacro-annotations",
  "-Ywarn-value-discard",
)

Test / logBuffered := false

val http4sVersion = "0.21.18"
val circeVersion = "0.14.1"
val tsecVersion = "0.2.1"
val catsVersion = "2.7.0"
val enumeratumVersion =  "1.7.0"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % catsVersion,
  "org.typelevel" %% "cats-effect" % "2.5.4",
  "co.fs2" %% "fs2-core" % "2.5.10",
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-literal" % circeVersion,
  "com.beachape" %% "enumeratum" % enumeratumVersion,
  "com.beachape" %% "enumeratum-circe" % enumeratumVersion,
  "com.stripe" % "stripe-java" % "20.56.0",
  "com.twilio.sdk" % "twilio" % "8.16.0",
  "io.scalaland" %% "chimney" % "0.6.1",
  "org.typelevel" %% "log4cats-slf4j" % "1.2.1",
  "org.typelevel" %% "cats-mtl" % "1.1.1",
  "com.olegpy" %% "meow-mtl-core" % "0.4.1",
  "org.scalatest" %% "scalatest" % "3.2.5" % "test",
)

libraryDependencies += "com.github.gekomad" %% "itto-csv" % "1.1.0"

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.1.3"
)

// Google Cloud
libraryDependencies ++= Seq(
  "com.google.firebase" % "firebase-admin" % "7.1.1",
  "com.google.cloud" % "google-cloud-storage" % "1.113.14",
)

libraryDependencies ++= Seq( //TODO clean
  "io.github.jmcardon" %% "tsec-http4s" % tsecVersion,
)
////  "io.github.jmcardon" %% "tsec-common" % tsecV,
////  "io.github.jmcardon" %% "tsec-password" % tsecV,
////  "io.github.jmcardon" %% "tsec-cipher-jca" % tsecV,
////  "io.github.jmcardon" %% "tsec-cipher-bouncy" % tsecV,
////  "io.github.jmcardon" %% "tsec-mac" % tsecV,
////  "io.github.jmcardon" %% "tsec-signatures" % tsecV,
////  "io.github.jmcardon" %% "tsec-hash-jca" % tsecV,
////  "io.github.jmcardon" %% "tsec-hash-bouncy" % tsecV,
////  "io.github.jmcardon" %% "tsec-libsodium" % tsecV,
////  "io.github.jmcardon" %% "tsec-jwt-mac" % tsecV,
////  "io.github.jmcardon" %% "tsec-jwt-sig" % tsecV,
//  "io.github.jmcardon" %% "tsec-http4s_2.13" % tsecVersion
//)


enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

dockerBaseImage := "openjdk:11"
Docker / packageName  := "elite-epoch-308417/api"
Docker / maintainer := "Maintainer"
packageSummary := "A to Z Car Rentals server"
packageDescription := "Yep"
dockerRepository := Some("us.gcr.io")