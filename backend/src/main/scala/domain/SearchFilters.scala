package domain

import domain.cars.Transmission
import io.circe.{Decoder, HCursor}

sealed trait Sort extends Product with Serializable

case object Relevance extends Sort
case object PriceAsc extends Sort
case object PriceDesc extends Sort

object Sort {
  implicit val decodeMode: Decoder[Sort] = Decoder[String].emap {
    case "Relevance" => Right(Relevance)
    case "PriceAsc" => Right(PriceAsc)
    case "PriceDesc" => Right(PriceDesc)
    case other => Left(s"Invalid mode: $other")
  }
}

object RangeFilter {
  implicit val decodeMode: Decoder[RangeFilter] = (c: HCursor) => for {
    ls <- c.as[List[Long]]
  } yield {
    RangeFilter(ls.head, ls.last)
  }
}

case class RangeFilter(from: Long, to: Long) {
  def contains(v: Long): Boolean = {
    v >= from && v <= to
  }
}

case class SearchFilters (
  date: RangeFilter,
  sort: Option[Sort] = Some(Relevance),
  transmission: Option[Transmission] = None,
  price: Option[RangeFilter] = None,
  year: Option[RangeFilter] = None,
)