package domain

import com.google.cloud.Timestamp

import java.time.Instant
import java.time.temporal.ChronoUnit

object TimeConversions {
  implicit class LongWithDaysBetween(val l: Long) {
    def daysUntil(until: Long): Int = {
      val startInstant = Instant.ofEpochSecond(l)
      val endInstant = Instant.ofEpochSecond(until)
      val numberOfHours = ChronoUnit.HOURS.between(startInstant, endInstant)
      (numberOfHours / 24.0).ceil.toInt
    }
  }

  implicit class InstantToTimestamp(val i: Instant) {
    def toTimestamp: Timestamp = {
      Timestamp.ofTimeSecondsAndNanos(i.getEpochSecond, i.getNano)
    }
  }
}
