package domain.auth

import cats.effect.Sync
import domain.MapCodec
import org.http4s.{HttpDate, ResponseCookie}
import tsec.authentication.{AugmentedJWT, AuthToken, TSecCookieSettings}
import tsec.common.SecureRandomId
import tsec.jws.mac.{JWSMacCV, JWTMac}
import tsec.jwt.algorithms.JWTMacAlgo

import java.time.Instant
import java.util
import scala.beans.BeanProperty
import scala.collection.mutable

object AugmentedJWTCodec {
  import scala.jdk.CollectionConverters._

  def apply[A, I]: MapCodec[AugmentedJWT[A, I]] = new MapCodec[AugmentedJWT[A, I]] {
    override def toMap(base: AugmentedJWT[A, I]): util.Map[String, Any] = {
      mutable.Map("id" -> base.id,
        "jwt" -> base.jwt,
        "identity" -> base.identity,
        "expiry" -> base.expiry,
        "lastTouched" -> base.lastTouched).asJava
    }

    override def fromMap(javaMap: util.Map[String, AnyRef]): Option[AugmentedJWT[A, I]] = {
      val map = javaMap.asScala
      for {
        id <- map.get("id").map(r => r.asInstanceOf[SecureRandomId])
        jwt <- map.get("jwt").map(r => r.asInstanceOf[JWTMac[A]])
        identity <- map.get("identity").map(r => r.asInstanceOf[I])
        expiry <- map.get("expiry").map(r => r.asInstanceOf[Instant])
        lastTouched <- map.get("lastTouched").map(r => r.asInstanceOf[Option[Instant]])
      } yield AugmentedJWT(id, jwt, identity, expiry, lastTouched)
    }
  }


//  def toMap[A, I](base: AugmentedJWT[A, I]): util.Map[String, Any] =
//    mutable.Map("id" -> base.id,
//      "jwt" -> base.jwt,
//      "identity" -> base.identity,
//      "expiry" -> base.expiry,
//      "lastTouched" -> base.lastTouched).asJava
//
//  def fromMap[A, I](javaMap: util.Map[String, AnyRef]): Option[AugmentedJWT[A, I]] = {
//    val map = javaMap.asScala
//    for {
//      id <- map.get("id").map(r => r.asInstanceOf[SecureRandomId])
//      jwt <- map.get("jwt").map(r => r.asInstanceOf[JWTMac[A]])
//      identity <- map.get("identity").map(r => r.asInstanceOf[I])
//      expiry <- map.get("expiry").map(r => r.asInstanceOf[Instant])
//      lastTouched <- map.get("lastTouched").map(r => r.asInstanceOf[Option[Instant]])
//    } yield AugmentedJWT(id, jwt, identity, expiry, lastTouched)
//  }
}