package domain.auth

import domain.users.User
import domain.users.UserModel.BirthDate

case class LoginRequest(
  email: String,
  password: String
)

case class SignupRequest(
  email: String,
  password: String,
  phone: String,
  firstName: String,
  lastName: String,
  birthDate: BirthDate,
) {
  def asUser(passHash: String): User =
    User(
      id = "",
      email = email.trim.toLowerCase(),
      phone = phone,
      firstName = firstName,
      lastName = lastName,
      birthDate = birthDate,
      passHash = passHash,
    )
}
