package domain.auth

import cats.data.{Kleisli, OptionT}
import cats.effect.{IO, Sync}
import cats.{Applicative, Eq, Monad, MonadError}
import domain.auth.AuthHelpers.User.authRole
import domain.users.User
import org.http4s.{HttpRoutes, Request, Response, Status}
import tsec.authentication.{
  AugmentedJWT,
  BackingStore,
  BearerTokenAuthenticator,
  IdentityStore,
  JWTAuthenticator,
  SecuredRequest,
  SecuredRequestHandler,
  TSecAuthService,
  TSecBearerToken,
  TSecMiddleware,
  TSecTokenSettings
}
import tsec.authorization.{AuthGroup, Authorization, AuthorizationInfo, BasicRBAC, SimpleAuthEnum}
import tsec.common.SecureRandomId
import tsec.jws.mac.{JWSMacCV, JWTMac}
import tsec.jwt.algorithms.JWTMacAlgo
import tsec.mac.jca.{HMACSHA256, MacSigningKey}

import java.time.Instant
import scala.beans.BeanProperty
import scala.concurrent.duration._

object AuthHelpers {

  sealed case class Role(roleRepr: String)

  object Role extends SimpleAuthEnum[Role, String] {
    val Admin: Role = Role("Admin")
    val User: Role = Role("User")
    val Custodian: Role = Role("Custodian")
    val Maintainer: Role = Role("Maintainer") // used by server to execute some maintanance function
    val Unverified: Role = Role("Unverified")

    implicit val E: Eq[Role] = Eq.fromUniversalEquals[Role]

    override def getRepr(t: Role): String = t.roleRepr

    protected val values: AuthGroup[Role] = AuthGroup(Admin, User, Custodian, Maintainer, Unverified)
  }

  object User {
    implicit def authRole[F[_]](implicit
        F: MonadError[F, Throwable]
    ): AuthorizationInfo[F, Role, User] =
      (u: User) => F.pure(u.role)
  }

  def bearerTokenAuthenticator[F[_]: Sync, I](
      tokenStore: BackingStore[F, SecureRandomId, TSecBearerToken[I]],
      userStore: IdentityStore[F, I, User]
  ): BearerTokenAuthenticator[F, I, User] = {
    BearerTokenAuthenticator(
      tokenStore,
      userStore,
      TSecTokenSettings(
        expiryDuration = 14.days, //Absolute expiration time
        maxIdle = None
      )
    )
  }

  def jwtAuthenticator[F[_]: Sync, I, Auth: JWTMacAlgo](
      key: MacSigningKey[Auth],
      authRepo: BackingStore[F, SecureRandomId, AugmentedJWT[Auth, I]],
      userRepo: IdentityStore[F, I, User]
  )(implicit cv: JWSMacCV[F, Auth]): JWTAuthenticator[F, I, User, Auth] = {

    JWTAuthenticator.backed.inBearerToken(
      expiryDuration = 30.days, //Absolute expiration time
      maxIdle = None,
      tokenStore = authRepo,
      identityStore = userRepo,
      signingKey = key
    )
  }

  def adminRequired[F[_], Auth](implicit
      F: MonadError[F, Throwable]
  ): MyRBAC[F, Role, User, Auth] =
    MyRBAC[F, Role, User, Auth](Role.Admin)

  def userRequired[F[_], Auth](implicit
      F: MonadError[F, Throwable]
  ): MyRBAC[F, Role, User, Auth] =
    MyRBAC[F, Role, User, Auth](Role.Admin, Role.User, Role.Custodian)

  def custodianRequired[F[_], Auth](implicit
      F: MonadError[F, Throwable]
  ): MyRBAC[F, Role, User, Auth] =
    MyRBAC[F, Role, User, Auth](Role.Maintainer, Role.Custodian, Role.Admin)

  def maintainerRequired[F[_], Auth](implicit
      F: MonadError[F, Throwable]
  ): MyRBAC[F, Role, User, Auth] =
    MyRBAC[F, Role, User, Auth](Role.Maintainer)

  def withAuthorization[I, A, F[_]](auth: Authorization[F, I, A])(
      pf: PartialFunction[SecuredRequest[F, I, A], F[Response[F]]]
  )(implicit F: Monad[F]): TSecAuthService[I, A, F] =
    withAuthorizationHandler(auth)(pf, defaultOnNotAuthorized[F, I, A])

  def defaultOnNotAuthorized[F[_], I, A](
      unused: SecuredRequest[F, I, A]
  )(implicit F: Monad[F]): OptionT[F, Response[F]] =
    OptionT(F.pure(Some(Response[F](Status.Unauthorized))))

  import cats.syntax.all._
  def withAuthorizationHandler[I, A, F[_]](auth: Authorization[F, I, A])(
      pf: PartialFunction[SecuredRequest[F, I, A], F[Response[F]]],
      onNotAuthorized: SecuredRequest[F, I, A] => OptionT[F, Response[F]]
  )(implicit F: cats.Monad[F]): TSecAuthService[I, A, F] =
    Kleisli { req: SecuredRequest[F, I, A] =>
      if (pf.isDefinedAt(req))
        OptionT(
          auth
            .isAuthorized(req)
            .fold(onNotAuthorized(req))(pf.andThen(OptionT.liftF(_)))
            .flatMap(_.value)
        )
      else OptionT.none[F, Response[F]]
    }

  implicit class SecuredRequestHandlerBubbleErrors[F[_]: Applicative, Identity, User, Auth](
      val auth: SecuredRequestHandler[F, Identity, User, Auth]
  )(implicit
      F: MonadError[F, Throwable]
  ) {
    val defaultNotAuthenticated: Request[F] => F[Response[F]] = _ =>
      F.pure(Response[F](Status.Unauthorized))

    def liftServiceBubbleErrors(
        service: TSecAuthService[User, Auth, F],
        onNotAuthenticated: Request[F] => F[Response[F]] = defaultNotAuthenticated
    ): HttpRoutes[F] = {
      val middleware =
        TSecMiddleware(Kleisli(auth.authenticator.extractAndValidate), onNotAuthenticated)
      middleware(service)
    }
  }
}
