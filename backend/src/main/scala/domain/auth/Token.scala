package domain.auth

import com.google.cloud.Timestamp
import domain.users.UserModel.UserID

import java.time.{Duration, Instant}
import java.time.temporal.TemporalAmount
import scala.beans.BeanProperty
import scala.util.Random
import domain.TimeConversions.InstantToTimestamp


sealed trait TokenType extends Product with Serializable

case object Sms extends TokenType

case object MailVerification extends TokenType

case object PasswordReset extends TokenType

case class Token(
  @BeanProperty id: String,
  @BeanProperty userID: UserID,
  @BeanProperty token: String,
  @BeanProperty expiry: Timestamp

) {
  def this() {
    this("", "", "", Timestamp.now)
  }
}

object Token {
  private def generateSmsToken: String =
    (1 to 6).foldLeft("") { (acc, _) => acc + Random.nextInt(10) }

  private def generateStringToken: String =
    tsec.common.SecureRandomId.Strong.generate

  def smsToken(userID: UserID, expiryDuration: TemporalAmount = Duration.ofMinutes(10)): Token = {
    Token(
      id = "",
      userID = userID,
      token = generateSmsToken,
      expiry = Instant.now().plus(expiryDuration).toTimestamp
    )
  }

  def mailVerificationToken(userID: UserID, expiryDuration: TemporalAmount = Duration.ofDays(14)): Token = {
    Token(
      id = "",
      userID = userID,
      token = generateStringToken,
      expiry = Instant.now().plus(expiryDuration).toTimestamp
    )
  }

  def passwordResetToken(userID: UserID, expiryDuration: TemporalAmount = Duration.ofMinutes(30)): Token = {
    Token(
      id = "",
      userID = userID,
      token = generateStringToken,
      expiry = Instant.now().plus(expiryDuration).toTimestamp
    )
  }
}