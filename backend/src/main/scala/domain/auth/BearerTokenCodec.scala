package domain.auth

import domain.MapCodec
import tsec.authentication.{AugmentedJWT, TSecBearerToken}
import tsec.common.SecureRandomId
import tsec.jws.mac.JWTMac

import java.time.Instant
import java.util
import scala.beans.BeanProperty
import scala.collection.mutable

object BearerTokenCodec {
  import scala.jdk.CollectionConverters._
  
  def apply[I]: MapCodec[TSecBearerToken[I]] = new MapCodec[TSecBearerToken[I]] {

    private def instant(epochSecond: Any): Instant =
      Instant.ofEpochSecond(epochSecond.asInstanceOf[Long])

    override def toMap(base: TSecBearerToken[I]): util.Map[String, Any] = {
      mutable.Map("id" -> base.id,
        "identity" -> base.identity,
        "expiry" -> base.expiry.getEpochSecond,
        "lastTouched" -> base.lastTouched.getOrElse(Instant.MIN).getEpochSecond).asJava
    }

    override def fromMap(javaMap: util.Map[String, AnyRef]): Option[TSecBearerToken[I]] = {
      val map = javaMap.asScala
      for {
        id <- map.get("id").map(r => r.asInstanceOf[SecureRandomId])
        identity <- map.get("identity").map(r => r.asInstanceOf[I])
        expiry <- map.get("expiry").map(r => instant(r))
        lastTouched <- map.get("lastTouched").map(r => {
          val lastTouch = instant(r)
          if (lastTouch.equals(Instant.MIN)) None else Some(lastTouch)
        })
      } yield TSecBearerToken(id, identity, expiry, lastTouched)
    }
  }
}
