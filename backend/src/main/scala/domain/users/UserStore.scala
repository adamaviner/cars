package domain.users

import cats.data.EitherT
import com.google.cloud.storage.Blob
import domain.users.UserModel.UserID

import java.io.InputStream

trait UserStore[F[_]]{
  def makeAdmin(userID: UserID): F[Unit]

  def validatePhone(userID: UserID): F[Unit]

  def find(email: String): F[User]

  def add(user: User): F[User]

  def delete(id: UserID): F[Unit]

  def deleteAll(ids: List[UserID]): F[Unit]

  def update(update: UserUpdate): F[User]

  def list(limit: Int, offset: Int): F[List[User]]

//  def uploadLicense(userID: UserID, is: InputStream, filename: Option[String]): F[User]

  def update(user: User): F[User]
}