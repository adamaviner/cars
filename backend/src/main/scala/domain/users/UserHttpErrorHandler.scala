package domain.users

import cats.MonadError
import endpoint.{HttpErrorHandler, RoutesHttpErrorHandler}
import io.circe.literal.JsonStringContext
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, Response}
import org.http4s.circe.CirceEntityCodec._
import io.circe.syntax._
import io.circe.generic.auto._
case class ErrorResponse(msg: String, mailExists: Boolean = false, phoneExists: Boolean = false,
  invalidAge: Boolean = false,
  invalidEmail: Boolean = false,
  invalidPhone: Boolean = false,
  InvalidPassword: Boolean = false,
)

class UserHttpErrorHandler[F[_]](implicit M: MonadError[F, UserError]) extends HttpErrorHandler[F, UserError] with Http4sDsl[F] {
  private val handler: UserError => F[Response[F]] = {
    case MailAlreadyExists(mail) => Conflict(ErrorResponse(mail, mailExists = true).asJson)
    case PhoneAlreadyExists(phone) => Conflict(ErrorResponse(phone, phoneExists = true).asJson)
    case UserNotFound(id) => NotFound(ErrorResponse(id).asJson)
    case UserAuthenticationFailedError(email) =>
      ExpectationFailed(ErrorResponse(email))
    case InvalidUser(msg) =>
      BadRequest(ErrorResponse(msg))
    case TokenNotFound(userID) =>
      NotFound(ErrorResponse("no token for: " + userID))
    case InvalidUserAge(age) => UnprocessableEntity(ErrorResponse(s"$age", invalidAge = true).asJson)
    case InvalidPhoneNumber(phone) => UnprocessableEntity(ErrorResponse(s"$phone", invalidPhone = true).asJson)
    case InvalidEmail(email) => UnprocessableEntity(ErrorResponse(s"$email", invalidEmail = true).asJson)
    case InvalidPassword(pass) => UnprocessableEntity(ErrorResponse(s"$pass", InvalidPassword = true).asJson)
    case ForbiddenUpdate(update) => Forbidden("Forbidden to update this user")
    case FraudRiskError(msg) => Forbidden(msg)
    case UserNotModified(msg) => NotModified()
  }

  override def handle(routes: HttpRoutes[F]): HttpRoutes[F] =
    RoutesHttpErrorHandler(routes)(handler)
}