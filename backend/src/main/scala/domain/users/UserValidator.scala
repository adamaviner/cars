package domain.users

import cats.Applicative
import cats.mtl.Raise
import cats._
import cats.data._
import cats.syntax.all._
import domain.auth.AuthHelpers.Role
import domain.auth.SignupRequest
import domain.cars.CarListing
import domain.human.VerifyHumanRequest
import domain.users.UserModel.{BirthDate, UserID}
import org.threeten.bp.ZonedDateTime
import services.HumanVerificationService

import java.time.{Instant, LocalDateTime, ZoneId, ZoneOffset}
import java.time.temporal.ChronoUnit
import scala.util.matching.Regex

sealed trait UserError extends Exception

case class MailAlreadyExists(email: String) extends UserError

case class PhoneAlreadyExists(phone: String) extends UserError

case class UserNotFound(email: String) extends UserError

case class TokenNotFound(userID: UserID) extends UserError

case class InvalidUser(msg: String) extends UserError

case class InvalidUserAge(age: Long) extends UserError

case class InvalidPhoneNumber(phone: String) extends UserError

case class InvalidEmail(email: String) extends UserError

case class InvalidPassword(pass: String) extends UserError

case class ForbiddenUpdate(update: UserUpdate) extends UserError

case class UserNotModified(msg: String) extends UserError

case class UserAuthenticationFailedError(email: String) extends UserError

case class FraudRiskError(msg: String) extends UserError

class UserValidator[F[_] : Applicative](
  humanVerificationService: HumanVerificationService[F],
)(implicit FR: Raise[F, UserError]) {
  private val minAge = 16 //TODO ??
  val minPasswordLength = 8

  def assertAge(birthDate: BirthDate): F[Unit] = {
    val birthTime = LocalDateTime.parse(birthDate + "T00:00:00")
    val now = LocalDateTime.now()
    val age = ChronoUnit.YEARS.between(birthTime, now)
    if (age < minAge) FR.raise(InvalidUserAge(age))
    else Applicative[F].unit
  }

  def assertAndFormatPhone(phone: String): F[String] = {
    val pattern = new Regex("""^\(?([0-9]{3})\)?[-.\s]?([0-9]{3})[-.\s]?([0-9]{4})$""")
    if (pattern.matches(phone)) {
      pattern.replaceAllIn(phone, "($1) $2-$3").pure[F]
    } else {
      FR.raise(InvalidPhoneNumber(phone))
    }
  }

   def assertAndFormatPhone(phone: Option[String]): F[String] = {
     phone match {
      case Some(v) => assertAndFormatPhone(phone.get)
      case None => "".pure[F]
    }
   }

  def assertValidEmail(email: String): F[Unit] = {
    val pattern = new Regex(".+@.+\\..+")
    if (pattern.matches(email)) Applicative[F].unit
    else FR.raise(InvalidEmail(email))
  }

  def assertValidEmail(email: Option[String]): F[Unit] = {
    unwrapOption(email, assertValidEmail)
  }

  def assertValidPassword(password: String): F[Unit] = {
    if (password.length < minPasswordLength) FR.raise(InvalidPassword(password))
    else Applicative[F].unit
  }

  def assertValidPassword(password: Option[String]): F[Unit] = {
    unwrapOption(password, assertValidPassword)
  }

  def verifyHuman(request: VerifyHumanRequest): F[Unit] = {
    humanVerificationService.verify(request.buildRequest())
  }

  def validate(signupRequest: SignupRequest): F[SignupRequest] = {
    assertValidPassword(signupRequest.password) *>
      assertValidEmail(signupRequest.email) *>
      assertAge(signupRequest.birthDate) *>
      assertAndFormatPhone(signupRequest.phone).map(formatted => signupRequest.copy(phone = formatted))
  }

  private def unwrapOption(phone: Option[String], f: String => F[Unit]): F[Unit] = {
    phone match {
      case Some(v) => f(v)
      case None => Applicative[F].unit
    }
  }

  def assertCanUpdate(user: User, update: UserUpdate): F[Unit] = {
    if (!user.id.contains(update.id) && user.role != Role.Admin)
      FR.raise(ForbiddenUpdate(update))
    else Applicative[F].unit
  }

  def validate(update: UserUpdate): F[UserUpdate] = {
    def validPhone(update: UserUpdate): F[UserUpdate] =
      update.phone match {
        case None => update.pure[F]
        case Some(phone) =>
          assertAndFormatPhone(phone).map(formatted => update.copy(phone = Some(formatted)))
      }
      unwrapOption(update.password, assertValidPassword) *>
      unwrapOption(update.email, assertValidEmail) *>
      unwrapOption(update.birthDate, assertAge) *>
        validPhone(update)
  }
}
