package domain.users

import domain.auth.AuthHelpers
import domain.users.UserModel.{BirthDate, UserID}
import io.scalaland.chimney.dsl._
import java.time.{Instant, LocalDateTime, ZoneId, ZoneOffset}
import java.time.temporal.ChronoUnit

import scala.beans.BeanProperty

object UserModel {
  type UserID = String
  type BirthDate = String
}

case class User(
    id: UserID,
    email: String,
    phone: String,
    firstName: String,
    lastName: String,
    birthDate: BirthDate,
    role: AuthHelpers.Role = AuthHelpers.Role.User,
    validLicense: Boolean = false,
    passHash: String = "",
    validPhone: Boolean = false,
    validEmail: Boolean = false
) {
  def name(): String = firstName + " " + lastName

  def toBean: UserBean =
    this
      .into[UserBean]
      .withFieldComputed(_.id, _.id)
      .withFieldComputed(_.birthDate, _.birthDate)
      .withFieldComputed(_.role, _.role.roleRepr)
      .transform

  def toUserUpdate: UserUpdate =
    this.into[UserUpdate].transform

  def age: Int = {
    val birthTime = LocalDateTime.parse(birthDate + "T00:00:00")
    val now = LocalDateTime.now()
    ChronoUnit.YEARS.between(birthTime, now).intValue()
  }

  def isYoungDriver: Boolean =
    age <= 24

  def isAdmin: Boolean = role == AuthHelpers.Role.Admin
}

case class UserBean(
    @BeanProperty id: UserID = null,
    @BeanProperty email: String = "",
    @BeanProperty phone: String = "",
    @BeanProperty firstName: String = "",
    @BeanProperty lastName: String = "",
    @BeanProperty birthDate: BirthDate = "",
    @BeanProperty validLicense: Boolean = false,
    @BeanProperty role: String = AuthHelpers.Role.User.roleRepr,
    @BeanProperty passHash: String = "",
    @BeanProperty validPhone: Boolean = false,
    @BeanProperty validEmail: Boolean = false
) {
  def this() {
    this(null)
  }

  def toBase: User = {
    this
      .into[User]
      .withFieldComputed(_.id, u => Option(u.id).getOrElse(""))
      .withFieldComputed(_.birthDate, u => u.birthDate)
      .withFieldComputed(_.role, u => AuthHelpers.Role(u.role))
      //      .enableMethodAccessors
      .transform
  }
}

// leave out privileged fields, so they can only be updated by admin in separate flow
case class UserUpdate(
    id: UserID,
    email: Option[String] = None,
    phone: Option[String] = None,
    firstName: Option[String] = None,
    lastName: Option[String] = None,
    birthDate: Option[BirthDate] = None,
    password: Option[String] = None,
    passHash: Option[String] = None
) {
  def merge(oldData: User): User =
    oldData.copy(
      email = email.getOrElse(oldData.email),
      phone = phone.getOrElse(oldData.phone),
      firstName = firstName.getOrElse(oldData.firstName),
      lastName = lastName.getOrElse(oldData.lastName),
      birthDate = birthDate.getOrElse(oldData.birthDate),
      passHash = passHash.getOrElse(oldData.passHash),
      validEmail = oldData.validEmail && email.isEmpty || email.contains(oldData.email),
      validPhone = oldData.validPhone && phone.isEmpty || phone.contains(oldData.phone)
    )
}

case class Verification(
    @BeanProperty userID: UserID = "",
    @BeanProperty sessionID: String = "",
    @BeanProperty status: String = "pending",
    @BeanProperty error: String = ""
) {
  def this() =
    this("")
}
