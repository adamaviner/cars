package domain

import java.util

trait MapCodec[V] {
  def toMap(base: V): util.Map[String, Any]
  def fromMap(javaMap: util.Map[String, AnyRef]): Option[V]
}
