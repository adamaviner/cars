package domain.cars

import cats.MonadError
import endpoint.{HttpErrorHandler, RoutesHttpErrorHandler}
import org.http4s.{HttpRoutes, Response}
import org.http4s.dsl.Http4sDsl

class CarHttpErrorHandler[F[_]](implicit M: MonadError[F, CarError]) extends HttpErrorHandler[F, CarError] with Http4sDsl[F] {
  private val handler: CarError => F[Response[F]] = {
    case InvalidCar(msg) => BadRequest(msg)
    case CarVinExists(vin) => Conflict(vin)
    case CarNotFound(vin) => NotFound(vin)
  }

  override def handle(routes: HttpRoutes[F]): HttpRoutes[F] =
    RoutesHttpErrorHandler(routes)(handler)
}