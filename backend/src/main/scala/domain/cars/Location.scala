package domain.cars

import domain.trips.{DiscountRules, DiscountRulesBean}
import domain.users.User
import io.circe.{Decoder, Encoder}

import scala.beans.BeanProperty

import scala.jdk.CollectionConverters._
import enumeratum._
import scala.util.matching.Regex
import java.net.URLEncoder

case class Location(
    @BeanProperty id: String = "",
    @BeanProperty label: String = "",
    @BeanProperty address: String = "",
    @BeanProperty directions: String = "",
    //TODO more fields
) {
  def this() {
    this("")
  }
  def mapsURL: String = {
    val base = "https://www.google.com/maps/search/?api=1&query="

    return URLEncoder.encode(base + address, "UTF-8")
  }
}
