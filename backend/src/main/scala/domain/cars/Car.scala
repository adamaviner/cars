package domain.cars

import domain.trips.{DiscountRules, DiscountRulesBean}
import domain.users.User
import io.circe.{Decoder, Encoder}

import scala.beans.BeanProperty
import io.scalaland.chimney.dsl._

import scala.jdk.CollectionConverters._
import enumeratum._
import scala.util.matching.Regex

object Defaults {
  val includedMiles = 200
}

case class CarListing(
  vin: String,
  make: String,
  model: String,
  imageURL: String,
  year: Int,
  price: Int,
  fullPrice: Int,
)

case class CarListingMask(
  @BeanProperty vin: String,
  @BeanProperty make: String,
  @BeanProperty model: String,
  @BeanProperty year: Int,
  @BeanProperty price: Int,
  @BeanProperty fullPrice: Int,
  @BeanProperty imageURLs: java.util.List[String],
  @BeanProperty discountRules: DiscountRulesBean = DiscountRulesBean(""),
) {
  def this() {
    this("", "", "", 0, 0, 0, List().asJava)
  }

  def toBase: CarListing =
    this.into[CarListing]
      .withFieldComputed(_.imageURL, cl => cl.imageURLs.get(0))
      .transform
}

object CarListingMask {
  val fields = List("vin", "make", "model", "year", "price", "imageURLs", "fullPrice", "discountRules")
}

case class Car(
  vin: String,
  powerWindows: Boolean = false,
  powerLocks: Boolean = false,
  remoteStart: Boolean = false,
  bluetooth: Boolean = false,
  sunroof: Boolean = false,
  usbCharger: Boolean = false,
  keylessEntry: Boolean = false,
  backupCamera: Boolean = false,
  androidAuto: Boolean = false,
  appleCarPlay: Boolean = false,
  transmission: Transmission = Transmission.Manual,
  imageURLs: List[String] = List(""),
  licensePlate: String = "",
  make: String = "",
  model: String = "",
  seats: Int = 0,
  doors: Int = 0,
  mpg: Int = 0,
  miles: Int = 0,
  year: Int = 0,
  price: Int = 0, // in cents
  fullPrice: Int = 0,
  includedMiles: Int = Defaults.includedMiles,
  discountRules: DiscountRules = DiscountRules.default,
  description: String = "",
  insurance: InsuranceCosts = InsuranceCosts(),
  locationID: String = "",
  disabled: Boolean = false,
) {
  def toBean: CarBean = {
    this.into[CarBean]
      .withFieldComputed(_.imageURLs, c => c.imageURLs.asJava)
      .withFieldComputed(_.discountRules, c => c.discountRules.toBean)
      .withFieldComputed(_.transmission, c => c.transmission.entryName)
      .transform
  }

  def toListing: CarListing =
    this.into[CarListing]
      .withFieldComputed(_.imageURL, c => c.imageURLs.head)
      .transform

      def label: String = 
      s"$make $model $year"

  def transformedHeroImage: String = {
    val hero = imageURLs.head
    val name = this.extractFileName(hero);
    if (name.isEmpty) return hero;
    val base = "https://ik.imagekit.io/hkb6txc7g/";
    val transform = "?tr=w-104,h-104,cm-pad_resize,bg-FFFFFF";
    return base + s"$vin/$name" + transform;
  }

  private def extractFileName(url: String): String = {
    val reg = s""".*(${vin}%2F)(.+)[?].*""".r
    url match {
      case reg(_, fileName) => fileName
      case _ => ""
    }
  }
}

case class InsuranceCost(
  @BeanProperty minimal: Int = 0,
  @BeanProperty standard: Int = 0,
) {
  def this() = this(0)
}

case class InsuranceCosts(
  @BeanProperty young: InsuranceCost = InsuranceCost(),
  @BeanProperty adult: InsuranceCost = InsuranceCost(),
) {
  def this() = this(InsuranceCost())
}

case class CarBean(
  @BeanProperty vin: String,
  @BeanProperty powerWindows: Boolean = false,
  @BeanProperty powerLocks: Boolean = false,
  @BeanProperty remoteStart: Boolean = false,
  @BeanProperty bluetooth: Boolean = false,
  @BeanProperty sunroof: Boolean = false,
  @BeanProperty usbCharger: Boolean = false,
  @BeanProperty keylessEntry: Boolean = false,
  @BeanProperty backupCamera: Boolean = false,
  @BeanProperty androidAuto: Boolean = false,
  @BeanProperty appleCarPlay: Boolean = false,
  @BeanProperty transmission: String =  "Manual",
  @BeanProperty imageURLs: java.util.List[String] = List[String]().asJava,
  @BeanProperty licensePlate: String = "",
  @BeanProperty make: String = "",
  @BeanProperty model: String = "",
  @BeanProperty seats: Int = 0,
  @BeanProperty doors: Int = 0,
  @BeanProperty mpg: Int = 0,
  @BeanProperty miles: Int = 0,
  @BeanProperty year: Int = 0,
  @BeanProperty price: Int = 0,
  @BeanProperty fullPrice: Int = 0,
  @BeanProperty includedMiles: Int = Defaults.includedMiles,
  @BeanProperty discountRules: DiscountRulesBean = DiscountRulesBean(""),
  @BeanProperty description: String = "",
  @BeanProperty insurance: InsuranceCosts = InsuranceCosts(),
  @BeanProperty locationID: String = "",
  @BeanProperty disabled: Boolean = false,
) {
  def this() {
    this("")
  }

  def toBase: Car = {
    this.into[Car]
      .withFieldComputed(_.imageURLs, c => c.imageURLs.asScala.toList)
      .withFieldComputed(_.discountRules, c => c.discountRules.toBase)
      .withFieldComputed(_.transmission, c => Transmission.withName(c.transmission))
      .transform
  }
}

sealed trait Transmission extends EnumEntry

case object Transmission extends Enum[Transmission] with CirceEnum[Transmission] {
  case object Manual extends Transmission

  case object Automatic extends Transmission

  val values: IndexedSeq[Transmission] = findValues
}

sealed trait Insurance extends EnumEntry

case object Insurance extends Enum[Insurance] with CirceEnum[Insurance] {
  case object Standard extends Insurance

  case object Minimal extends Insurance

  case object None extends Insurance

  val values: IndexedSeq[Insurance] = findValues
}