package domain.cars

import cats.Applicative
import cats.mtl.Raise
import cats._
import cats.data._
import cats.syntax.all._

sealed trait CarError extends Exception
case class InvalidCar(msg: String) extends CarError
case class CarVinExists(vin: String) extends CarError
case class CarNotFound(vin: String) extends CarError

class CarValidator[F[_]: Applicative](implicit FR: Raise[F, InvalidCar]) {
//  def isValid(car: Car): F[Boolean] = {
//    if (car.make.length < 2)
//      FR.raise(InvalidCar("Invalid car make: " + car.make))
//  }
}
