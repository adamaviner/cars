package domain.trips

import domain.cars.Car
import domain.TimeConversions.LongWithDaysBetween
import scala.beans.BeanProperty
import scala.jdk.CollectionConverters._
import io.scalaland.chimney.dsl._

final case class DiscountRules(
    @BeanProperty vin: String,
    @BeanProperty daysRules: List[DiscountDaysRule] = List()
) {
  def this() {
    this("")
  }

  def toBean: DiscountRulesBean =
    this
      .into[DiscountRulesBean]
      .withFieldComputed(_.daysRules, _.daysRules.asJava)
      .transform

  def discount(startDate: Long, untilDate: Long): Int = {
    val daysRules =
      if (this.daysRules.nonEmpty) this.daysRules
      else DiscountRules.defaultDaysDiscount
    tripLengthDiscount(daysRules, startDate.daysUntil(untilDate))
      .map(_.discount)
      .getOrElse(0)
  }
  
  def applicableDiscounts(price: Int, startDate: Long, untilDate: Long): List[Discount] = {
    val daysRules =
      if (this.daysRules.nonEmpty) this.daysRules
      else DiscountRules.defaultDaysDiscount
    val lengthDiscount =
      tripLengthDiscount(daysRules, startDate.daysUntil(untilDate))

    if (lengthDiscount.nonEmpty) {
      val discountFactor = lengthDiscount.get.discount / 100.0
      val discountAmount = Math.round(price * discountFactor).toInt
      val discount = Discount(
        s"${lengthDiscount.get.from}+ day discount",
        discountAmount
      )
      List(discount)
    } else List();
  }

  def priceWithDiscount(
      price: Int,
      insurancePrice: Int,
      startDate: Long,
      untilDate: Long
  ): PriceWithDiscount = {
    val discounts = applicableDiscounts(price, startDate, untilDate)

    PriceWithDiscount(price = price + insurancePrice, discounts)
  }

  private def tripLengthDiscount(
      list: List[DiscountDaysRule],
      tripLength: Int
  ): Option[DiscountDaysRule] = {
    list.find(_.discountFor(tripLength) > 0)
  }
}

final case class DiscountRulesBean(
    @BeanProperty vin: String,
    @BeanProperty daysRules: java.util.List[DiscountDaysRule] = List().asJava
) {
  def this() {
    this("")
  }

  def toBase: DiscountRules =
    this
      .into[DiscountRules]
      .withFieldComputed(_.daysRules, _.daysRules.asScala.toList)
      .transform
}

object DiscountRules {
  val defaultDaysDiscount: List[DiscountDaysRule] = List(
    DiscountDaysRule(from = 5, to = 9, discount = 5),
    DiscountDaysRule(from = 10, to = 14, discount = 10),
    DiscountDaysRule(from = 15, to = 1000, discount = 15)
  )

  val default: DiscountRules =
    DiscountRules(vin = "defaultRules", daysRules = defaultDaysDiscount)
}

case class DetailedPrice(
    basePrice: Int,
    discounts: List[Discount],
    discount: Int,
    deposit: Int,
    protection: Int,
    total: Int,
    taxable: Int,
    tax: Int
)

object DetailedPrice {
  def from(
      basePrice: Int,
      discounts: List[Discount],
      deposit: Int,
      protection: Int
  ): DetailedPrice = {
    val discount = discounts.map(_.amount).sum
    val taxable = basePrice - discount + protection
    val tax = Math.round(taxable * (9.5 / 100)).toInt
    DetailedPrice(
      basePrice,
      discounts,
      discount = discount,
      deposit = deposit,
      protection = protection,
      total = basePrice - discount + deposit + protection + tax,
      taxable = taxable,
      tax = tax
    )
  }
}

case class PriceWithDiscount(
    price: Int,
    discounts: List[Discount],
    deposit: Int = 30000 //FIXME fireConfig
)

case class Discount(
    title: String,
    amount: Int
)

final case class DiscountDaysRule(
    @BeanProperty from: Int = 0,
    @BeanProperty to: Int = 0,
    @BeanProperty discount: Int = 0
) {
  def this() {
    this(0)
  }

  def discountFor(days: Int): Int =
    if (days >= from && days <= to) discount
    else 0
}
