package domain.trips

import io.scalaland.chimney.dsl.TransformerOps

import java.time.{Instant, LocalDate, LocalDateTime, ZoneId}
import java.time.temporal.ChronoUnit
import scala.beans.BeanProperty
import scala.reflect.ClassTag
import domain.TimeConversions.LongWithDaysBetween
import domain.cars.Car

import scala.jdk.CollectionConverters._
import java.time.format.DateTimeFormatter
import enumeratum._
import domain.payment.IncidentalChargeBean
import domain.payment.IncidentalCharge
import domain.cars.Insurance
import domain.users.User
import domain.cars.Insurance.Standard
import domain.cars.Insurance.Minimal

import utils.TimeUtils.{DurationInt, PeriodInt}
import domain.payment.PaymentType
import domain.payment.ChargeRequest

case class TripDates(
    @BeanProperty startDate: Long = 0,
    @BeanProperty endDate: Long = 0,
    @BeanProperty id: String = ""
) {
  def this() {
    this(0)
  }
}

object TripDates {
  def fields: List[String] =
    List("startDate", "endDate", "id")
}

sealed trait TripType extends EnumEntry
case object TripType extends Enum[TripType] with CirceEnum[TripType] {
  case object Manual extends TripType
  case object Turo extends TripType
  case object Maintenance extends TripType
  case object Other extends TripType
  val values: IndexedSeq[TripType] = findValues
}

case class Trip(
    id: String = "",
    vin: String = "",
    userID: String = "",
    startDate: Long = 0, // seconds from epoch
    endDate: Long = 0,
    price: Int = 0, //  in cents
    wasPaid: Boolean = false,
    active: Boolean = true,
    canceled: Boolean = false,
    preInspected: Boolean = false,
    postInspected: Boolean = false,
    timestamp: Long = Instant.now().getEpochSecond,
    booked: Boolean = false,
    insurance: Insurance = Insurance.Standard,
    inspectionMailsSent: Int = 0,
    tripType: Option[TripType] = None,
    manualDescription: String = "",
) {
  def startDateFormatted(): String = Trip.formatDate(startDate)

  def endDateFormatted(): String = Trip.formatDate(endDate)

  def startInstant: Instant = Instant.ofEpochSecond(this.startDate)

  def endInstant: Instant = Instant.ofEpochSecond(this.endDate)

  def lateForInspectionHours = 12

  def moreLateForInspectionHours = 24

  def tripTypeString: String = tripType.map(_.entryName).getOrElse("invalid type")

  def asTripBean: TripBean =
    this
      .into[TripBean]
      .withFieldComputed(_.insurance, _.insurance.entryName)
      .withFieldComputed(_.tripType, _.tripType.map(_.entryName).getOrElse(""))
      .transform

  def asTripRequest: TripRequest =
    this.into[TripRequest].transform

  def asTripUpdateRequest: TripUpdateRequest =
    this.into[TripUpdateRequest].transform

  def expired(): Boolean =
    Instant.ofEpochSecond(timestamp).until(Instant.now(), ChronoUnit.MINUTES) > 15

  def canHold: Boolean = {
    false; // Don't do holds for now
    // val maxHoldTime = 7.days // can hold for seven days
    // val inspectionTime = 12.hours // we need 12 hours for trip inspection before releasing hold
    // val maxHoldDate = Instant.now.plus(maxHoldTime).minus(inspectionTime)
    // endInstant.isBefore(maxHoldDate)
  }

  def paymentType: PaymentType =
    if (canHold) PaymentType.Hold
    else PaymentType.Charge

  def lateForInspection: Boolean = {
    val lateHours =
      if (inspectionMailsSent == 0) lateForInspectionHours
      else moreLateForInspectionHours * inspectionMailsSent
    val timeUntilStart = Instant.now.until(startInstant, ChronoUnit.HOURS)
    val hoursAfterEnd = endInstant.until(Instant.now, ChronoUnit.HOURS)
    val preInspectionRequired =
      (!this.preInspected && inspectionMailsSent == 0 &&
        timeUntilStart < lateForInspectionHours && timeUntilStart > 0)
    val postInspectionRequired = (!this.postInspected && hoursAfterEnd > lateHours)
    preInspectionRequired || postInspectionRequired
  }
}

object Trip {
  def formatDate(date: Long): String =
    DateTimeFormatter
      .ofPattern("MM dd")
      .withZone(ZoneId.systemDefault)
      .format(Instant.ofEpochSecond(date))

  def humanFormat(date: Long): String =
    DateTimeFormatter
      .ofPattern("MMM dd")
      .withZone(ZoneId.systemDefault)
      .format(Instant.ofEpochSecond(date))
}

case class TripBean(
    @BeanProperty id: String = "",
    @BeanProperty vin: String = "",
    @BeanProperty userID: String = "",
    @BeanProperty startDate: Long = 0, // seconds from epoch
    @BeanProperty endDate: Long = 0,
    @BeanProperty price: Int = 0, //  in cents
    @BeanProperty wasPaid: Boolean = false,
    @BeanProperty active: Boolean = true,
    @BeanProperty canceled: Boolean = false,
    @BeanProperty preInspected: Boolean = false,
    @BeanProperty postInspected: Boolean = false,
    @BeanProperty timestamp: Long = Instant.now().getEpochSecond,
    @BeanProperty booked: Boolean = false,
    @BeanProperty insurance: String = Insurance.Standard.entryName,
    @BeanProperty inspectionMailsSent: Int = 0,
    @BeanProperty tripType: String = "",
    @BeanProperty manualDescription: String = "",
) extends TripMask {
  override def fields: List[String] =
    List(
      "id",
      "vin",
      "userID",
      "startDate",
      "endDate",
      "price",
      "wasPaid",
      "active",
      "timestamp",
      "booked",
      "canceled"
    )
  def this() {
    this("")
  }

  def asBase: Trip =
    this
      .into[Trip]
      .withFieldComputed(_.insurance, b => Insurance.withName(b.insurance))
      .withFieldComputed(_.tripType, b => TripType.withNameOption(b.tripType))
      .transform
}

sealed trait InspectionType extends EnumEntry
case object InspectionType extends Enum[InspectionType] with CirceEnum[InspectionType] {
  case object Pre extends InspectionType
  case object Post extends InspectionType
  val values: IndexedSeq[InspectionType] = findValues
}

sealed trait FuelLevel extends EnumEntry
case object FuelLevel extends Enum[FuelLevel] with CirceEnum[FuelLevel] {
  case object Full extends FuelLevel
  case object ThreeQtrs extends FuelLevel
  case object Half extends FuelLevel
  case object Qrtr extends FuelLevel
  case object NearEmpty extends FuelLevel
  case object Empty extends FuelLevel
  val values: IndexedSeq[FuelLevel] = findValues
}

case class TripInspectionCSV(
    tripID: String,
    currentMiles: Int = 0,
    gasLevel: FuelLevel = FuelLevel.Full,
    cleaning: Boolean = false,
    charge: Int = 0,
    description: String = "",
    timestamp: Long = Instant.now.getEpochSecond,
    inspectionType: InspectionType = InspectionType.Pre,
)

case class TripInspection(
    tripID: String,
    currentMiles: Int = 0,
    gasLevel: FuelLevel = FuelLevel.Full,
    cleaning: Boolean = false,
    charge: Int = 0,
    description: String = "",
    timestamp: Long = Instant.now.getEpochSecond,
    inspectionType: InspectionType = InspectionType.Pre,
    photos: List[String] = List(),
    incidentals: List[IncidentalCharge] = List()
) {
  def this() = {
    this("")
  }

  def inspectedField(): String = {
    val prefix = if (inspectionType == InspectionType.Pre) "pre" else "post"
    prefix + "Inspected"
  }

  def chargeAmount: Int = {
    incidentals.map(_.amount).sum
  }

  def asCsv: TripInspectionCSV =
    this.into[TripInspectionCSV]
    .transform

  def asBean: TripInspectionBean =
    this
      .into[TripInspectionBean]
      .withFieldComputed(_.photos, _.photos.asJava)
      .withFieldComputed(_.incidentals, b => b.incidentals.map(_.asBean).asJava)
      .withFieldComputed(_.inspectionType, i => i.inspectionType.entryName)
      .withFieldComputed(_.gasLevel, i => i.gasLevel.entryName)
      .transform
}

case class TripInspectionBean(
    @BeanProperty tripID: String,
    @BeanProperty currentMiles: Int = 0,
    @BeanProperty gasLevel: String = FuelLevel.Full.entryName,
    @BeanProperty cleaning: Boolean = false,
    @BeanProperty charge: Int = 0,
    @BeanProperty description: String = "",
    @BeanProperty timestamp: Long = Instant.now.getEpochSecond,
    @BeanProperty inspectionType: String = InspectionType.Pre.entryName,
    @BeanProperty photos: java.util.List[String] = List().asJava,
    @BeanProperty incidentals: java.util.List[IncidentalChargeBean] = List().asJava
) {
  def this() = {
    this("")
  }

  def asBase: TripInspection =
    this
      .into[TripInspection]
      .withFieldComputed(_.photos, _.photos.asScala.toList)
      .withFieldComputed(_.incidentals, b => b.incidentals.asScala.toList.map(_.asBase))
      .withFieldComputed(_.inspectionType, b => InspectionType.withName(b.inspectionType))
      .withFieldComputed(_.gasLevel, b => FuelLevel.withName(b.gasLevel))
      .transform
}

case class TripUpdateRequest(
    id: String,
    vin: String,
    userID: String = "",
    startDate: Long,
    endDate: Long,
    price: Int = 0, // in cents
    insurance: Insurance,
    chargeRequest: Option[ChargeRequest] = scala.None
) {
  def asTrip: Trip = {
    this.into[Trip].transform
  }

  def asTripRequest: TripRequest = {
    this.into[TripRequest].transform
  }
}

case class TripCalculateRequest(
    startDate: Long,
    endDate: Long,
    tripId: String = "",
    insurance: Insurance = Insurance.None
)

case class TripRequest(
    vin: String,
    userID: String = "",
    startDate: Long,
    endDate: Long,
    price: Int = 0, // in cents
    insurance: Insurance = Insurance.Standard
) {
  def asTrip: Trip = {
    this.into[Trip].transform
  }

  def asTripUpdate(tripID: String): TripUpdateRequest = {
    this
      .into[TripUpdateRequest]
      .withFieldComputed(_.id, _ => tripID)
      .transform
  }

  def tripLength: Int = {
    startDate.daysUntil(endDate)
  }

  def updateFee(car: Car): Int = {
    if (freeCancel) 0 else car.price // price of one day
  }

  def freeCancel: Boolean =
    startDate >= Instant.now.plus(1, ChronoUnit.DAYS).getEpochSecond()

  def detailedPrice(car: Car, user: Option[User] = scala.None): DetailedPrice = {
    val priceWithDiscounts = priceWithDiscount(car, user)
    val discount = priceWithDiscounts.discounts.map(_.amount).sum
    val protectionPerDay = insuranceCosts(car, user)
    val protection = protectionPerDay * tripLength
    DetailedPrice.from(
      basePrice = car.price * tripLength,
      discounts = priceWithDiscounts.discounts,
      deposit = priceWithDiscounts.deposit,
      protection = protection
    )
  }

  def insuranceCosts(car: Car, user: Option[User]): Int = {
    val insuranceCosts =
      if (user.isEmpty || user.get.isYoungDriver) car.insurance.young
      else car.insurance.adult
    insurance match {
      case Standard => insuranceCosts.minimal + insuranceCosts.standard
      case Minimal  => insuranceCosts.minimal
      case Insurance.None     => 0
    }
  }

  def priceWithDiscount(car: Car, user: Option[User] = scala.None): PriceWithDiscount = {
    val pricePerDay = car.price
    val insurancePerDay = insuranceCosts(car, user)
    val numberOfDays = tripLength
    val price = pricePerDay * numberOfDays
    val insurancePrice = insurancePerDay * numberOfDays
    car.discountRules.priceWithDiscount(price, insurancePrice, startDate, endDate)
  }
}

trait TripMask {
  def fields: List[String]
  def id: String
}

case class TripVinMask(
    @BeanProperty id: String = "",
    @BeanProperty vin: String = ""
) extends TripMask {
  override implicit def fields: List[String] = List("id", "vin")

  def this() {
    this("")
  }
}

case class TripIdMask(
    @BeanProperty id: String = ""
) extends TripMask {
  override implicit def fields: List[String] = List("id")

  def this() {
    this("")
  }
}

case class TripVinUserMask(
    @BeanProperty id: String = "",
    @BeanProperty vin: String = "",
    @BeanProperty userID: String = ""
) extends TripMask {
  def fields: List[String] = List("id", "vin", "userID")

  def this() {
    this("")
  }
}

case class MaintenanceRequest(
    id: String = "",
    vin: String,
    startDate: Option[Long],
    endDate: Option[Long],
    indefinite: Boolean = false
)

object MaintenanceTrip {
  val userID: String = "maintenance"
  private def makeTrip(vin: String, from: Long, until: Long): Trip =
    Trip("", vin, userID = userID, from, until, price = 0)

  private val maxEndDate: Long =
    LocalDateTime.now().plusYears(1).atZone(ZoneId.systemDefault).toEpochSecond

  def indefinite(vin: String): Trip =
    this.makeTrip(vin, Instant.now().getEpochSecond, maxEndDate)

  def range(vin: String, from: Option[Long], until: Option[Long]): Trip =
    this.makeTrip(vin, from.getOrElse(Instant.now().getEpochSecond), until.getOrElse(maxEndDate))

  def fromRequest(request: MaintenanceRequest): Trip = {
    val trip =
      if (request.indefinite) this.indefinite(request.vin)
      else this.range(request.vin, request.startDate, request.endDate)
    trip.copy(id = request.id)
  }
}
