package domain.trips

import cats.Applicative
import cats.mtl.Raise
import cats._
import cats.data._
import cats.syntax.all._
import domain.cars.{Car, CarListing}

import java.time.Instant
import java.time.temporal.ChronoUnit
import domain.trips.PriceWithDiscount
import domain.users.UserModel.UserID
import domain.users.User
import utils.TimeUtils.{DurationInt, PeriodInt}

sealed trait TripError extends Exception

case class InvalidTrip(msg: String) extends TripError

case class CarIsReservedForDate(msg: String) extends TripError

case class TripNotFound(id: String) extends TripError

case class StartTooLate(startDate: Long) extends TripError

case class TripTooLong(days: Long) extends TripError

case class PriceMismatch(serverPrice: Int) extends TripError

case class BadUserTrip(userID: UserID) extends TripError

case class NoInspection(tripID: String) extends TripError

case class TripImmutable(tripID: String) extends TripError

case class TripInspectionExists(tripID: String) extends TripError

class TripValidator[F[_]: Applicative](implicit FR: Raise[F, TripError]) {
  def validateUser(trip: Trip, user: User): F[Unit] = {
    if (user.id != trip.userID)
      FR.raise(BadUserTrip(user.id))
    else Applicative[F].unit
  }

  def validateTotal(car: Car, tripRequest: TripRequest, user: User): F[Unit] = {
    val price = tripRequest.detailedPrice(car, Some(user))
    println("validateTotal: " + price)
    validatePrice(price.total, tripRequest)
  }

  def validateBasePrice(car: Car, tripRequest: TripRequest, user: User): F[Unit] = {
    val price = tripRequest.detailedPrice(car, Some(user))
    validatePrice(price.basePrice, tripRequest)
  }

  def validatePrice(price: Int, tripRequest: TripRequest): F[Unit] = {
      println(s"validatePrice: price: ${price}, requestPrice: ${tripRequest.price}")
    if (price != tripRequest.price)
      FR.raise(PriceMismatch(price))
    else Applicative[F].unit
  }

  def validateTrip(tripRequest: TripRequest): F[Unit] = {
    validateDates(tripRequest) *>
     validateTimeBeforeTrip(tripRequest) *>
      validateMaxBookAhead(tripRequest) *>
      validateMaxBookTime(tripRequest)
  }

  def validateCanUpdateTrip(trip: Trip): F[Unit] = {
    Applicative[F].unit
    // if (!trip.booked) Applicative[F].unit
    // // TODO update whenever??
    // if (trip.startDate >= Instant.now.plus(1.days).getEpochSecond()) Applicative[F].unit
    // else FR.raise(TripImmutable(trip.id))
  }

  def priceForTripUpdate(trip: Trip, car: Car): Int = {
    trip.asTripRequest.updateFee(car)
  }

  def freeCancel(trip: Trip): Boolean =
    trip.asTripRequest.freeCancel

  def validateDates(tripRequest: TripRequest): F[Unit] = {
    if (tripRequest.startDate >= tripRequest.endDate)
      FR.raise(InvalidTrip("Ends before start"))
    else if (tripRequest.startDate < Instant.now.getEpochSecond())
      FR.raise(InvalidTrip("Starts before now"))
    else Applicative[F].unit
  }

  def validateTimeBeforeTrip(tripRequest: TripRequest): F[Unit] = {
    val maxTimeBeforeTrip = 12.hours
    val minStart = Instant.now().plus(maxTimeBeforeTrip)
    val tripStart = Instant.ofEpochSecond(tripRequest.startDate)
    if (tripStart.isBefore(minStart))
      FR.raise(StartTooLate(tripRequest.startDate))
    else Applicative[F].unit
  }

  def validateMaxBookAhead(tripRequest: TripRequest): F[Unit] = {
    val maxBookAhead = 90.days
    val maxStart = Instant.now().plus(maxBookAhead)
    val tripStart = Instant.ofEpochSecond(tripRequest.startDate)
    if (tripStart.isAfter(maxStart))
      FR.raise(StartTooLate(tripRequest.startDate))
    else Applicative[F].unit
  }

  def validateMaxBookTime(tripRequest: TripRequest): F[Unit] = {
    val maxBookTime = 31
    val tripLength = ChronoUnit.DAYS.between(
      Instant.ofEpochSecond(tripRequest.startDate),
      Instant.ofEpochSecond(tripRequest.endDate)
    )
    if (tripLength > maxBookTime)
      FR.raise(TripTooLong(tripLength))
    else Applicative[F].unit
  }
}
