package domain.trips

import cats.MonadError
import cats._
import cats.data._
import cats.syntax.all._
import endpoint.{HttpErrorHandler, RoutesHttpErrorHandler}
import io.circe.literal.JsonStringContext
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, Response}
import org.http4s.circe.CirceEntityCodec._
import io.circe.syntax._
import io.circe.generic.auto._
import org.typelevel.log4cats.Logger

case class ErrorResponse(msg: String, price: Option[Int] = None)

class TripHttpErrorHandler[F[_]: Logger](implicit M: MonadError[F, TripError])
    extends HttpErrorHandler[F, TripError]
    with Http4sDsl[F] {
  private val handler: TripError => F[Response[F]] = {
    case InvalidTrip(msg) => BadRequest(ErrorResponse(msg).asJson)
    case CarIsReservedForDate(msg) =>
      Conflict(ErrorResponse(msg).asJson)
    case TripNotFound(id) => NotFound(ErrorResponse(id).asJson)
    case PriceMismatch(serverPrice) => {
      Logger[F].error("Price mismatch: " + serverPrice) *>
        ExpectationFailed(ErrorResponse("", price = Some(serverPrice)))
    }
    case StartTooLate(date) =>
      ExpectationFailed(ErrorResponse(""))
    case TripTooLong(date) =>
      ExpectationFailed(ErrorResponse(""))
    case BadUserTrip(userID) =>
      Forbidden("")
    case TripImmutable(tripID) =>
      Locked(ErrorResponse(""))
    case NoInspection(tripID) =>
      ExpectationFailed(ErrorResponse("NoInspection").asJson)
    case TripInspectionExists(tripID) =>
      Logger[F].error(s"Inspection for trip: $tripID already exists") *>
        Conflict(ErrorResponse("Already Inspected").asJson)
  }

  override def handle(routes: HttpRoutes[F]): HttpRoutes[F] =
    RoutesHttpErrorHandler(routes)(handler)
}
