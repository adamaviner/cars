package domain.payment

import cats.Applicative
import cats.mtl.Raise
import cats._
import cats.data._
import cats.syntax.all._
import com.stripe.exception.CardException
import domain.cars.{Car, CarListing}

import java.time.Instant
import java.time.temporal.ChronoUnit
import domain.trips.PriceWithDiscount
import domain.users.UserModel.UserID
import domain.users.User
import domain.trips.Trip

sealed trait PaymentError extends Exception

case class PaymentNotFound(msg: String) extends PaymentError

case class IntentError(msg: String) extends PaymentError

case class InvalidStatus(msg: String) extends PaymentError

case class AlreadyCharged(msg: String) extends PaymentError

case class AlreadyCaptured(msg: String) extends PaymentError

case class NoCustomer(userID: UserID) extends PaymentError

case class UserNotAuthorized(userID: UserID) extends PaymentError

case class InsufficientFunds(availableFunds: Float) extends PaymentError

case class NoPaymentMethods(userID: String) extends PaymentError

case class AuthRequired(msg: String) extends PaymentError

case class StripeCardException(msg: String) extends PaymentError

case class BadUserTrip(msg: String) extends PaymentError

case class DepositExists(paymentID: String) extends PaymentError

sealed trait IdentityError extends PaymentError

class PaymentValidator[F[_]: Applicative](implicit FR: Raise[F, PaymentError]) {

  def validateUser(user: User, trip: Trip): F[Unit] = {
    if (trip.userID != user.id) FR.raise(UserNotAuthorized(user.id))
    else Applicative[F].unit
  }
//  def validatePrice(car: Car, tripRequest: TripRequest): F[Unit] = {
//    val price = calculatePrice(car, tripRequest)
//    if (price.neqv(tripRequest.price))
//      FR.raise(PriceMismatch(price))
//    Applicative[F].unit
//  }
}
