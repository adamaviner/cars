package domain.payment

import com.google.cloud.firestore.annotation.PropertyName
import domain.users.UserModel.UserID
import io.scalaland.chimney.dsl.TransformerOps

import scala.beans.BeanProperty
import com.stripe.model.PaymentIntent
import enumeratum._
import com.stripe.model.PaymentMethod

sealed trait PaymentType extends EnumEntry
case object PaymentType extends Enum[PaymentType] with CirceEnum[PaymentType] {
  case object Charge extends PaymentType
  case object Hold extends PaymentType
  val values: IndexedSeq[PaymentType] = findValues
}

case class Payment(
    id: String = "",
    tripID: String = "",
    userID: UserID = "",
    customerID: String = "",
    paymentID: String = "",
    amount: Int = 0, //in cents
    collateralAmount: Int = 0, //in cents
    paymentType: PaymentType = PaymentType.Charge,
    requiresAction: Boolean = false,
    paymentIntent: Option[PaymentIntent] = None,
    clientSecret: Option[String] = None,
    refunded: Boolean = false,
) {
  def this() {
    this("")
  }

  def asBean: PaymentBean =
    this
      .into[PaymentBean]
      .withFieldComputed(_.paymentType, _.paymentType.entryName)
      .withFieldComputed(_.clientSecret, _.clientSecret.getOrElse(""))
      .transform
}

case class PaymentBean(
    @BeanProperty id: String = "",
    @BeanProperty tripID: String = "",
    @BeanProperty userID: UserID = "",
    @BeanProperty customerID: String = "",
    @BeanProperty paymentID: String = "",
    @BeanProperty amount: Int = 0, //in cents
    @BeanProperty collateralAmount: Int = 0, //in cents
    @BeanProperty paymentType: String = PaymentType.Charge.entryName,
    @BeanProperty requiresAction: Boolean = false,
    @BeanProperty clientSecret: String = "",
    @BeanProperty refunded: Boolean = false,
) {
  def this() {
    this("")
  }

  def asBase: Payment =
    this
      .into[Payment]
      .withFieldComputed(_.paymentType, p => PaymentType.withName(p.paymentType))
      .withFieldComputed(
        _.clientSecret,
        p =>
          if (clientSecret.isEmpty) None
          else Some(clientSecret)
      )
      .transform
}

case class DeferredPayment(
    @BeanProperty id: String = "",
    @BeanProperty tripID: String = "",
    @BeanProperty userID: UserID = "",
    @BeanProperty amount: Int = 0, //in cents
    @BeanProperty collateralAmount: Int = 0, //in cents
    @BeanProperty dispatchTime: Long = 0, // epoch seconds
    @BeanProperty sentCount: Long = 0, // how many times a request for payment has been sent
    @BeanProperty dispatched: Boolean = false,
    @BeanProperty paymentIntentID: String = ""
) {
  def this() {
    this("")
  }
}

case class PaymentAction(
    secret: String = "",
    requiresAuthentication: Boolean = true,
    requiresPaymentMethod: Boolean = false
)

case class ChargeResponse(
    chargeTime: Long = 0,
    paymentAction: Option[PaymentAction] = None,
)

case class ChargeRequest(
    tripID: String,
    paymentMethodID: Option[String] = None,
)

sealed trait IncidentalCause extends EnumEntry
case object IncidentalCause extends Enum[IncidentalCause] with CirceEnum[IncidentalCause] {
  case object Miles extends IncidentalCause
  case object Gas extends IncidentalCause
  case object Clean extends IncidentalCause
  case object Manual extends IncidentalCause
  val values: IndexedSeq[IncidentalCause] = findValues
}
case class IncidentalCharge(
    amount: Int,
    overage: Int,
    description: String,
    cause: IncidentalCause
) {

  def asBean: IncidentalChargeBean =
    this
      .into[IncidentalChargeBean]
      .withFieldComputed(_.cause, b => b.cause.entryName)
      .transform
}

case class IncidentalChargeBean(
    @BeanProperty amount: Int = 0,
    @BeanProperty overage: Int = 0,
    @BeanProperty description: String = "",
    @BeanProperty cause: String = IncidentalCause.Manual.entryName
) {
  def this() {
    this(0)
  }

  def asBase: IncidentalCharge =
    this
      .into[IncidentalCharge]
      .withFieldComputed(_.cause, b => IncidentalCause.withName(b.cause))
      .transform
}

case class StripeInfo(
    @BeanProperty customerID: String = "",
    @BeanProperty cardHoldID: String = "",
    @BeanProperty paymentID: String = "",
    @BeanProperty userID: UserID = ""
) {
  def this() {
    this("")
  }
}

case class CardDetails(
    id: String,
    brand: String,
    expMonth: Int,
    expYear: Int,
    last4: String
)

object CardDetails {
  import com.stripe.model.PaymentMethod.Card
  def fromCard(id: String, card: Card): CardDetails = {
    CardDetails(
      id,
      card.getBrand(),
      card.getExpMonth().toInt,
      card.getExpYear().toInt,
      card.getLast4()
    )
  }
}
