package domain.payment

import cats._
import cats.data._
import cats.syntax.all._
import cats.MonadError
import endpoint.{HttpErrorHandler, RoutesHttpErrorHandler}
import io.circe.literal.JsonStringContext
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, Response}
import org.http4s.circe.CirceEntityCodec._
import io.circe.syntax._
import io.circe.generic.auto._
import org.typelevel.log4cats.Logger

case class ErrorResponse(msg: String, price: Option[Int] = None)

class PaymentHttpErrorHandler[F[_]: Logger](implicit M: MonadError[F, PaymentError])
    extends HttpErrorHandler[F, PaymentError]
    with Http4sDsl[F] {
  private val handler: PaymentError => F[Response[F]] = {
    case PaymentNotFound(msg) => BadRequest(ErrorResponse(msg).asJson)
    case NoPaymentMethods(user) =>
      Logger[F].error("no payment method for: " + user) *>
        NotFound(ErrorResponse("No payment method").asJson)
    case StripeCardException(msg) =>
      Logger[F].error(msg) *>
        PaymentRequired(ErrorResponse("Your card was declined, please try a different card").asJson)
    case BadUserTrip(msg) =>
      ExpectationFailed(ErrorResponse("not a trip of this user").asJson)
    case NoCustomer(msg) =>
      NotFound(ErrorResponse("No customer for user").asJson)
    case AlreadyCharged(msg) =>
      Conflict(ErrorResponse("Already charged").asJson)
    case AlreadyCaptured(id) =>
      Logger[F].error(s"already captured payment for trip $id") *>
        Conflict(ErrorResponse("Already captured").asJson)
    case DepositExists(id) =>
      Logger[F].error(s"hold $id exists for trip") *>
        Conflict("Hold exists")
    case InsufficientFunds(availableFunds) =>
      Logger[F].error(s"InsufficientFunds to pay for trip available: $availableFunds") *>
        PaymentRequired(ErrorResponse("Insufficient funds to pay for this trip").asJson)
    case AuthRequired(msg) =>
      Logger[F].error(s"AuthRequired for payment $msg") *>
        Forbidden(ErrorResponse("Authentication Required").asJson)
    case UserNotAuthorized(id) =>
      Logger[F].error(s"User $id not authorized") *>
        Forbidden(ErrorResponse("Authorization Required").asJson)
    case IntentError(msg) =>
      Logger[F].error(s"couldn't find intent $msg") *>
        InternalServerError(ErrorResponse("").asJson)
    case InvalidStatus(msg) =>
      Logger[F].error(s"Invalid status: $msg") *>
        InternalServerError(ErrorResponse("").asJson)
  }

  override def handle(routes: HttpRoutes[F]): HttpRoutes[F] =
    RoutesHttpErrorHandler(routes)(handler)
}
