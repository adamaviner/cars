package domain.human

import io.circe.{Decoder, HCursor, Json}

case class VerifyHumanRequest(
    phone: String = "",
    firstName: String = "",
    middleName: String = "",
    lastName: String = "",
    licenseNumber: String = "",
    expDate: String = "",
    birthDate: String = "",
    country: String = "",
    state: String = ""
) {
  def buildRequest(): HumanRequest = {
    HumanRequest(
      PersonInfo(
        PersonName(firstName, middleName, lastName),
        birthDate,
        DriversLicense(licenseNumber, state, country, IssuedDt = "", expDate)
      )
    )
  }
}

// microbilt api request
case class HumanRequest(
    PersonInfo: PersonInfo
)

case class PersonInfo(
    PersonName: PersonName,
    BirthDt: String,
    DriversLicense: DriversLicense
)

case class DriversLicense(
    LicenseNum: String,
    StateProv: String,
    Country: String,
    IssuedDt: String,
    ExpirationDt: String
)

case class PersonName(
    FirstName: String,
    MiddleName: String,
    LastName: String
)

//microbilt api response
object VerifyHumanResponse {
//    implicit val decodeMode: Decoder[VerifyHumanResponse] = (c: HCursor) => {
//      val cursor = c.downField("Subject").downArray.downField("Score")
//      for {
//        value <- cursor.downField("Value").as[Int]
//      } yield {
//        RangeFilter(ls.head, ls.last)
//      }
//    }

  import io.circe.generic.auto._
//  implicit val decodeMode: Decoder[VerifyHumanResponse] = (c: HCursor) =>
//    for {
//      score <- c.downField("Subject").downArray.downField("Score").as[Score]
//    } yield {
//      Score(score.Value, score.Factor, score.Alert)
//    }

  def parseJson(json: Json): Option[Score]  = {
    val cursor = json.hcursor.downField("Subject").downArray.downField("Score")
    if (cursor.failed)
      return None
    val value = cursor.get[Int]("Value")
    val factors = cursor.get[List[Factor]]("Factor")
    val alerts = cursor.get[List[Factor]]("Factor")
    Some(Score(value.getOrElse(0), factors.getOrElse(List()), alerts.getOrElse(List())))
  }

}

case class VerifyHumanResponse(
    Subject: List[Subject]
)

case class Subject(
    Score: Score
)

case class Score(
    Value: Int = 0,
    Factor: List[Factor] = List(),
    Alert: List[Factor] = List()
)

case class Factor(
    Code: String,
    Description: String
)
