package domain

import scala.beans.BeanProperty


case class Text (
  name: String = "",
  value: String = ""
)

case class CommonTexts (
  @BeanProperty protectionFullTooltip: String = "",
  @BeanProperty protectionPartialTooltip: String = "",
  @BeanProperty protectionWaiverTooltip: String = "",
  @BeanProperty chargeAgreement: String = "",
  @BeanProperty chargeExplanation: String = "",
  @BeanProperty chargeExplanationNoHold: String = "",
) {
  def this() =
    this("")
}
