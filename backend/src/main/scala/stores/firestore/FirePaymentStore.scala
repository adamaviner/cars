package stores.firestore

import cats.effect.{Async, ContextShift}
import cats.mtl.Raise
import cats.syntax.all._
import cats.{MonadError, Parallel}
import com.google.cloud.firestore.{FieldMask, Firestore, QuerySnapshot}
import domain.payment._
import domain.users.UserModel.UserID
import stores.firestore.FirestoreIO.{
  DocumentSnapshotUtilitiesWithError,
  FS,
  QuerySnapshotUtilities,
  QueryUtilities
}

import java.time.Clock.systemUTC
import java.time.{Clock, Instant}
import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters._

class FirePaymentStore[F[_]: Parallel](db: Firestore, clock: Clock = systemUTC)(implicit
    ec: ExecutionContext,
    F: Async[F],
    cs: ContextShift[F],
    ME: MonadError[F, Throwable],
    FR: Raise[F, PaymentError]
) {
  private val payments = db.collection("payments")
  private val deferred = db.collection("deferredPayments")
  private val stripe = db.collection("stripe")

  def userPayments(userID: UserID): F[List[Payment]] = {
    FS(payments.whereEqualTo("userID", userID).get)
      .flatMap(_.asList[PaymentBean].map(_.map(_.asBase)))
  }

  def userDeferred(userID: UserID): F[List[DeferredPayment]] = {
    FS(deferred.whereEqualTo("userID", userID).get)
      .flatMap(_.asList[DeferredPayment])
  }

  def getTripPayments(tripID: String): F[List[Payment]] = {
    FS(
      payments
        .whereEqualTo("tripID", tripID)
        .get
    ).flatMap(_.asList[PaymentBean].map(_.map(_.asBase)))
  }

  def getTripPayment(tripID: String): F[Payment] = {
    FS(
      payments
        .whereEqualTo("tripID", tripID)
        .get
    ).flatMap(
      _.asList[PaymentBean].map(
        _.map(_.asBase)
          .filter(!_.refunded) // ignore refunded payments here
          .headOption
      )
    ).flatMap(paymentOpt =>
      if (paymentOpt.isEmpty) FR.raise(PaymentNotFound("No Payment")) else paymentOpt.get.pure[F]
    )
  }

  def markPaymentAsRefunded(paymentId: String): F[Unit] = {
    FS(payments.document(paymentId).update("refunded", true))
      .flatMap(_ => F.unit)
  }

  def markPaymentsAsDone(donePayments: List[Payment]): F[Unit] = {
    val ids = donePayments.map(_.id)
    FS {
      val batch = db.batch()
      ids.foreach(id => batch.update(payments.document(id), "requiresAction", false))
      batch.commit
    }.flatMap(_ => F.unit)
  }

  def markPaymentAsDone(donePayment: Payment): F[Unit] = {
    FS(payments.document(donePayment.id).update("requiresAction", false))
      .flatMap(_ => F.unit)
  }

  def clearPayments(tripID: String): F[Unit] = {
    val nonFinalPayments = getTripPayments(tripID)
      .map(_.filter(payment => payment.requiresAction))

    nonFinalPayments
      .flatMap(nonFinal => {
        FS {
          val batch = db.batch()
          nonFinal.foreach(p => batch.delete(payments.document(p.id)))
          batch.commit
        }
      })
      .flatMap(_ => F.unit)
  }

  def add(payment: Payment): F[Payment] = {
    val addedDocRef = payments.document()
    val paymentWithID = payment.copy(id = addedDocRef.getId)
    FS(payments.document(paymentWithID.id).set(paymentWithID.asBean)).map(_ => paymentWithID)
  }

  def delete(payment: Payment): F[Unit] =
    FS(payments.document(payment.id).delete()).flatMap(_ => F.unit)

  def capture(payment: Payment, amount: Int): F[Unit] = {
    for {
      _ <- FS(
        payments.document(payment.id).set(payment.copy(amount = amount).asBean)
      )
    } yield {}
  }

  def setStripeId(userID: UserID, stripeID: String): F[Unit] = {
    FS(stripe.document(userID).set(Map("stripeID" -> stripeID).asJava)).flatMap(_ => F.unit)
  }

  def getStripeId(userID: UserID): F[String] = {
    FS(
      stripe
        .document(userID)
        .get(FieldMask.of("stripeID"))
    )
      .flatMap(id => {
        if (!id.exists()) FR.raise(NoCustomer(userID))
        else id.getField[String]("stripeID")
      })
  }
}
