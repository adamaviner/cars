package stores.firestore

import cats.MonadError
import cats.data.OptionT
import cats.effect.{Async, ContextShift}
import cats.mtl.Raise
import cats.syntax.all._
import com.google.cloud.Timestamp
import com.google.cloud.firestore.{CollectionReference, Firestore}
import domain.auth.AuthHelpers.Role
import domain.users.UserModel.UserID
import domain.users._
import stores.firestore.FirestoreIO.{DocumentSnapshotUtilitiesWithError, FS}
import tsec.authentication.IdentityStore
import domain.auth.{MailVerification, PasswordReset, Sms, Token, TokenType}

import java.time.Instant
import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters._

class FireTokenStore[F[_]](
  db: Firestore)(
  implicit ec: ExecutionContext, F: Async[F], cs: ContextShift[F],
  ME: MonadError[F, Throwable],
  FR: Raise[F, UserError]
) {
  private val smsTokens = db.collection("smsTokens")
  private val resetTokens = db.collection("resetTokens")
  private val mailTokens = db.collection("mailTokens")

  private def collectionForType(tokenType: TokenType): CollectionReference =
    tokenType match {
      case Sms => smsTokens
      case PasswordReset => resetTokens
      case MailVerification => mailTokens
    }

  private def find(collection: CollectionReference, userID: Option[UserID], token: String): F[Option[Token]] = {
    val now = Timestamp.now()
    FS{
      val query = collection
      .whereEqualTo("token", token)
      val queryWithUser = if (userID.nonEmpty) query.whereEqualTo("userID", userID.get) else query
      queryWithUser.get
    }
      .map(_.getDocuments.asScala.toList)
      .flatMap(docs =>
        if (docs.isEmpty)
          none[Token].pure
        else
          docs
            .traverse(_.as[Token])
            .map(_.find(token => token.expiry.compareTo(now) == 1))
      )
  }

  def find(userID: UserID, token: String, tokenType: TokenType): F[Token] = {
    val coll = collectionForType(tokenType)
    find(coll, Some(userID), token).flatMap {
      case None => FR.raise(TokenNotFound(userID))
      case Some(token) => token.pure
    }
  }

  def find(token: String, tokenType: TokenType): F[Token] = {
    val coll = collectionForType(tokenType)
    find(coll, None, token).flatMap {
      case None => FR.raise(TokenNotFound(token))
      case Some(token) => token.pure
    }
  }

  def add(token: Token, tokenType: TokenType): F[Token] = {
    val coll = collectionForType(tokenType)
    val addedDocRef = coll.document()
    val tokenWithID = token.copy(id = addedDocRef.getId)
    FS(coll.document(addedDocRef.getId).set(tokenWithID)).map(_ => tokenWithID)
  }

  def discard(tokenID: String, tokenType: TokenType): F[Unit] = {
    val coll = collectionForType(tokenType)
    FS(coll.document(tokenID).delete()).flatMap(_ => F.unit)
  }
}



