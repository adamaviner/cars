package stores.firestore

import cats.{MonadError, Parallel}
import cats.effect.{Async, ContextShift}
import cats.mtl.Raise
import cats.syntax.all._
import cats.implicits
import com.google.api.core.ApiFuture
import com.google.cloud.firestore.{
  CollectionReference,
  FieldMask,
  Firestore,
  Query,
  QuerySnapshot,
  WriteResult
}
import com.google.cloud.storage.{Blob, Bucket, StorageException}
import domain.cars._
import domain.trips._
import domain.users.UserModel.UserID
import org.typelevel.log4cats.Logger
import stores.firestore.FirestoreIO.{
  DocumentSnapshotUtilitiesWithError,
  QuerySnapshotUtilities,
  FS,
  QueryUtilities
}

import java.io.InputStream
import java.time.temporal.ChronoUnit
import java.time.{Instant, ZoneId, ZoneOffset, ZonedDateTime}
import java.util
import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters._
import scala.reflect.ClassTag
import scala.util.Random
import com.google.cloud.firestore.FieldValue
import com.google.cloud.firestore.DocumentSnapshot

class FireTripStore[F[_]: Logger: Parallel](db: Firestore)(implicit
    ec: ExecutionContext,
    F: Async[F],
    cs: ContextShift[F],
    ME: MonadError[F, Throwable],
    FR: Raise[F, TripError]
) {
  private val trips = db.collection("trips")
  private val archivedTrips = db.collection("archivedTrips")
  private val inspections = db.collection("inspections")
  private val maxTurnoverTime = 24 // hours
  private val reservationExpiry = 60 * 15 // 15 minutes

  def get(id: String): F[Trip] = {
    FS(trips.document(id).get)
      .flatMap(ds => dsAsTrip(id, ds))
  }

  def getOrArchived(id: String): F[Trip] = {
    get(id).handleErrorWith { case e: TripNotFound =>
      FS(archivedTrips.document(id).get).flatMap(ds => dsAsTrip(id, ds))
    }
  }

  private def dsAsTrip(id: String, ds: DocumentSnapshot): F[Trip] = {
    if (!ds.exists) FR.raise(TripNotFound(id))
    else ds.as[TripBean].map(_.asBase)
  }

  private def qsAsTripList(qs: QuerySnapshot) =
    qs.asList[TripBean].map(_.map(_.asBase))

  def addFields(): F[Unit] = {
    FS(trips.get)
      .flatMap(qs =>
        qs.getDocuments()
          .asScala
          .toList
          .map(_.getId())
          .parTraverse(id =>
            FS {
              trips
                .document(id)
                .update("inspected", FieldValue.delete)
            }
          )
      )
      .flatMap(_ => F.unit)
  }

  def getTripsToInspect(): F[List[Trip]] = {
    for {
      pre <- getTripsToPreInspect()
      post <- getTripsToPostInspect()
    } yield pre.concat(post)
  }

  def getTripsToPreInspect(): F[List[Trip]] = {
    FS(
      trips
        .whereEqualTo("canceled", false)
        .whereGreaterThanOrEqualTo("startDate", Instant.now.getEpochSecond)
        .orderBy("startDate", Query.Direction.ASCENDING)
        .whereEqualTo("preInspected", false)
        .get
    ).flatMap { qs =>
      qsAsTripList(qs).map(_.filter(_.userID != MaintenanceTrip.userID))
    }
  }

  def getTripsToPostInspect(): F[List[Trip]] = {
    FS(
      trips
        .whereEqualTo("canceled", false)
        .whereLessThanOrEqualTo("endDate", Instant.now.getEpochSecond)
        .orderBy("endDate", Query.Direction.DESCENDING)
        .whereEqualTo("postInspected", false)
        .get
    ).flatMap { qs =>
      qsAsTripList(qs).map(_.filter(_.userID != MaintenanceTrip.userID))
    }
  }

  private def set(trip: Trip): F[Trip] = {
    val addedDocRef = trips.document()
    val tripWithId = trip.copy(id = addedDocRef.getId)
    FS {
      addedDocRef.set(tripWithId.asTripBean)
    }.map(_ => tripWithId)
  }

  def add(trip: Trip): F[Trip] = {
    for {
      _ <- assertAvailable(trip.asTripRequest)
      tripResult <- set(trip)
    } yield tripResult
  }

  def update(trip: Trip): F[Trip] = {
    for {
      _ <- assertAvailableForUpdate(trip.asTripUpdateRequest)
      tripWithTime = trip.copy(timestamp = Instant.now().getEpochSecond)
      res <- FS(
        trips
          .document(trip.id)
          .set(tripWithTime.asTripBean)
      ).map(_ => tripWithTime)
    } yield res
  }

  def sentInspectionMail(trip: Trip): F[Unit] = {
    FS(
      trips
        .document(trip.id)
        .update("inspectionMailsSent", trip.inspectionMailsSent + 1)
    ).flatMap(_ => F.unit)
  }

  def assertNotInspected(inspection: TripInspection): F[Unit] = {
    FS(
      trips
        .document(inspection.tripID)
        .get(FieldMask.of(inspection.inspectedField))
    ).flatMap(ds => {
      val hasInspection = ds.getBoolean(inspection.inspectedField)
      if (hasInspection) FR.raise(TripInspectionExists(inspection.tripID))
      else F.unit
    })
  }

  def assertNotPostInspected(tripID: String): F[Unit] = {
    assertNotInspected(new TripInspection(tripID, inspectionType = InspectionType.Post))
  }

  def assertCanInspect(trip: Trip, inspection: TripInspection): F[Unit] = {
    if (canInspect(trip, inspection)) F.unit
    else FR.raise(TripInspectionExists(trip.id))
  }

  def canInspect(trip: Trip, inspection: TripInspection): Boolean = {
    inspection.inspectionType match {
      case InspectionType.Pre  => !trip.preInspected && !trip.postInspected
      case InspectionType.Post => !trip.postInspected
    }
  }

  def attachInspection(inspection: TripInspection): F[Unit] = {
    FS {
      trips.document(inspection.tripID).update(inspection.inspectedField, true)
      if (inspection.inspectionType == InspectionType.Post)
        trips.document(inspection.tripID).update("active", false)
      inspections.document().set(inspection.copy(timestamp = Instant.now.getEpochSecond).asBean)
    }.flatMap(_ => F.unit)
  }

  def listInspections(): F[List[TripInspection]] = {
    FS(inspections.get).flatMap(qs => {
      val docs = qs.getDocuments.asScala.toList
      docs.traverse(ds => ds.as[TripInspectionBean].map(_.asBase))
    })
  }

  def getInspection(tripID: String, inspectionType: InspectionType): F[TripInspection] = {
    FS(
      inspections
        .whereEqualTo("tripID", tripID)
        .whereEqualTo("inspectionType", inspectionType.entryName)
        .get
    )
      .flatMap { qs =>
        val results = qs.asList[TripInspectionBean]
        results.flatMap(inspections =>
          if (inspections.isEmpty)
            TripInspection(tripID = tripID, inspectionType = inspectionType).pure
          else inspections.sortBy(_.timestamp).reverse.head.asBase.pure
        )
      }
  }

  def getInspectionID(tripID: String, inspectionType: InspectionType): F[String] = {
    FS(
      inspections
        .whereEqualTo("tripID", tripID)
        .whereEqualTo("inspectionType", inspectionType.entryName)
        .get
    )
      .flatMap { qs =>
        val results = qs.getDocuments
        if (results.size != 1) FR.raise(NoInspection(tripID))
        else results.get(0).getId.pure
      }
  }

  def setInspectionPhotos(inspectionID: String, links: List[String]): F[Unit] =
    FS(inspections.document(inspectionID).update("photos", links.asJava)).flatMap(_ => F.unit)

  def addInspectionPhotos(
      tripID: String,
      inspectionType: InspectionType,
      links: List[String]
  ): F[List[String]] =
    for {
      id <- getInspectionID(tripID, inspectionType)
      _ <- setInspectionPhotos(id, links)
    } yield links

  def payed(tripID: String): F[Unit] = {
    FS(trips.document(tripID).update("wasPaid", true))
      .flatMap(_ => F.unit)
  }

  def schedule(request: TripRequest): F[Trip] = {
    add(request.asTrip)
  }

  def scheduleMaintenance(request: MaintenanceRequest): F[Trip] = {
    val trip = MaintenanceTrip.fromRequest(request)
    val maintainerID = "CURTTUeYkdpQeUx5e6qt"
    add(trip.copy(userID = maintainerID))
  }

  def updateMaintenance(request: MaintenanceRequest): F[Trip] = {
    val trip = MaintenanceTrip.fromRequest(request)
    update(trip)
  }

  def markInactive(ids: List[String]): F[Unit] = {
    FS {
      val batch = db.batch()
      ids.foreach(id => batch.update(trips.document(id), "active", false))
      batch.commit
    }.flatMap(_ => F.unit)
  }

  def archive(id: String): F[Unit] = {
    FS(trips.document(id).get())
      .flatMap(_.as[TripBean])
      .flatMap(trip => {
        FS {
          archivedTrips.document(id).set(trip)
          trips.document(id).delete
        }
      })
      .flatMap(_ => F.unit)
  }

  def archive(toArchive: List[Trip]): F[Unit] = {
    FS {
      val batch = db.batch()
      toArchive.foreach(trip => {
        archivedTrips.document(trip.id).set(trip.asTripBean)
        trips.document(trip.id).delete
        //TODO delete non booked trips or handle in another way? maybe move to pending and retry if user tries to access
      })
      batch.commit
    }.flatMap(_ => F.unit)
  }

  def clear(): F[Unit] = {
    val a = FS(trips.get).map(_.getDocuments().asScala.toList.map(_.getId()))
    a.flatMap(ids => delete(ids))
  }

  def delete(ids: List[String]): F[Unit] = {
    FS {
      val batch = db.batch()
      ids.foreach(id => batch.delete(trips.document(id)))
      batch.commit
    }.flatMap(_ => F.unit)
  }

  def cancel(id: String): F[Unit] = {
    FS(
      trips
        .document(id)
        .update(
          Map[String, Object](
            "canceled" -> Boolean.box(true),
            "active" -> Boolean.box(false)
          ).asJava
        )
    )
      .flatMap(_ => F.unit)
  }

  def list(limit: Int, offset: Int): F[List[Trip]] = {
    FS(trips.orderBy("startDate").offset(offset).limit(limit).get).flatMap(qs => {
      val docs = qs.getDocuments.asScala.toList
      docs.traverse(ds => ds.as[TripBean].map(_.asBase))
    })
  }
  
  def listAll(): F[List[Trip]] = {
    FS(trips.orderBy("startDate").get).flatMap(qs => {
      val docs = qs.getDocuments.asScala.toList
      docs.traverse(ds => ds.as[TripBean].map(_.asBase))
    })
  }

  def assertAvailableForUpdate(request: TripUpdateRequest): F[Unit] = {
    implicit val ev: TripIdMask = TripIdMask()
    conflictingTrips[TripIdMask](request.startDate, request.endDate, Some(request.vin))
      .flatMap { trips =>
        if (
          trips.size == 1 && trips.head.id == request.id
        ) // ignore self trip in update conflict search
          F.unit
        else if (trips.nonEmpty)
          FR.raise(CarIsReservedForDate(request.vin))
        else F.unit
      }
  }

  def assertAvailable(request: TripRequest): F[Unit] = {
    implicit val ev: TripIdMask = TripIdMask()
    conflictingTrips[TripIdMask](request.startDate, request.endDate, Some(request.vin))
      .flatMap { trips =>
        if (trips.nonEmpty)
          FR.raise(CarIsReservedForDate(request.vin))
        else F.unit
      }
  }

  /** @param fromDate  from date
    * @param untilDate until date
    * @return list of vin's of cars that are reserved in this time span
    */
  def reservedCars(fromDate: Long, untilDate: Long): F[List[String]] = {
    implicit val ev: TripVinMask = TripVinMask()
    conflictingTrips[TripVinMask](fromDate, untilDate, vin = None)
      .map(trips => trips.map(_.vin))
  }

  def listUserTrips(userID: UserID): F[List[Trip]] = {
    FS(
      trips
        .whereEqualTo("userID", userID)
        .orderBy("startDate")
        .get
    ).flatMap { qs =>
      qsAsTripList(qs)
    }
  }

  def listPastUserTrips(userID: UserID): F[List[Trip]] = {
    import com.google.cloud.firestore.Query
    FS(
      trips
        .whereEqualTo("userID", userID)
        .whereLessThanOrEqualTo("endDate", Instant.now.getEpochSecond)
        .get
    ).flatMap { qs =>
      qsAsTripList(qs)
    }.map(trips => trips.sortBy(_.endDate).reverse)
  }

  def listFutureUserTrips(userID: UserID): F[List[Trip]] = {
    FS(
      trips
        .whereEqualTo("userID", userID)
        .whereGreaterThanOrEqualTo("endDate", Instant.now.getEpochSecond)
        .get
    ).flatMap { qs =>
      qsAsTripList(qs)
    }.map(trips => trips.filter(trip => trip.booked || !trip.expired()).sortBy(_.startDate))
  }

  def listCarTrips(id: String): F[List[Trip]] = {
    FS(
      trips
        .whereEqualTo("vin", id)
        .orderBy("startDate")
        .get
    ).flatMap { qs =>
      val docs = qs.getDocuments.asScala.toList
      docs.traverse(ds => ds.as[TripBean].map(_.asBase))
    }
  }

  def confirmBooking(tripID: String): F[Unit] = {
    FS(trips.document(tripID).update("booked", true)).flatMap(_ => F.unit)
  }

  def deactivateTrips(): F[Unit] = {
    val now = Instant.now().getEpochSecond()
    for {
      qs <- FS(
        trips
          .whereLessThanOrEqualTo("endDate", now - maxTurnoverTime)
          .selectEmpty
          .get
      )
      tripsToDeactivate = qs.asIDs
      _ <- FS {
        val batch = db.batch()
        tripsToDeactivate.foreach(entry => {
          batch.update(trips.document(entry), "active", false)
        })
        batch.commit
      }
    } yield {}
  }

  def archiveExpiredRequests(): F[Unit] = {
    val now = Instant.now().getEpochSecond
    val expiryTime =
      reservationExpiry + 5 * 60 //only delete 5 minutes after reservation end so as to not delete a trip that is being payed for
    for {
      qs <- FS(
        trips
          .whereLessThanOrEqualTo("timestamp", now - expiryTime)
          .whereEqualTo("booked", false)
          .get
      )
      tripsToArchive <- qsAsTripList(qs)
      _ <- archive(tripsToArchive)
    } yield {}
  }

  private def isBusinessTime(hour: Int): Boolean = {
    val businessStart = 10
    val businessEnd = 18
    hour >= businessStart && hour <= businessEnd
  }

  private def minusMaxTurnoverTime(date: Long): Long =
    Instant.ofEpochSecond(date).minus(24, ChronoUnit.HOURS).getEpochSecond

  private def getTurnoverTime(date: Long): Int = {
    val businessTimeTurnoverHours = 12
    val nonBusinessTimeTurnoverHours = 24
    val instant = Instant.ofEpochSecond(date)
    val zoneId = ZoneId.of(
      "America/Los_Angeles"
    ) //TODO hardcoded Los Angeles as cars don't have other locations yet
    val zonedDateTime = ZonedDateTime.ofInstant(instant, zoneId)
    zonedDateTime.toString
    val hourOfDay = zonedDateTime.getHour
    if (isBusinessTime(hourOfDay)) businessTimeTurnoverHours
    else nonBusinessTimeTurnoverHours
  }

  private def datePlusTurnover(date: Long): Long = {
    val instant = Instant.ofEpochSecond(date)
    val turnoverTime = getTurnoverTime(date)
    instant.plus(turnoverTime, ChronoUnit.HOURS).getEpochSecond
  }

  // returns the a TripDates representing when the car is not available for booking
  def booked(vin: String): F[List[TripDates]] = {
    val now = Instant.now.getEpochSecond
    FS {
      trips
        .whereEqualTo("active", true)
        .whereEqualTo("vin", vin)
        .whereGreaterThanOrEqualTo("endDate", minusMaxTurnoverTime(now))
        .select(TripDates.fields: _*)
        .get()
    }.flatMap(_.asList[TripDates])
      .map(tripDates => {
        tripDates
          .map(trip => trip.copy(endDate = datePlusTurnover(trip.endDate)))
          .filter(trip => trip.endDate >= now)
      })
  }

  private def conflictingEnds(vin: Option[String], fromDate: Long): F[List[String]] = {
    FS {
      val query = trips
        .whereEqualTo("active", true)
        .whereGreaterThanOrEqualTo("endDate", minusMaxTurnoverTime(fromDate))
      val queryOnlyVin = if (vin.isEmpty) query else query.whereEqualTo("vin", vin.get)
      queryOnlyVin.select(List("id", "endDate"): _*).get
    }.flatMap(qs => {
      val docs = qs.getDocuments.asScala.toList
      val tripIdsWithNones = docs.traverse(ds => {
        val endDate = ds.getField[Long]("endDate")
        endDate.flatMap(endDate =>
          if (datePlusTurnover(endDate) >= fromDate)
            ds.getField[String]("id")
          else "None".pure
        )
      })
      tripIdsWithNones.map(trips => trips.filter(_ != "None"))
    })
  }

  private def tripsQuery[T <: TripMask: ClassTag](
      vin: Option[String],
      transformQuery: Query => Query
  )(implicit T: TripMask): F[List[T]] = {
    FS {
      val query = transformQuery(trips).whereEqualTo("active", true)
      val queryOnlyVin = if (vin.isEmpty) query else query.whereEqualTo("vin", vin.get)
      queryOnlyVin.select(T.fields: _*).get
    }.flatMap(qs => {
      val docs = qs.getDocuments.asScala.toList
      docs.traverse(ds => ds.as[T])
    })
  }

  /** get all possibly conflicting trips where start date before untilDate + turnoverTime
    * get all possibly conflicting trips where end date + turnoverTime is after fromDate
    */
  private def conflictingTrips[T <: TripMask: ClassTag](
      fromDate: Long,
      untilDate: Long,
      vin: Option[String]
  )(implicit T: TripMask): F[List[T]] = {
    val conflictingStart = tripsQuery[T](
      vin,
      query => query.whereLessThanOrEqualTo("startDate", datePlusTurnover(untilDate))
    )

    val conflictingStartIds = conflictingStart.map(trips => trips.map(_.id))
    val conflictingEndIds = conflictingEnds(vin, fromDate)

    val conflictingIds = List(conflictingStartIds, conflictingEndIds).parSequence
      .map(results => {
        results.fold(results.head)((res, trips) => res.intersect(trips))
      })

    val inspect = for {
      ids <- conflictingIds
      trips <- conflictingStart
    } yield (ids, trips)

    for {
      ids <- conflictingIds
      trips <- conflictingStart
    } yield {
      ids.map(id => trips.find(_.id == id).get)
    }
  }
}
