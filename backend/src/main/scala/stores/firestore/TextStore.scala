package stores.firestore

import cats.effect.{Async, ContextShift}
import cats.syntax.all._
import cats.{MonadError, Parallel}
import com.google.cloud.firestore.Firestore
import domain.CommonTexts
import org.typelevel.log4cats.Logger
import stores.firestore.FirestoreIO.{DocumentSnapshotUtilitiesWithError, FS}

import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters._


class TextStore[F[_]: Logger: Parallel](db: Firestore)(implicit
  ec: ExecutionContext,
  F: Async[F],
  cs: ContextShift[F],
  ME: MonadError[F, Throwable]
) {
  private val texts = db.collection("texts")
  private val commonID = "common"
  private val textField = "text"

  def getCommons(): F[CommonTexts] = {
    FS(texts.document(commonID).get)
      .flatMap(ds => {
        ds.as[CommonTexts]
      })
  }

  def setCommons(text: CommonTexts): F[Unit] = {
    FS(texts.document(commonID).set(text)).flatMap(_ => F.unit)
  }

  def get(id: String): F[String] = {
    FS(texts.document(id).get)
      .flatMap(ds => {
        if (!ds.exists) "".pure
        else ds.getField[String](textField)
      })
  }

  def set(id: String, text: String): F[Unit] = {
    FS(texts.document(id).set(Map(textField -> text).asJava)).flatMap(_ => F.unit)
  }
}