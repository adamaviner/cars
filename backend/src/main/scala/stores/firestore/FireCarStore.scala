package stores.firestore

import cats.MonadError
import cats.effect.{Async, ContextShift}
import cats.mtl.Raise
import cats.syntax.all._
import com.google.cloud.firestore.{FieldMask, Firestore, WriteResult}
import com.google.cloud.storage.{Blob, StorageException}
import domain.SearchFilters
import domain.cars.{Car, CarBean, CarError, CarListing, CarListingMask, CarNotFound, CarVinExists}
import stores.firestore.FirestoreIO.{DocumentSnapshotUtilitiesWithError, FS}

import scala.concurrent.ExecutionContext
import scala.reflect.ClassTag
import scala.jdk.CollectionConverters._
import scala.util.Random
import org.typelevel.log4cats.Logger

import java.io.InputStream
import java.util
import domain.PriceAsc
import com.google.cloud.firestore.Query
import domain.Sort
import domain.PriceDesc
import domain.Relevance
import cats.instances.boolean

class FireCarStore[F[_]: Logger](db: Firestore)(implicit
    ec: ExecutionContext,
    F: Async[F],
    cs: ContextShift[F],
    ME: MonadError[F, Throwable],
    FR: Raise[F, CarError]
) {
  private val cars = db.collection("cars")
  private val imageURLsFieldName = "imageURLs"

  def getWithMask[T](vin: String, mask: FieldMask)(implicit ct: ClassTag[T]): F[T] = {
    FS(cars.document(vin).get(mask))
      .flatMap { ds =>
        if (!ds.exists) FR.raise(CarNotFound(vin))
        else ds.as[T]
      }
  }

  def getCar(vin: String): F[Car] = {
    FS(cars.document(vin).get)
      .flatMap { ds =>
        if (!ds.exists) FR.raise(CarNotFound(vin))
        else ds.as[CarBean].map(_.toBase)
      }
  }

  def getCarListing(vin: String): F[CarListing] = {
    val fieldMask = FieldMask.of(CarListingMask.fields: _*)
    getWithMask[CarListingMask](vin, fieldMask).map(_.toBase)
  }

  def getCarListings(vins: List[String]): F[List[CarListing]] = {
//    val fieldMask = FieldMask.of(CarListingMask.fields: _*)
    val docs = vins.map(vin => cars.document(vin))
    FS(db.getAll(docs: _*)) //TODO fieldMask
      .flatMap(docSnaps => docSnaps.asScala.toList.traverse(_.as[CarBean].map(_.toBase.toListing)))
  }

  def list(limit: Int, offset: Int, listDisabled: Boolean = false): F[List[CarListing]] = {
    FS(
      cars
        .select(CarListingMask.fields: _*)
        .offset(offset)
        .limit(limit)
        .whereEqualTo("disabled", listDisabled)
        .get
    ).flatMap(qs => {
      val docs = qs.getDocuments.asScala.toList
      docs.traverse(ds => ds.as[CarListingMask].map(_.toBase))
    })
  }

  private def applyFilters(cars: List[Car], filters: SearchFilters): List[Car] =
    cars.filter(car =>
      (filters.price.isEmpty ||
        filters.price.get.contains(car.price)) &&
        (filters.year.isEmpty ||
          filters.year.get.contains(car.year)) &&
        (filters.transmission.isEmpty ||
          filters.transmission.get == car.transmission)
    )

  private def addOrderBy(query: Query, sort: Option[Sort]): Query =
    if (sort.isEmpty) query
    else
      sort.get match {
        case PriceAsc  => query.orderBy("price")
        case PriceDesc => query.orderBy("price", Query.Direction.DESCENDING)
        case Relevance => query
      }

  def list(limit: Int, offset: Int, filters: SearchFilters): F[List[CarListing]] = {
    FS {
      val query = cars
        //      .select(CarListingMask.fields: _*)
        .offset(offset)
        .limit(limit)
      addOrderBy(query, filters.sort).get
    }
      .flatMap(qs => {
        val docs = qs.getDocuments.asScala.toList
        docs
          .traverse(ds => {
            ds.as[CarBean]
              .map(car => {
                val discount = car.discountRules.toBase.discount(filters.date.from, filters.date.to)
                val finalPrice = car.price * (1 - discount / 100)
                car.toBase.copy(price = finalPrice)
              })
          })
          .map(ls =>
            applyFilters(ls, filters)
              .map(car => car.toListing)
          )
      })
  }

  def exists(vin: String): F[Boolean] = {
    val fieldMask = FieldMask.of("vin")
    getWithMask[CarListingMask](vin, fieldMask)
      .map(_ => true)
      .handleError { case _: CarNotFound =>
        false
      }
  }

  def add(car: Car): F[Car] = {
    exists(car.vin).flatMap {
      case true  => FR.raise(CarVinExists(car.vin)) //TODO test
      case false => put(car)
    }
  }

  private def put(car: Car): F[Car] = {
    FS {
      cars.document(car.vin).set(car.toBean)
    }.map { _ =>
      car
    } //TODO test error handling
  }

  def disable(vin: String): F[Unit] = {
    FS(cars.document(vin).update("disabled", true)).flatMap(_ => F.unit)
  }

  def update(car: Car): F[Car] = {
    put(car)
  }

  def updateMiles(id: String, miles: Int): F[Unit] =
    FS(cars.document(id).update("miles", miles)).flatMap(_ => F.unit)

  def getAndMergePhotos(vin: String, photos: List[String]): F[List[String]] = {
    val query = FS(cars.document(vin).get(FieldMask.of(imageURLsFieldName)))

    // explicit type necessary to resolve types
    val urls: F[util.List[String]] = query.flatMap { ds =>
      if (!ds.exists) FR.raise(CarNotFound(vin))
      else ds.getField[util.List[String]](imageURLsFieldName)
    }
    urls.map(urls => (urls.asScala.toList ++ photos).distinct)
  }

  def addPhotos(vin: String, photos: List[String]): F[List[String]] = {
    getAndMergePhotos(vin, photos).flatMap(urls => {
      FS(cars.document(vin).update(imageURLsFieldName, urls.asJava)).map(_ => urls)
    })
  }
}

object FireCarStore {
  def apply[F[_]: Logger](db: Firestore)(implicit
      ec: ExecutionContext,
      F: Async[F],
      cs: ContextShift[F],
      ME: MonadError[F, Throwable],
      FR: Raise[F, CarError]
  ): FireCarStore[F] = {
    new FireCarStore[F](db)
  }
}
