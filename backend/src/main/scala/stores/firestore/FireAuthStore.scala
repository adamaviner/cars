package stores.firestore

import cats.data.{EitherT, OptionT}
import cats.effect.{Async, ContextShift}
import cats.syntax.all._
import com.google.cloud.firestore.Firestore
import domain.auth.BearerTokenCodec
import stores.firestore.FirestoreIO._
import tsec.authentication.{BackingStore, TSecBearerToken}

import scala.concurrent.ExecutionContext

object FireAuthStore {
  def apply[F[_] : Async : ContextShift, SI, I](db: Firestore, getId: TSecBearerToken[I] => I)(
    implicit ec: ExecutionContext)
  : BackingStore[F, SI, TSecBearerToken[I]] = new BackingStore[F, SI, TSecBearerToken[I]] {
    private val collectionName = "auth"

    override def get(id: SI): OptionT[F, TSecBearerToken[I]] = {
      val documentSnapshot = FS {
        db.collection(collectionName).document(id.toString).get()
      }

      val elem = documentSnapshot.map {
        ds =>
          val res = ds.asWithMap(BearerTokenCodec[I])
          res
      }

      EitherT(elem).toOption
    }

    override def put(elem: TSecBearerToken[I]): F[TSecBearerToken[I]] = {
      FS {
        db.collection(collectionName).document(getId(elem).toString)
          .set(BearerTokenCodec[I].toMap(elem))
      }.map(_ => elem)
    }

    override def update(v: TSecBearerToken[I]): F[TSecBearerToken[I]] = {
      put(v) //TODO change something?
    }

    override def delete(id: SI): F[Unit] = {
      Async.fromFuture(Async[F].delay {
        db.collection(collectionName).document(id.toString)
          .delete().asScala
      })
      Async[F].unit
    }
  }
}