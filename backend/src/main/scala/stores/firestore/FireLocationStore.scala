package stores.firestore

import cats.MonadError
import cats.data.OptionT
import cats.effect.{Async, ContextShift}
import cats.mtl.Raise
import cats.syntax.all._
import com.google.cloud.Timestamp
import com.google.cloud.firestore.{CollectionReference, Firestore}
import domain.auth.AuthHelpers.Role
import domain.users.UserModel.UserID
import domain.users._
import stores.firestore.FirestoreIO.{DocumentSnapshotUtilitiesWithError, FS, QuerySnapshotUtilities}
import tsec.authentication.IdentityStore
import domain.auth.{MailVerification, PasswordReset, Sms, Token, TokenType}

import java.time.Instant
import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters._
import domain.cars.Location

class FireLocationStore[F[_]](
  db: Firestore)(
  implicit ec: ExecutionContext, F: Async[F], cs: ContextShift[F],
  ME: MonadError[F, Throwable],
  FR: Raise[F, UserError]
) {
  private val locations = db.collection("locations")

  def list(): F[List[Location]] = {
    FS(locations.get).flatMap { qs =>
      val docs = qs.getDocuments.asScala.toList
      docs.traverse(ds => ds.as[Location])
    }
  }

  def add(location: Location): F[Location] = {
    val addedDocRef = locations.document()
    val locationWithId = location.copy(id = addedDocRef.getId)
    FS(locations.document(addedDocRef.getId).set(locationWithId)).map(_ => locationWithId)
  }

  def get(id: String): F[Location] = {
    FS(locations.document(id).get).flatMap(v => v.as[Location])
  }

  def update(location: Location): F[Location] = {
    FS(locations.document(location.id).set(location)).map(v => location)
  }

  def discard(id: String): F[Unit] = {
    FS(locations.document(id).delete()).flatMap(_ => F.unit)
  }
}



