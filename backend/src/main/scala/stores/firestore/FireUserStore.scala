package stores.firestore

import cats.MonadError
import cats.data.{EitherT, OptionT}
import cats.effect.{Async, Blocker, ConcurrentEffect, ContextShift, Sync}
import cats.mtl.Raise
import cats.syntax.all._
import com.google.cloud.firestore.{FieldMask, Firestore, WriteResult}
import com.google.cloud.storage.{Blob, Bucket, StorageException}
import domain.auth.AuthHelpers.Role
import domain.users.UserModel.UserID
import domain.users._
import org.typelevel.log4cats.Logger
import tsec.authentication.IdentityStore

import scala.concurrent.ExecutionContext
import scala.util.Random
import stores.firestore.FirestoreIO.{DocumentSnapshotUtilitiesWithError, FS, QuerySnapshotUtilities}

import java.io.InputStream
import scala.jdk.CollectionConverters._
import cats.Parallel
import domain.auth.AuthHelpers

class FireUserStore[F[_]: Parallel: ConcurrentEffect: Logger](
    db: Firestore,
    bucket: Bucket,
    blocker: Blocker
)(implicit
    ec: ExecutionContext,
    F: Async[F],
    cs: ContextShift[F],
    ME: MonadError[F, Throwable],
    FR: Raise[F, UserError]
) extends UserStore[F]
    with IdentityStore[F, UserID, User] {
  private val dbUsers = db.collection("users")
  private val verifications = db.collection("verifications")

  override def find(email: String): F[User] = {
    val a = FS(dbUsers.whereEqualTo("email", email).get)
      .map(_.getDocuments.asScala.toList)
    a.flatMap(docs =>
      if (docs.isEmpty)
        FR.raise(UserNotFound(email))
      else
        docs.head.as[UserBean].map(_.toBase)
    )
  }

  override def get(id: UserID): OptionT[F, User] = {
    OptionT.liftF(doGet(id))
  }

  def getRoleEmails(role: AuthHelpers.Role): F[List[String]] = {
    val field = "email"
    FS(dbUsers.whereEqualTo("role", role.roleRepr).select(field).get)
      .flatMap(qs => {
        qs.getDocuments.asScala.toList.traverse(_.getField[String](field))
      })
  }

  def getAdminEmails(): F[List[String]] = {
    getRoleEmails(AuthHelpers.Role.Admin)
  }

  def getCustodianEmails(): F[List[String]] = {
    getRoleEmails(AuthHelpers.Role.Custodian)
  }

  def getCustodianAndAdminEmails(): F[List[String]] = {
    val field = "email"
    val roles = List(AuthHelpers.Role.Admin, AuthHelpers.Role.Custodian)
    .map(_.roleRepr).asJava
    FS(dbUsers.whereIn("role", roles).select(field).get)
      .flatMap(qs => {
        qs.getDocuments.asScala.toList.traverse(_.getField[String](field))
      })
  }

  def doGet(id: UserID): F[User] = {
    FS(dbUsers.document(id).get)
      .flatMap(v => v.as[UserBean].map(_.toBase))
  }

  def exists(email: String): F[Boolean] = {
    find(email)
      .map(_ => true)
      .handleError { case _: UserNotFound =>
        false
      }
  }

  def verifyMailAvailable(mail: String): F[Unit] = {
    exists(mail)
      .flatMap {
        case true  => FR.raise(MailAlreadyExists(mail))
        case false => F.unit
      }
  }

  def verifyPhoneAvailable(phone: String): F[Unit] = {
    FS(
      dbUsers
        .whereEqualTo("phone", phone)
        // .whereEqualTo("validPhone", true) //TODO ask yaniv?
        .get
    )
      .flatMap(_.asList[UserBean])
      .map(_.nonEmpty)
      .flatMap {
        case true  => FR.raise(PhoneAlreadyExists(phone))
        case false => F.unit
      }
  }

  override def add(user: User): F[User] = {
    for {
      _ <- verifyPhoneAvailable(user.phone)
      _ <- verifyMailAvailable(user.email)
      res <- set(user)
    } yield res
  }

  override def delete(id: UserID): F[Unit] = {
    FS(dbUsers.document(id).delete()).flatMap(_ => F.unit)
  }

  override def deleteAll(ids: List[UserID]): F[Unit] = {
    FS {
      val batch = db.batch()
      ids.foreach(id => batch.delete(dbUsers.document(id)))
      batch.commit
    }.flatMap(_ => F.unit)
  }

  private def set(user: User): F[User] = {
    val addedDocRef = dbUsers.document()
    val userWithId = user.copy(id = addedDocRef.getId)
    set(addedDocRef.getId, userWithId)
  }

  private def set(userID: UserID, user: User): F[User] = {
    FS(dbUsers.document(userID).set(user.toBean)).map(_ => user)
  }

  private def getAndSet(id: UserID, transform: User => User): F[User] = {
    for {
      current <- doGet(id)
      res <- set(id, transform(current))
    } yield res
  }

  override def update(userUpdate: UserUpdate): F[User] = {
    getAndSet(userUpdate.id, user => userUpdate.merge(user))
  }

  override def update(user: User): F[User] = {
    if (user.id.isEmpty)
      FR.raise(InvalidUser("no id"))
    else FS(dbUsers.document(user.id).set(user.toBean)).map(_ => user)
  }

  private def cloudLicenseFilename(userID: UserID) = {
    s"licenses/$userID/license.png"
  }

//  override def uploadLicense(userID: UserID, is: InputStream, filename: Option[String]): F[User] = {
//    val name = cloudLicenseFilename(userID)
//    val blob = F.delay(bucket.create(name, is)).handleErrorWith {
//      case err: StorageException =>
//        ME.raiseError(err) //TODO more robust, maybe retry if fails
//    }
//
//    blob.flatMap { blob =>
//      update(UserUpdate(userID, licenseURL = Some(name)))
//    }
//  }

  def streamLicense(userID: UserID): fs2.Stream[F, Byte] = {
    val chunkSize = 2 * 1024
    val name = cloudLicenseFilename(userID)

    def getBlob: F[Blob] = {
      blocker.delay(bucket.get(name))
    }.handleErrorWith { case e: StorageException =>
      Logger[F].error(s"Error: $e \nwhile fetching blob for: $name") *>
        ME.raiseError(e)
    }

    fs2.Stream
      .eval(getBlob)
      .flatMap(blob =>
        fs2.io.readOutputStream(blocker, chunkSize)(outputStream =>
          blocker.delay(blob.downloadTo(outputStream))
        )
      )
  }

  override def makeAdmin(userID: UserID): F[Unit] = {
    FS(dbUsers.document(userID).update("role", Role.Admin)).flatMap(_ =>
      F.unit
    ) // TODO error handling
  }

  override def validatePhone(userID: UserID): F[Unit] = {
    FS(dbUsers.document(userID).update("validPhone", true)).flatMap(_ => F.unit)
  }

  def validateLicense(userID: UserID): F[Unit] = {
    FS(dbUsers.document(userID).update("validLicense", true)).flatMap(_ => F.unit)
  }

  def validateEmail(userID: UserID): F[Unit] = {
    FS(dbUsers.document(userID).update("validEmail", true)).flatMap(_ => F.unit)
  }

  override def list(limit: Int, offset: Int): F[List[User]] = {
    FS(dbUsers.offset(offset).limit(limit).get).flatMap { qs =>
      val docs = qs.getDocuments.asScala.toList
      docs.traverse(ds => ds.as[UserBean].map(_.toBase))
    }
  }

  def setVerificationSession(verification: Verification): F[Unit] = {
    deleteVerifications(verification.userID) *>
      FS(verifications.document(verification.sessionID).set(verification))
        .flatMap(_ => F.unit)
  }

  def deleteVerifications(userID: UserID): F[Unit] = {
    for {
      sessions <- findVerificationSessions(userID)
      _ <- sessions.parTraverse(s => FS(verifications.document(s.sessionID).delete))
    } yield {}
  }

  def verificationSession(id: String): F[Verification] = {
    FS(verifications.document(id).get).flatMap(_.as[Verification])
  }

  def findVerificationSession(userID: UserID): F[Option[Verification]] = {
    findVerificationSessions(userID).map(_.headOption)
  }

  def findVerificationSessions(userID: UserID): F[List[Verification]] = {
    FS(verifications.whereEqualTo("userID", userID).get)
      .flatMap(_.asList[Verification])
  }
}
