package stores.firestore

import cats.MonadError
import cats.effect.{Async, ContextShift}
import cats.syntax.all._
import com.google.cloud.firestore.{Firestore, WriteResult}
import domain.trips.{DiscountRules, DiscountRulesBean}
import org.typelevel.log4cats.Logger
import stores.firestore.FirestoreIO.{DocumentSnapshotUtilitiesWithError, FS}

import scala.concurrent.ExecutionContext

class FireDiscountStore[F[_] : Logger](
  db: Firestore)(implicit ec: ExecutionContext, F: Async[F], cs: ContextShift[F],
  ME: MonadError[F, Throwable]) {
  private val store = db.collection("discounts")

  private val defaultRules = get("defaultRules")

  def get(id: String): F[DiscountRules] = {
    FS(store.document(id).get)
      .flatMap { ds =>
        if (!ds.exists) defaultRules
        else ds.as[DiscountRulesBean].map(_.toBase)
      }
  }

  def add(item: DiscountRules): F[DiscountRules] = {
    put(item)
  }

  private def put(item: DiscountRules): F[DiscountRules] = {
    FS {
      store.document(item.vin).set(item.toBean)
    }.map { _ =>
      item
    }
  }

  def delete(id: String): F[WriteResult] = {
    FS(store.document(id).delete)
  }
}



