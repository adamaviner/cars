package stores.firestore

import cats.{Applicative, MonadError}
import cats.effect.{Async, BracketThrow, ContextShift, IO, Resource, Sync}
import com.google.api.core.{ApiFuture, ApiFutureCallback, ApiFutures}
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.firestore.{
  CollectionReference,
  DocumentSnapshot,
  Firestore,
  QueryDocumentSnapshot,
  QuerySnapshot
}
import com.google.cloud.storage.Bucket
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.cloud.{FirestoreClient, StorageClient}
import domain.MapCodec
import domain.payment.Payment
import domain.users.User

import java.io.FileInputStream
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.jdk.CollectionConverters._
import scala.reflect.ClassTag
import scala.util.Try
import com.google.cloud.firestore.Query

object FirestoreDB {
  val storageBucketName = "images_carrentals"

  def instance(): Firestore = {
    val firestoreTry = Try(FirestoreClient.getFirestore())
    if (firestoreTry.isSuccess)
      return firestoreTry.get

    val serviceAccountPath = "src/main/resources/serviceAccount.json"

    var fis: FileInputStream = null
    try {
      fis = new FileInputStream(serviceAccountPath)
      val credentials = GoogleCredentials.fromStream(fis)

      val options = FirebaseOptions
        .builder()
        .setCredentials(credentials)
        .build()

      FirebaseApp.initializeApp(options)
      FirestoreClient.getFirestore()
    } finally {
      if (fis != null) {
        fis.close()
      }
    }
  }

  def bucket: Bucket =
    StorageClient.getInstance.bucket(storageBucketName)

  def secureBucket: Bucket =
    StorageClient.getInstance.bucket("secure_carrentals")

  def apply[F[_]](
      serviceAccountPath: String = "src/main/resources/serviceAccount.json"
  )(implicit F: Sync[F]): Resource[F, Firestore] = {

    def openDB(): Firestore = {
      val options = FirebaseOptions
        .builder()
        .setProjectId("elite-epoch-308417")
        .setCredentials(GoogleCredentials.getApplicationDefault())
        .build()

      FirebaseApp.initializeApp(options)
      FirestoreClient.getFirestore()
    }

    Resource.eval(F.delay(openDB()))
  }
}

object FirestoreIO {
  implicit class ApiFutureDecorator[T](val f: ApiFuture[T]) {
    def asScala(implicit ec: ExecutionContext): Future[T] = {
      val p = Promise[T]()
      ApiFutures.addCallback(
        f,
        new ApiFutureCallback[T]() {
          override def onFailure(t: Throwable): Unit = p failure t

          override def onSuccess(result: T): Unit = p success result
        },
        (command: Runnable) => ec.execute(command)
      )
      p.future
    }
  }

  import cats.syntax.all._

  def FS[F[_], V](
      body: => ApiFuture[V]
  )(implicit ec: ExecutionContext, F: Async[F], cs: ContextShift[F]): F[V] = {
    Async.fromFuture(Async[F].delay(body).map(f => f.asScala))
  }

//  implicit class QueryDocumentSnapshotUtilities[F[_]: Applicative](val ds: QueryDocumentSnapshot) {
//    def as[T](implicit ct: ClassTag[T],
//      ME: MonadError[F, Throwable]): F[T] = {
//      try {
//        ds.toObject(ct.runtimeClass).asInstanceOf[T].pure[F]
//      } catch {
//        case e: RuntimeException => {
//          ME.raiseError(e)
//        }
//      }
//    }
//  }

  implicit class QueryUtilities[F[_]: Applicative](val qs: Query) {
    def selectEmpty: Query = {
      val empty: List[String] = List()
      qs.select(empty: _*)
    }
  }

  implicit class QuerySnapshotUtilities[F[_]: Applicative](val qs: QuerySnapshot) {
    def asList[T](implicit ct: ClassTag[T], ME: MonadError[F, Throwable]): F[List[T]] = {
      val docs = qs.getDocuments.asScala.toList
      docs.traverse(a => a.as[T])
    }

    def asIDs: List[String] = qs.getDocuments.asScala.toList.map(_.getId)
  }

  implicit class DocumentSnapshotUtilitiesWithError[F[_]: Applicative](val ds: DocumentSnapshot) {
    def as[T](implicit ct: ClassTag[T], ME: MonadError[F, Throwable]): F[T] = {
      try {
        ds.toObject(ct.runtimeClass).asInstanceOf[T].pure[F]
      } catch {
        case e: RuntimeException =>
          ME.raiseError(e)
      }
    }

    def getField[T](field: String)(implicit ct: ClassTag[T], ME: MonadError[F, Throwable]): F[T] = {
      try {
        ds.getData.get(field).asInstanceOf[T].pure[F]
      } catch {
        case e: RuntimeException =>
          ME.raiseError(e)
      }
    }

    def asOpt[T](implicit ct: ClassTag[T]): Option[T] = {
      try {
        Option(ds.toObject(ct.runtimeClass).asInstanceOf[T])
      } catch {
        case e: RuntimeException => {
          None
        }
      }
    }
  }

  implicit class DocumentSnapshotUtilities(val ds: DocumentSnapshot) {
//    def as[T](implicit ct: ClassTag[T]): Either[Exception, T] = {
//      if (!ds.exists())
//        return Left(new NoSuchElementException(s"No document in datastore: $ds"))
//      try {
//        Right(ds.toObject(ct.runtimeClass).asInstanceOf[T])
//      } catch {
//        case e: RuntimeException => Left(e)
//      }
//    }
//
//    def as[F[_]: Applicative, T](implicit ct: ClassTag[T],
//      ME: MonadError[F, Throwable]): F[T] = {
//      if (!ds.exists())
//        return ME.raiseError(new NoSuchElementException(s"No document in datastore: $ds"))
//      try {
//        ds.toObject(ct.runtimeClass).asInstanceOf[T].pure[F]
//      } catch {
//        case e: RuntimeException => ME.raiseError(e)
//      }
//    }

    def asMappable[T](implicit
        f: java.util.Map[String, AnyRef] => Option[T]
    ): Either[Exception, T] = {
      if (!ds.exists())
        return Left(new NoSuchElementException(s"No Element in ds: $ds"))
      try {
        val data = ds.getData
        val elem = f(data)
        elem match {
          case Some(e) => Right(e)
          case None    => Left(new ClassCastException(s"Couldn't parse data: $data"))
        }
      } catch {
        case e: RuntimeException => Left(e)
      }
    }

    def asWithMap[T](codec: MapCodec[T]): Either[Exception, T] = {
      if (!ds.exists())
        return Left(new NoSuchElementException(s"No Element in ds: $ds"))
      try {
        val data = ds.getData
        val elem = codec.fromMap(data)
        elem match {
          case Some(e) => Right(e)
          case None    => Left(new ClassCastException(s"Couldn't parse data: $data"))
        }
      } catch {
        case e: RuntimeException => Left(e)
      }
    }
  }

}
