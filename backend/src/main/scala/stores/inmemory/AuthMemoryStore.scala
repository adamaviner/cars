package stores.inmemory

import cats.data.OptionT
import cats.effect.Sync
import tsec.authentication._

import scala.collection.mutable

object AuthMemoryStore {
  def apply[F[_], I, V](getId: V => I)(implicit F: Sync[F]): BackingStore[F, I, V] = new BackingStore[F, I, V] {
    private val storageMap = mutable.HashMap.empty[I, V]

    override def put(elem: V): F[V] = {
      val storage = storageMap.put(getId(elem), elem)
      if (storage.isEmpty)
        F.pure(elem)
      else
        F.raiseError(new IllegalArgumentException)

    }

    override def update(v: V): F[V] = {
      storageMap.update(getId(v), v)
      F.pure(v)
    }


    override def delete(id: I): F[Unit] = {
      storageMap.remove(id) match {
        case Some(_) => F.unit
        case None => F.raiseError(new IllegalArgumentException)
      }
    }

    override def get(id: I): OptionT[F, V] =
      OptionT.fromOption[F](storageMap.get(id))
  }
}
