package stores.inmemory

import cats.data.{EitherT, OptionT}
import cats.effect.{Async, Sync}
import cats.mtl.Raise
import cats.syntax.all._
import domain.auth.AuthHelpers.Role
import domain.users.UserModel.UserID
import domain.users._
import tsec.authentication.IdentityStore

import java.io.InputStream
import scala.util.Random

class InmemoryUserStore[F[_]](implicit F: Sync[F], FR: Raise[F, UserError]) extends UserStore[F] with IdentityStore[F, UserID, User] {
  var state: Map[UserID, User] = Map()

  def addUser(userID: UserID, user: User): F[User] = {
    F.delay {
      val userWithID = user.copy(id = userID)
      state = state.updated(userID, user.copy(id = userID))
      userWithID
    }
  }

  override def find(email: String): F[User] = {
    F.delay(state.find(_._2.email == email).map(_._2)).flatMap {
      case Some(user) => user.pure
      case None => FR.raise(UserNotFound(email))
    }
  }

  override def get(id: UserID): OptionT[F, User] =
    OptionT(F.delay(state.get(id)))

  def exists(email: String): F[Boolean] = {
    find(email)
      .map(_ => true).handleError {
      case _: UserNotFound => false
    }
  }

  override def add(user: User): F[User] = {
    exists(user.email).flatMap {
      case true => FR.raise(MailAlreadyExists(user.email))
      case false => addUser(Random.nextString(6), user)
    }
  }

  private def set(userID: UserID, user: User): F[User] = {
    F.delay {
      state = state.updated(userID, user)
    }.map(_ => user)
  }

  override def delete(id: UserID): F[Unit] = {
    F.delay {
      state = state.removed(id)
    }.flatMap(_ => F.unit)
  }

  override def deleteAll(ids: List[UserID]): F[Unit] = {
    ids.traverse(id => delete(id)).flatMap(_ => F.unit)
  }

  private def getAndSet(userID: UserID, f: User => User): F[User] = {
    get(userID).value.flatMap {
      case None =>
        FR.raise(UserNotFound(userID))
      case Some(user) =>
        set(userID, f(user))
    }
  }

  override def update(userUpdate: UserUpdate): F[User] = {
    getAndSet(userUpdate.id, user => userUpdate.merge(user))
  }

  override def update(user: User): F[User] = {
    user.pure[F] 
  }

  override def makeAdmin(userID: UserID): F[Unit] = {
    getAndSet(userID, user => user.copy(role = Role.Admin)).flatMap(_ => F.unit)
  }

  override def validatePhone(userID: UserID): F[Unit] = {
    getAndSet(userID, user => user.copy(validPhone = true)).flatMap(_ => F.unit)
  }

  override def list(limit: Int, offset: Int): F[List[User]] = ???

//  override def uploadLicense(userID: UserID, is: InputStream, filename: Option[String]): F[User] = ???
}

object InmemoryUserStore {
  def apply[F[_]](implicit F: Sync[F], FR: Raise[F, UserError]) =
    new InmemoryUserStore[F]
}

