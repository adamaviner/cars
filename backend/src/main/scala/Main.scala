import cats.effect.{Blocker, ExitCode, IO, IOApp, Resource, SyncIO}

import java.util.concurrent.Executors
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, ExecutionContextExecutorService}

object Main extends IOApp {
  val customBlockingEC: ExecutionContextExecutorService =
    ExecutionContext.fromExecutorService(
      Executors.newCachedThreadPool((r: Runnable) => {
        val t = new Thread(r)
        t.setName(s"custom-blocking-ec-${t.getName}")
        t
      })
    )

  def run(args: List[String]): IO[ExitCode] = {
    implicit val blocker: Blocker = Blocker.liftExecutionContext(customBlockingEC)

    CarsServer.create.use(_ => IO.never).as(ExitCode.Success)
  }
}
