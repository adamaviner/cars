package utils

import java.time.{Duration, Period}

object TimeUtils {

  implicit class DurationInt(val i: Int) {
    def seconds: Duration = Duration.ofSeconds(i)
    def second: Duration = seconds

    def minutes: Duration = Duration.ofMinutes(i)
    def minute: Duration = minutes

    def hours: Duration = Duration.ofHours(i)
    def hour: Duration = hours
  }

  implicit class PeriodInt(val i: Int) {
    def days: Period = Period.ofDays(i)
    def day: Period = days

    def weeks: Period = Period.ofWeeks(i)
    def week: Period = weeks
  }
}
