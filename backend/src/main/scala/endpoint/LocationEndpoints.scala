package endpoint

import cats._
import cats.effect._
import cats.syntax.all._
import domain.CommonTexts
import domain.auth.AuthHelpers
import domain.users.User
import domain.users.UserModel.UserID
import domain.cars.Location
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.typelevel.log4cats.Logger
import stores.firestore.FireLocationStore
import tsec.authentication.{SecuredRequestHandler, TSecAuthService, asAuthed}

class LocationEndpoints[F[_]: ContextShift: Async: Parallel: ConcurrentEffect: Logger, Auth]
    extends Http4sDsl[F] {

  def endpoints(
      locationStore: FireLocationStore[F],
      securedRequestHandler: SecuredRequestHandler[F, UserID, User, Auth]
  ): HttpRoutes[F] = {
    import io.circe.generic.auto._
    import io.circe.syntax._
    import org.http4s.circe.CirceEntityCodec._

    val adminEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.adminRequired[F, Auth]) {
        case req @ POST -> Root / "add" asAuthed _ =>
          req.request
            .as[Location]
            .flatMap(location => locationStore.add(location).flatMap(v => Ok(v)))

        case req @ PUT -> Root / "update" asAuthed _ =>
          req.request
            .as[Location]
            .flatMap(location => locationStore.update(location).flatMap(v => Ok(v)))

        case req @ DELETE -> Root / "delete" / id asAuthed _ =>
          locationStore.discard(id).flatMap(_ => Ok())
      }

    val unauthEndpoints = HttpRoutes.of[F] {
      case GET -> Root / "get" / id =>
        locationStore.get(id).flatMap(v => Ok(v))

      case GET -> Root / "list" =>
        locationStore.list.flatMap(v => Ok(v))
    }

    import AuthHelpers.SecuredRequestHandlerBubbleErrors
    val routes = unauthEndpoints <+> securedRequestHandler.liftServiceBubbleErrors(
      adminEndpoints
    )
    routes
  }
}
