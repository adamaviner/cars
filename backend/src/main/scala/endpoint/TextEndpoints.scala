package endpoint

import cats._
import cats.effect._
import cats.syntax.all._
import domain.CommonTexts
import domain.auth.AuthHelpers
import domain.users.User
import domain.users.UserModel.UserID
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.typelevel.log4cats.Logger
import stores.firestore.TextStore
import tsec.authentication.{SecuredRequestHandler, TSecAuthService, asAuthed}
import domain.Text

class TextEndpoints[F[_]: ContextShift: Async: Parallel: ConcurrentEffect: Logger, Auth]
    extends Http4sDsl[F] {

  def endpoints(
      textStore: TextStore[F],
      securedRequestHandler: SecuredRequestHandler[F, UserID, User, Auth]
  ): HttpRoutes[F] = {
    import io.circe.generic.auto._
    import org.http4s.circe.CirceEntityCodec._

    val adminEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.adminRequired[F, Auth]) {
        case req @ PUT -> Root / "commons" asAuthed _ =>
          req.request
            .as[CommonTexts]
            .flatMap(commonTexts => textStore.setCommons(commonTexts).flatMap(_ => Ok()))

        case req @ PUT -> Root / "set" asAuthed _ =>
          req.request
            .as[Text]
            .flatMap(text =>
              textStore
                .set(text.name, text.value)
                .flatMap(_ => Ok())
            )
            .handleErrorWith { case e: Exception =>
              Logger[F].error(
                s"Error: $e \nwhile setting text for: ${req.request}"
              ) *> InternalServerError()
            }
      }

    val unauthEndpoints = HttpRoutes.of[F] {
      case GET -> Root / "commons" =>
        textStore.getCommons().flatMap(text => Ok(text))

      case GET -> Root / "get" / id =>
        textStore.get(id).flatMap(text => Ok(text))
    }

    import AuthHelpers.SecuredRequestHandlerBubbleErrors
    val routes = unauthEndpoints <+> securedRequestHandler.liftServiceBubbleErrors(
      adminEndpoints
    )
    routes
  }
}
