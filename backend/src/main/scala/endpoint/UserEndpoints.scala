package endpoint

import cats.Parallel
import cats.data.OptionT
import cats.effect._
import cats.syntax.all._
import cats.mtl.Raise
import com.stripe.model.Event
import com.stripe.model.identity.VerificationSession
import domain.auth.AuthHelpers.Role
import domain.auth.{
  AuthHelpers,
  LoginRequest,
  MailVerification,
  PasswordReset,
  SignupRequest,
  Sms,
  Token
}
import domain.human.VerifyHumanRequest
import domain.users.UserModel.UserID
import domain.users._
import endpoint.Pagination.{OptionalLimitMatcher, OptionalOffsetMatcher}
import io.circe.Json
import org.http4s.{HttpRoutes, MediaType, ServerSentEvent}
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.`Content-Type`
import org.http4s.multipart.Multipart
import org.typelevel.log4cats.Logger
import services.{MailService, SmsService, StripeService}
import stores.firestore.{FireTokenStore, FireUserStore}
import tsec.authentication.{SecuredRequestHandler, TSecAuthService, asAuthed}
import tsec.common.Verified
import tsec.passwordhashers.{PasswordHash, PasswordHasher}
import fs2.text
import services.PaymentService

case class PasswordResetRequest(password: String, token: String)

class UserEndpoints[F[_]: Sync: Parallel: ConcurrentEffect: Logger, Hash, Auth]
    extends Http4sDsl[F] {

  def endpoints(
      userStore: FireUserStore[F],
      userValidator: UserValidator[F],
      tokenStore: FireTokenStore[F],
      mailService: MailService[F],
      smsService: SmsService[F],
      stripeService: StripeService[F],
      paymentService: PaymentService[F],
      crypt: PasswordHasher[F, Hash],
      securedRequestHandler: SecuredRequestHandler[F, UserID, User, Auth]
  )(implicit
      H: HttpErrorHandler[F, UserError],
      FR: Raise[F, UserError]
  ): HttpRoutes[F] = {
    import io.circe.generic.auto._
    import io.circe.syntax._
    import org.http4s.circe.CirceEntityCodec._

    def handleVerificationSession(session: VerificationSession): F[Unit] = { // TODO send mails?
      userStore
        .verificationSession(session.getId)
        .flatMap(verification => {
          if (session.getStatus == "verified") {
            userStore.setVerificationSession(verification.copy(status = session.getStatus)) *>
              userStore.validateLicense(verification.userID)
          } else {
            val err = if (session.getLastError != null) session.getLastError.getReason else ""
            userStore
              .setVerificationSession(verification.copy(status = session.getStatus, error = err))
          }
        })
    }

    def sendVerificationMail(user: User): F[Unit] = {
      if (user.validEmail) Sync[F].unit
      else
        tokenStore
          .add(Token.mailVerificationToken(user.id), tokenType = MailVerification)
          .flatMap(token => mailService.sendMailVerification(user, token.token))
    }

    def sendVerificationSms(user: User): F[Unit] = {
      if (user.validPhone) Sync[F].unit
      else
        tokenStore
          .add(Token.smsToken(user.id), tokenType = Sms)
          .flatMap(token => smsService.sendVerification(user, token.token))
    }

    val adminEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.adminRequired[F, Auth]) {
        case req @ PUT -> Root / "admin" / "update" asAuthed _ =>
          for {
            updateInfo <- req.request.as[User]
            validUpdateInfo <- userValidator
              .validate(updateInfo.toUserUpdate)
              .map(validUpdate =>
                updateInfo.copy(phone = validUpdate.phone.getOrElse(updateInfo.phone))
              )
            updatedUser <- userStore.update(validUpdateInfo)
            response <- Ok(updatedUser.asJson)
          } yield response

        case req @ POST -> Root / "admin" / "delete" asAuthed _ =>
          req.request.as[List[UserID]].flatMap { ids =>
            userStore.deleteAll(ids).flatMap(_ => Ok())
          }

//        case GET -> Root / "admin" / "license" / userID asAuthed _ => {
//          Logger[F].debug(s"stream license: $userID") *>
//            Ok(userStore.streamLicense(userID))
//              .map(_.withContentType(`Content-Type`(MediaType.image.png)))
//        }
      }

    val custodianEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.custodianRequired[F, Auth]) {
        case GET -> Root / "list" :? OptionalLimitMatcher(limit) :? OptionalOffsetMatcher(
              offset
            ) asAuthed _ =>
          userStore
            .list(limit = limit.getOrElse(10000), offset = offset.getOrElse(0))
            .flatMap(users => Ok(users.asJson))

        case GET -> Root / "fetch" / id asAuthed _ =>
          userStore.doGet(id).flatMap(user => Ok(user.asJson))
      }

    val auth = securedRequestHandler.authenticator

    val userEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.userRequired[F, Auth]) {
        case req @ PUT -> Root / "update" asAuthed user => {
          def withPassHash(update: UserUpdate): F[UserUpdate] =
            update.password match {
              case None => update.pure[F]
              case Some(pass) =>
                crypt.hashpw(pass).map(passHash => update.copy(passHash = Some(passHash)))
            }

          for {
            updateInfo <- req.request.as[UserUpdate]
            _ <- userValidator.assertCanUpdate(user, updateInfo)
            _ <- userValidator.assertValidPassword(updateInfo.password)
            _ <- userValidator.assertValidEmail(updateInfo.email)
            _ <- userValidator.assertAndFormatPhone(updateInfo.phone)
            _ <-
              if (updateInfo.email.nonEmpty && updateInfo.email.get != user.email)
                userStore.verifyMailAvailable(updateInfo.email.get)
              else Sync[F].unit
            _ <-
              if (updateInfo.phone.nonEmpty && updateInfo.phone.get != user.phone)
                userStore.verifyPhoneAvailable(updateInfo.phone.get)
              else Sync[F].unit
            updateWithPassHash <- withPassHash(updateInfo)
            updatedUser <- userStore.update(updateWithPassHash)
            _ <- sendVerificationMail(updatedUser)
            response <- Ok(updatedUser.asJson)
          } yield response
        }

        case DELETE -> Root / "delete" asAuthed user =>
          userStore.delete(user.id).flatMap(_ => Ok())

        case req @ POST -> Root / "signout" asAuthed user =>
          auth.discard(req.authenticator).flatMap { _ =>
            Ok()
          }

//        case req @ POST -> Root / "license" asAuthed user =>
//          req.request.decode[Multipart[F]](m => {
//            m.parts.headOption match {
//              case None => BadRequest()
//              case Some(part) =>
//                val action = part.body
//                  .through(fs2.io.toInputStream)
//                  .evalMap(is => userStore.uploadLicense(user.id, is, part.filename))
//                  .compile
//                  .lastOrError
//
//                action
//                  .flatMap(link => Ok())
//                  .handleErrorWith { //TODO errors bubble? are there?
//                    case e: Exception =>
//                      Logger[F].error(
//                        s"Error: $e \nwhile uploading photos for: ${user.id}"
//                      ) *> InternalServerError()
//                  }
//            }
//          })

        case PUT -> Root / "phone" / phone asAuthed user =>
          for {
            formattedPhone <- userValidator.assertAndFormatPhone(phone)
            _ <-
              if (formattedPhone != user.phone) userStore.verifyPhoneAvailable(formattedPhone)
              else FR.raise(UserNotModified(formattedPhone))
            update = UserUpdate(user.id, phone = Option(formattedPhone))
            updatedUser <- userStore.update(update)
            response <- Ok(updatedUser.asJson)
          } yield response

        case POST -> Root / "sms" / "sendVerification" asAuthed user =>
          if (user.validPhone) NotModified()
          else
            sendVerificationSms(user).flatMap(_ => Ok())

        // TODO email validation + keep email reputation with multi send & require verification before pasword rest etc..
        case GET -> Root / "email" / "sendVerification" asAuthed user =>
          sendVerificationMail(user).flatMap(_ => Ok())

        case POST -> Root / "sms" / "verification" / "consume" / token asAuthed user =>
          val response = for {
            token <- tokenStore.find(user.id, token, Sms)
            _ <- userStore.validatePhone(user.id)
            _ <- tokenStore.discard(token.id, Sms)
          } yield token

          response.flatMap(_ => Ok())

        case POST -> Root / "license" / "verificationSession" asAuthed user =>
          val secret = for {
            session <- stripeService.verificationSession()
            _ <- userStore.setVerificationSession(Verification(user.id, session.getId))
          } yield session.getClientSecret
          secret
            .flatMap(secret => Ok(secret))

        case GET -> Root / "license" / "verificationSession" asAuthed user =>
          userStore
            .findVerificationSession(user.id)
            .flatMap(session => Ok(session.asJson))

        case GET -> Root / "me" asAuthed user =>
          Ok(user.asJson);
      }

    val unauthEndpoints = HttpRoutes.of[F] {
      case PUT -> Root / "email" / "verify" / token =>
        val response = for {
          token <- tokenStore.find(token, MailVerification)
          _ <- userStore.validateEmail(token.userID)
          user <- userStore.doGet(token.userID)
          _ <- tokenStore.discard(token.id, MailVerification)
        } yield user

        response.flatMap(u => Ok(u.email))

      case POST -> Root / "password" / "reset" / email => {
        (for {
          user <- userStore.find(email)
          token <- tokenStore.add(Token.passwordResetToken(user.id), PasswordReset)
          _ <- mailService.sendPasswordReset(user, token.token)
          resp <- Ok()
        } yield resp)
          .handleErrorWith { case e: UserNotFound =>
            Logger[F].error(
              s"attemp to reset password for non existing user: $email"
            ) *> Ok() // don't show client that mail doesn't have account
          }
      }

      case request @ PUT -> Root / "password" / "reset" / "consume" => {
        for {
          _ <- Logger[F].debug("consuming password reset")
          req <- request.as[PasswordResetRequest]
          _ <- userValidator.assertValidPassword(req.password)
          token <- tokenStore.find(req.token, PasswordReset)
          pass <- crypt.hashpw(req.password)
          user <- userStore.update(UserUpdate(token.userID, passHash = Some(pass)))
          response <- auth
            .create(token.userID)
            .flatMap(authToken => // login new user
              tokenStore.discard(token.id, tokenType = PasswordReset)
                *> Accepted(user.asJson).map(auth.embed(_, authToken))
            )
        } yield response
      }

      case request @ POST -> Root / "webhook" => {
        request.body
          .through(text.utf8Decode)
          .compile
          .string
          // request
          //   .as[Json]
          .flatMap(payload => {
            val remoteAddress = request.from.map(_.getHostAddress)
            val response = for {
              _ <- OptionT.liftF(Logger[F].debug("webhook: remoteAddress: " + remoteAddress))
              event <- OptionT(stripeService.getEvent(payload, request, remoteAddress))
              session <- OptionT(stripeService.getVerificationSession(event))
              _ <- OptionT.liftF(handleVerificationSession(session))
            } yield event
            response.value
          })
          .flatMap(response => if (response.isEmpty) InternalServerError() else Ok())
          .handleErrorWith { case e: Exception =>
            Logger[F].error(
              s"Error: $e \nwhile processing webhook"
            ) *> InternalServerError()
          }
      }

      case req @ POST -> Root / "login" => {
        val action = for {
          login <- req.as[LoginRequest]
          email = login.email.trim().toLowerCase()
          user <- userStore.find(email)
          checkResult <- crypt.checkpw(login.password, PasswordHash[Hash](user.passHash))
          _ <-
            if (checkResult == Verified) Sync[F].unit
            else FR.raise(UserAuthenticationFailedError(email))
          token <- auth.create(user.id)
        } yield (user, token)

        action.flatMap { case (user, token) =>
          Ok(user.copy(passHash = "").asJson).map(auth.embed(_, token))
        }
      }

      case req @ POST -> Root / "signup" =>
        for {
          signup <- req.as[SignupRequest]
          validSignup <- userValidator.validate(signup)
          passHash <- crypt.hashpw(validSignup.password)
          user <- validSignup.asUser(passHash).pure[F]
          createdUser <- userStore.add(user)
          _ <- paymentService.attachStripeCustomer(createdUser)
          response <- auth.create(createdUser.id).flatMap { token => // login new user
            Created(createdUser.copy(passHash = "").asJson).map(auth.embed(_, token))
          }
          _ <- sendVerificationMail(createdUser)
        } yield response
    }

    import AuthHelpers.SecuredRequestHandlerBubbleErrors
    val routes = unauthEndpoints <+> securedRequestHandler.liftServiceBubbleErrors(
      custodianEndpoints <+> adminEndpoints <+> userEndpoints
    )
    H.handle(routes)
  }
}

//TODO add validation of user input

object UserEndpoints {
  def endpoints[F[_]: Sync: Parallel: ConcurrentEffect: Logger, Hash, Auth](
      userStore: FireUserStore[F],
      userValidator: UserValidator[F],
      tokenStore: FireTokenStore[F],
      mailService: MailService[F],
      smsService: SmsService[F],
      stripeService: StripeService[F],
      paymentService: PaymentService[F],
      crypt: PasswordHasher[F, Hash],
      securedRequestHandler: SecuredRequestHandler[F, UserID, User, Auth]
  )(implicit H: HttpErrorHandler[F, UserError], FR: Raise[F, UserError]): HttpRoutes[F] =
    new UserEndpoints[F, Hash, Auth].endpoints(
      userStore,
      userValidator,
      tokenStore,
      mailService,
      smsService,
      stripeService,
      paymentService,
      crypt,
      securedRequestHandler
    )
}
