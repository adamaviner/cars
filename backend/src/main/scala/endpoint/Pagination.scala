package endpoint

import org.http4s.dsl.impl.OptionalQueryParamDecoderMatcher

object Pagination {
  /* Necessary for decoding query parameters */
//  import org.http4s.QueryParamDecoder._

  /* Parses out the optional offset and page size params */
  object OptionalLimitMatcher extends OptionalQueryParamDecoderMatcher[Int]("limit")
  object OptionalOffsetMatcher extends OptionalQueryParamDecoderMatcher[Int]("offset")
}
