package endpoint

import cats._
import cats.effect._
import cats.syntax.all._
import domain.auth.AuthHelpers
import domain.cars.CarListing
import domain.trips.{
  MaintenanceRequest,
  Trip,
  TripError,
  TripInspection,
  TripRequest,
  TripUpdateRequest,
  TripValidator
}
import QueryParam._
import domain.users.User
import domain.users.UserModel.UserID
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.http4s.multipart.Multipart
import org.typelevel.log4cats.Logger
import stores.firestore.{FireCarStore, FireTripStore, FireUserStore}
import tsec.authentication.{SecuredRequestHandler, TSecAuthService, asAuthed}
import domain.payment.ChargeResponse

import java.time.Instant
import java.time.temporal.ChronoUnit
import cats.instances.char
import services.{PaymentService, StorageService}
import domain.trips.InspectionType
import enumeratum._
import services.MailService
import org.http4s.Response
import domain.payment.PaymentAction
import domain.cars.Car
import domain.cars
import domain.cars.Insurance
import domain.trips.TripCalculateRequest
import com.github.gekomad.ittocsv.parser.io.ToFile.csvToFileStream
import com.github.gekomad.ittocsv.core.ToCsv._

class TripEndpoints[F[_]: ContextShift: Async: Parallel: ConcurrentEffect: Logger, Auth](implicit
    F: Async[F],
    blocker: Blocker
) extends Http4sDsl[F] {

  def endpoints(
      tripStore: FireTripStore[F],
      validator: TripValidator[F],
      carStore: FireCarStore[F],
      userStore: FireUserStore[F],
      mailService: MailService[F],
      paymentService: PaymentService[F],
      storageService: StorageService[F],
      securedRequestHandler: SecuredRequestHandler[F, UserID, User, Auth]
  )(implicit H: HttpErrorHandler[F, TripError]): HttpRoutes[F] = {
    import io.circe.generic.auto._
    import io.circe.syntax._
    import org.http4s.circe.CirceEntityCodec._

    def sendInspectionMails(): F[Unit] = {
      for {
        _ <- Logger[F].debug("sendInspectionMails")
        tripsToInspect <- tripStore.getTripsToInspect()
        lateForInspection = tripsToInspect.filter(_.lateForInspection)
        _ <- lateForInspection.parTraverse(trip => {
          for {
            _ <- Logger[F].debug("sendLateForInspection: trip: " + trip)
            _ <- mailService.sendLateForInspection(trip)
            _ <- mailService.sendLateForInspection(trip)
            _ <- mailService.sendLateForInspection(trip)
            _ <- tripStore.sentInspectionMail(trip)
          } yield {}
        })
      } yield {}
    }

    def sendTripMail(trip: Trip): F[Unit] = {
      for {
        _ <- Logger[F].debug("sendInspectionMails")
        tripsToInspect <- tripStore.getTripsToInspect()
        lateForInspection = tripsToInspect.filter(_.lateForInspection)
        _ <- lateForInspection.parTraverse(trip => {
          for {
            _ <- Logger[F].debug("sendLateForInspection: trip: " + trip)
            _ <- mailService.sendLateForInspection(trip)
            _ <- mailService.sendLateForInspection(trip)
            _ <- mailService.sendLateForInspection(trip)
            _ <- tripStore.sentInspectionMail(trip)
          } yield {}
        })
      } yield {}
    }

    def calculateTrip(
        tripID: String = "",
        tripRequest: TripRequest,
        user: Option[User] = None
    ): F[Response[F]] = {
      for {
        car <- carStore.getCar(tripRequest.vin)
        _ <- validator.validateTrip(tripRequest)
        _ <-
          if (tripID.nonEmpty)
            tripStore.assertAvailableForUpdate(tripRequest.asTripUpdate(tripID))
          else tripStore.assertAvailable(tripRequest)
        price = tripRequest.detailedPrice(car, user)
        res <- Ok(price.asJson)
      } yield res
    }

    val maintainerEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.maintainerRequired[F, Auth]) {
        case POST -> Root / "maintainer" asAuthed _ =>
          for {
            _ <- Logger[F].debug("maintainer executed")
            _ <- tripStore.archiveExpiredRequests()
            _ <- tripStore.deactivateTrips()
            _ <- sendInspectionMails()
            res <- Ok()
          } yield res

        case POST -> Root / "pruneExpiredRequests" asAuthed _ =>
          tripStore.archiveExpiredRequests().flatMap(_ => Ok())

        case POST -> Root / "deactivateTrips" asAuthed _ =>
          tripStore.deactivateTrips().flatMap(_ => Ok())
      }

    val adminEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.adminRequired[F, Auth]) {
        // case req @ DELETE -> Root / "delete" / id asAuthed _ =>
        //   tripStore.archive(id).flatMap(_ => Ok())

        case req @ POST -> Root / "add" asAuthed _ =>
          req.request.as[Trip].flatMap { tripRequest =>
            tripStore
              .add(tripRequest)
              .flatMap(_ => Ok())
          }

        // case req @ PUT -> Root / "update" asAuthed _ =>
        //   req.request.as[Trip].flatMap { tripRequest =>
        //     tripStore
        //       .update(tripRequest)
        //       .flatMap(trip => Ok(trip.asJson))
        //   }
      }

    val custodianEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.custodianRequired[F, Auth]) {
        case req @ PUT -> Root / "update" asAuthed user =>
          req.request.as[Trip].flatMap { tripRequest =>
            {
              for {
                _ <- if (user.isAdmin) F.unit else tripStore.assertNotPostInspected(tripRequest.id)
                trip <- tripStore.update(tripRequest)
                res <- Ok(trip.asJson)
              } yield res
            }
          }

        case GET -> Root / "getTripsToPreInspect" asAuthed _ =>
          tripStore
            .getTripsToPreInspect()
            .flatMap(trips => Ok(trips.asJson))

        case GET -> Root / "getTripsToPostInspect" asAuthed _ =>
          tripStore
            .getTripsToPostInspect()
            .flatMap(trips => Ok(trips.asJson))

        case GET -> Root / "inspection" / inspectionTypeParam / tripID asAuthed _ =>
          val inspectionType = InspectionType.withName(inspectionTypeParam)
          tripStore
            .getInspection(tripID, inspectionType)
            .flatMap(inspections => Ok(inspections.asJson))

        case req @ POST -> Root / "inspection" asAuthed _ =>
          def chargeIncidental(trip: Trip, inspection: TripInspection): F[Unit] = {
            if (inspection.chargeAmount == 0 || inspection.inspectionType == InspectionType.Pre)
              return Async[F].unit
            for {
              _ <- paymentService.chargeIncidental(
                inspection.tripID,
                inspection.chargeAmount,
                "Incidental Costs"
              )
              car <- carStore.getCar(trip.vin)
              user <- userStore.doGet(trip.userID)
              _ <- mailService.sendIncidentalCharge(user, trip, car, inspection)
            } yield {}
          }
          val inspection = for {
            inspection <- req.request.as[TripInspection]
            trip <- tripStore.get(inspection.tripID)
            _ <- tripStore.assertCanInspect(trip, inspection)
            _ <- tripStore.attachInspection(inspection)
            _ <- chargeIncidental(trip, inspection)
            _ <- carStore.updateMiles(trip.vin, inspection.currentMiles)
          } yield inspection
          inspection.flatMap(_ => Ok())

        case req @ POST -> Root / "inspection" / "upload" / inspectionTypeParam / tripID asAuthed _ => {
          val inspectionType = InspectionType.withName(inspectionTypeParam)
          req.request.decode[Multipart[F]] { m =>
            {
              val blobLinks =
                storageService.uploadPhotos(s"inspections/$tripID/${inspectionType.entryName}", m)
              val addPhotosAction = blobLinks.flatMap(links =>
                tripStore.addInspectionPhotos(tripID, inspectionType, links)
              )

              addPhotosAction
                .flatMap(links => Ok(links.asJson))
                .handleErrorWith { case e: Exception =>
                  Logger[F].error(
                    s"Error: $e \nwhile uploading photos for: $tripID"
                  ) *> InternalServerError()
                }
            }
          }
        }

        case req @ POST -> Root / "maintenance" asAuthed user =>
          req.request.as[MaintenanceRequest].flatMap { req =>
            for {
              trip <- tripStore.scheduleMaintenance(req)
              car <- carStore.getCar(req.vin)
              _ <- mailService.sendManualBookingToCustodians(user, trip, car)
              resp <- Ok(trip.asJson)
            } yield resp
          }

        case req @ PUT -> Root / "maintenance" asAuthed user =>
          req.request.as[MaintenanceRequest].flatMap { req =>
            for {
              trip <- tripStore.updateMaintenance(req)
              car <- carStore.getCar(req.vin)
              _ <- mailService.sendManualBookingToCustodians(user, trip, car)
              resp <- Ok(trip.asJson)
            } yield resp
          }

        case GET -> Root / "admin" / "list"
            :? Pagination.OptionalLimitMatcher(limit)
            :? Pagination.OptionalOffsetMatcher(offset) asAuthed _ =>
          tripStore
            .list(limit.getOrElse(10000), offset.getOrElse(0))
            .flatMap(trips => Ok(trips.asJson))

        case GET -> Root / "allTrips" asAuthed _ =>
          tripStore
            .listAll()
            .flatMap(trips => Ok(trips.asJson))

        case GET -> Root / "allTripsCSV" asAuthed _ =>
          implicit val csvFormat = com.github.gekomad.ittocsv.parser.IttoCSVFormat.default
            .withPrintHeader(true)

          tripStore
            .listAll()
            .flatMap(trips => Ok(toCsvL((trips))))

        case GET -> Root / "allInspections" asAuthed _ =>
          tripStore
            .listInspections()
            .flatMap(inspections => Ok(inspections.asJson))

        case GET -> Root / "allInspectionsCSV" asAuthed _ =>
          implicit val csvFormat = com.github.gekomad.ittocsv.parser.IttoCSVFormat.default
            .withPrintHeader(true)

          tripStore
            .listInspections()
            .flatMap(inspections => Ok(toCsvL((inspections.map(_.asCsv)))))
      }

    val userEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.userRequired[F, Auth]) {
        case GET -> Root / "list" asAuthed user =>
          tripStore.listUserTrips(user.id).flatMap(trips => Ok(trips.asJson))

        case GET -> Root / "list" / "past" asAuthed user =>
          tripStore.listPastUserTrips(user.id).flatMap(trips => Ok(trips.asJson))

        case GET -> Root / "list" / "future" asAuthed user =>
          tripStore.listFutureUserTrips(user.id).flatMap(trips => Ok(trips.asJson))

        case GET -> Root / "get" / tripID asAuthed user =>
          val trip = for {
            trip <- tripStore.get(tripID)
            _ <- validator.validateUser(trip, user)
          } yield trip

          trip.flatMap(t => Ok(t.asJson))

        case GET -> Root / "available" / vin
            :? StartDateParamMatcher(startDate)
            :? EndDateParamMatcher(endDate)
            asAuthed user =>
          val tripRequest = TripRequest(vin, startDate = startDate, endDate = endDate)
          (for {
            valid <- validator.validateTrip(tripRequest)
          } yield valid)
            .flatMap(_ => Ok())

        case req @ PUT -> Root / "calculateFull" / vin asAuthed user =>
          req.request
            .as[TripCalculateRequest]
            .flatMap(req => {
              val tripRequest =
                TripRequest(
                  vin,
                  startDate = req.startDate,
                  endDate = req.endDate,
                  insurance = req.insurance
                )
              calculateTrip(req.tripId, tripRequest, Some(user))
            })

        case req @ POST -> Root / "request" asAuthed user =>
          req.request.as[TripRequest].map(_.copy(userID = user.id)).flatMap { tripRequest =>
            val action =
              for { // FIXME price is first without deposit then with, when ordering straight
                car <- carStore.getCar(tripRequest.vin)
                _ <- validator.validateTrip(tripRequest)
                _ <- validator.validateBasePrice(car, tripRequest, user)
                trip <- tripStore.schedule(tripRequest.copy(userID = user.id))
              } yield trip

            action.flatMap(trip => Ok(trip.asJson)) //TODO good return value? exposes?
          }

        case DELETE -> Root / "cancel" / tripID asAuthed user =>
          def refundExcept(trip: Trip, car: Car): F[Unit] = {
            if (trip.canceled) return Async[F].unit
            for {
              _ <- paymentService.refundTrip(
                trip.id,
                trip.asTripRequest.updateFee(car),
                "Trip Cancelled"
              )
            } yield {}
          }

          val action = for {
            trip <- tripStore.get(tripID)
            car <- carStore.getCar(trip.vin)
            _ <- validator.validateUser(trip, user)
            _ <- refundExcept(trip, car)
            _ <- tripStore.cancel(tripID)
            _ <- mailService.sendTripCancelled(user, trip, car)
          } yield {}

          action.flatMap(_ => Ok())

        case req @ PUT -> Root / "request" / "update" asAuthed user =>
          req.request.as[TripUpdateRequest].flatMap { tripUpdateRequest =>
            for {
              _ <- Logger[F].debug("request.update:")
              _ <- validator.validateTrip(tripUpdateRequest.asTripRequest)
              trip <- tripStore.get(tripUpdateRequest.id)
              _ <- validator.validateCanUpdateTrip(trip)
              car <- carStore.getCar(tripUpdateRequest.vin)
              _ <- validator.validateTotal(car, tripUpdateRequest.asTripRequest, user)
              updatedTrip = trip
                .copy(
                  price = tripUpdateRequest.asTripRequest.detailedPrice(car, Some(user)).total,
                  startDate = tripUpdateRequest.startDate,
                  endDate = tripUpdateRequest.endDate,
                  insurance = tripUpdateRequest.insurance
                )
              updateFee = validator.priceForTripUpdate(trip, car)
              paymentMethodId = tripUpdateRequest.chargeRequest.flatMap(_.paymentMethodID)
              action <-
                if (trip.price == updatedTrip.price) none[PaymentAction].pure[F]
                else if (trip.booked)
                  paymentService.updateTripPayment(user, updatedTrip, paymentMethodId, updateFee)
                else paymentService.payForTripNow(user, updatedTrip, paymentMethodId)
              _ <- Logger[F].debug("request.update: action: " + action)
              _ <-
                if (action.isEmpty) paymentService.confirmPaymentAndTrip(user, updatedTrip)
                else Async[F].unit
              tripAfterPaymentProcess = updatedTrip
                .copy(booked = action.isEmpty, wasPaid = action.isEmpty)
              _ <- tripStore.update(tripAfterPaymentProcess)
              res <- Ok(ChargeResponse(paymentAction = action).asJson)
            } yield res
          }
      }

    val unauthEndpoints = HttpRoutes.of[F] {
      case GET -> Root / "booked" / vin =>
        (for {
          booked <- tripStore.booked(vin)
        } yield booked)
          .flatMap(booked => Ok(booked.asJson))

      case req @ PUT -> Root / "calculate" / vin =>
        req
          .as[TripCalculateRequest]
          .flatMap(req => {
            val tripRequest =
              TripRequest(
                vin,
                startDate = req.startDate,
                endDate = req.endDate,
                insurance = req.insurance
              )
            calculateTrip(req.tripId, tripRequest = tripRequest)
          })
    }

    import AuthHelpers.SecuredRequestHandlerBubbleErrors
    val routes = unauthEndpoints <+> securedRequestHandler.liftServiceBubbleErrors(
      custodianEndpoints <+> maintainerEndpoints <+> adminEndpoints <+> userEndpoints
    )
    H.handle(routes)
  }
}

object TripEndpoints {
  def endpoints[F[
      _
  ]: ContextShift: Async: Parallel: ConcurrentEffect: Logger, Auth](
      tripStore: FireTripStore[F],
      validator: TripValidator[F],
      carStore: FireCarStore[F],
      userStore: FireUserStore[F],
      mailService: MailService[F],
      paymentService: PaymentService[F],
      storageService: StorageService[F],
      securedRequestHandler: SecuredRequestHandler[F, UserID, User, Auth]
  )(implicit
      H: HttpErrorHandler[F, TripError],
      blocker: Blocker
  ): HttpRoutes[F] =
    new TripEndpoints[F, Auth].endpoints(
      tripStore,
      validator,
      carStore,
      userStore,
      mailService,
      paymentService,
      storageService,
      securedRequestHandler
    )
}
