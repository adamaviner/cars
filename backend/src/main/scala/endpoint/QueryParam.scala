package endpoint

import domain.cars.Insurance

object QueryParam {

  import org.http4s.dsl.impl.QueryParamDecoderMatcher
  import org.http4s.dsl.impl.OptionalQueryParamDecoderMatcher
  import org.http4s.dsl.impl.OptionalMultiQueryParamDecoderMatcher

  object StartDateParamMatcher extends QueryParamDecoderMatcher[Long]("startDate")

  object EndDateParamMatcher extends QueryParamDecoderMatcher[Long]("endDate")
  
  object OptionalTripIDParamMatcher extends OptionalQueryParamDecoderMatcher[String]("tripID")

  object CarIDsMultiParam extends OptionalMultiQueryParamDecoderMatcher[String]("id")
}