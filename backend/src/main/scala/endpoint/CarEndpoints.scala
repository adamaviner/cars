package endpoint

import cats._
import cats.data.NonEmptyList
import cats.data.Validated
import cats.effect._
import cats.syntax.all._
import domain.SearchFilters
import domain.auth.AuthHelpers
import domain.cars.{Car, CarError}
import domain.users.User
import domain.users.UserModel.UserID
import org.http4s.CacheDirective.`no-cache`
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.`Cache-Control`
import org.http4s.multipart.Multipart
import org.typelevel.log4cats.Logger
import QueryParam._
import stores.firestore.{FireCarStore, FireTripStore}
import tsec.authentication.{SecuredRequestHandler, TSecAuthService, asAuthed}

import scala.util.Random
import services.StorageService
import stores.firestore.FireLocationStore
import services.MailService

//case class CarError(mailExists: Boolean = false, phoneExists: Boolean = false)

class CarEndpoints[F[_]: ContextShift: Async: Parallel: ConcurrentEffect: Logger, Auth]()
    extends Http4sDsl[F] {

  def endpoints(
      carStore: FireCarStore[F],
      tripStore: FireTripStore[F],
      locationStore: FireLocationStore[F],
      storageService: StorageService[F],
      mailService: MailService[F],
      securedRequestHandler: SecuredRequestHandler[F, UserID, User, Auth]
  )(implicit H: HttpErrorHandler[F, CarError]): HttpRoutes[F] = {
    import io.circe.generic.auto._
    import io.circe.syntax._
    import org.http4s.circe.CirceEntityCodec._

    val adminEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.adminRequired[F, Auth]) {
        case req @ PUT -> Root / "disable" / vin asAuthed _ =>
          carStore.disable(vin).flatMap(_ => Ok())

        case req @ PUT -> Root / "update" asAuthed _ =>
          req.request.as[Car].flatMap { carRequest =>
            carStore
              .update(carRequest)
              .flatMap(_ => Ok())
          }

        case GET -> Root / "list" / "disabled" asAuthed _ =>
          carStore
            .list(10000, 0, listDisabled = true)
            .flatMap { ls =>
              Ok(ls.asJson)
            }
      }

    val custodianEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.custodianRequired[F, Auth]) {
        case req @ POST -> Root / "uploadPhotos" / vin asAuthed _ => {
          req.request.decode[Multipart[F]] { m =>
            {
              val blobLinks = storageService.uploadPhotos(s"$vin", m)
              val addPhotosAction = blobLinks.flatMap(links => carStore.addPhotos(vin, links))

              addPhotosAction
                .flatMap(links => Ok(links.asJson))
                .handleErrorWith { case e: Exception =>
                  Logger[F].error(
                    s"Error: $e \nwhile uploading photos for: $vin"
                  ) *> InternalServerError()
                }
            }
          }
        }

        case req @ POST -> Root / "add" asAuthed user =>
          req.request
            .as[Car]
            .flatMap { carRequest =>
              def inflatePrice(car: Car): Car = {
                val fullPrice =
                  (car.price * Random.between(1.05, 1.15)).floor.toInt // 10-30 percent inflation
                car.copy(fullPrice = fullPrice)
              }

              val action = for {
                car <- carStore.add(carRequest)
                location <- locationStore.get(car.locationID)
                _ <- mailService.sendCarAdded(car, location, user)
                resp <- Ok()
              } yield resp

              action
                .handleErrorWith { case e: Exception =>
                  Logger[F].error(s"Error: $e \nwhile creating car") *> InternalServerError()
                }
            }
            .handleErrorWith { case e: Exception =>
              Logger[F].error(s"Error: $e \nwhile creating car") *> InternalServerError()
            }

      }

    val userEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.userRequired[F, Auth]) {
        case GET -> Root / "favorites" asAuthed user =>
          NotImplemented("no favorites yet, sorry :)")
      }

    import Pagination._
    val unauthEndpoints = HttpRoutes.of[F] {
      case GET -> Root / "listAll"
          :? OptionalLimitMatcher(limit)
          :? OptionalOffsetMatcher(offset) =>
        carStore.list(limit.getOrElse(10000), offset.getOrElse(0)).flatMap { ls =>
          Ok(ls.asJson)
        }

      case req @ PUT -> Root / "list"
          :? OptionalLimitMatcher(limit)
          :? OptionalOffsetMatcher(offset) =>
        val availableCars = for {
          filters <- req.as[SearchFilters]
          startDate = filters.date.from
          untilDate = filters.date.to //TODO order by price after discount
          reservedCars <- tripStore.reservedCars(startDate, untilDate)
          cars <- carStore.list(limit.getOrElse(10000), offset.getOrElse(0), filters)
        } yield cars.filter(car => !reservedCars.contains(car.vin))

        availableCars
          .flatMap { cars =>
            Ok(cars.asJson /*, `Cache-Control`(NonEmptyList(`no-cache`(), Nil))*/ )
          }
          .handleErrorWith { case e: Exception =>
            Logger[F].error(s"Error: $e \nlisting cars: $req") *> InternalServerError()
          }

      case GET -> Root / "get" / vin =>
        carStore
          .getCar(vin)
          .flatMap { car =>
            Ok(car.asJson)
          }
          .recoverWith { e: Throwable =>
            Logger[F].error(s"Error: $e \nwhile getting car:${vin}") *> InternalServerError()
          }

      case GET -> Root / "listing" / "get" / vin =>
        carStore.getCarListing(vin).flatMap { car =>
          Ok(car.asJson)
        }

      case GET -> Root / "listings"
          :? CarIDsMultiParam(ids) =>
        ids match {
          case Validated.Invalid(err) => InternalServerError("")
          case Validated.Valid(vins) =>
            carStore.getCarListings(vins).flatMap(cars => Ok(cars.asJson))
        }

    }

    import AuthHelpers.SecuredRequestHandlerBubbleErrors
    val routes = unauthEndpoints <+> securedRequestHandler.liftServiceBubbleErrors(
      custodianEndpoints <+> adminEndpoints <+> userEndpoints
    )
    H.handle(routes)
  }
}

//TODO add validation of user input

object CarEndpoints {
  def endpoints[F[_]: ContextShift: Async: Parallel: ConcurrentEffect: Logger, Auth](
      carStore: FireCarStore[F],
      tripStore: FireTripStore[F],
      locationStore: FireLocationStore[F],
      storageService: StorageService[F],
      mailService: MailService[F],
      securedRequestHandler: SecuredRequestHandler[F, UserID, User, Auth]
  )(implicit H: HttpErrorHandler[F, CarError], blocker: Blocker): HttpRoutes[F] =
    new CarEndpoints[F, Auth].endpoints(
      carStore,
      tripStore,
      locationStore,
      storageService,
      mailService,
      securedRequestHandler
    )
}
