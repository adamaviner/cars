package endpoint

import cats._
import cats.effect._
import cats.syntax.all._
import domain.auth.AuthHelpers
import domain.payment._
import domain.trips._
import domain.users.User
import domain.users.UserModel.UserID
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, Response}
import org.typelevel.log4cats.Logger
import services.PaymentService
import stores.firestore.{FirePaymentStore, FireTripStore}
import tsec.authentication.{SecuredRequestHandler, TSecAuthService, asAuthed}

class PaymentEndpoints[F[_]: ContextShift: Async: Parallel: ConcurrentEffect: Logger, Auth](implicit
    blocker: Blocker
) extends Http4sDsl[F] {

  def endpoints(
      paymentStore: FirePaymentStore[F],
      validator: PaymentValidator[F],
      paymentService: PaymentService[F],
      tripStore: FireTripStore[F],
      securedRequestHandler: SecuredRequestHandler[F, UserID, User, Auth]
  )(implicit H: HttpErrorHandler[F, PaymentError]): HttpRoutes[F] = {
    import io.circe.generic.auto._
    import io.circe.syntax._
    import org.http4s.circe.CirceEntityCodec._

    val maintainerEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.maintainerRequired[F, Auth]) {
        case POST -> Root / "dispatch" asAuthed _ =>
          Ok()
          // paymentService.reapDeferredCharges().flatMap(_ => Ok())
      }

    val adminEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.adminRequired[F, Auth]) {
        case GET -> Root / "user" / id asAuthed _ =>
          paymentStore.userPayments(id).flatMap(payments => Ok(payments.asJson))

        case POST -> Root / "hold" / "consume" / tripID / amount asAuthed _ =>
          paymentService
            .chargeIncidental(
              tripID,
              amount.toInt,
              "incidental"
            ) //TODO details for charge descriptor
            .flatMap(_ => Ok())

        case req @ POST -> Root / "chargeIncidental" asAuthed _ =>
          val charge = for {
            charge <- req.request.as[IncidentalCharge]
          } yield charge

          charge.flatMap(c => Ok(c.asJson))
      }

    val userEndpoints: TSecAuthService[User, Auth, F] =
      AuthHelpers.withAuthorization(AuthHelpers.userRequired[F, Auth]) {
        case GET -> Root / "setup" asAuthed user =>
          paymentService
            .setup(user)
            .flatMap(intent => Ok(intent.getClientSecret))

        case GET -> Root / "cards" asAuthed user =>
          paymentService.getCards(user).flatMap(cards => Ok(cards.asJson))

        case req @ POST -> Root / "charge" asAuthed user =>
          def chargeNow(user: User, trip: Trip, paymentMethod: Option[String]): F[Response[F]] =
            for {
              paymentAction <- paymentService.payForTripNow(user, trip, paymentMethod)
              _ <- Logger[F].debug("chargeNow: paymentAction: " + paymentAction)
              _ <- if (paymentAction.isEmpty) paymentService.confirmPaymentAndTrip(user, trip) else Async[F].unit
              res <- Ok(ChargeResponse(paymentAction = paymentAction).asJson)
            } yield res

          for {
            chargeReq <- req.request.as[ChargeRequest]
            trip <- tripStore.get(chargeReq.tripID)
            paymentMethod = chargeReq.paymentMethodID
            res <- chargeNow(user, trip, paymentMethod)
          } yield res


        case POST -> Root / "confirm" / tripID asAuthed user =>
          for {
            _ <- Logger[F].debug("payments.confirm: trip: " + tripID)
            trip <- tripStore.get(tripID)
            _ <- paymentService.confirmPayment(tripID)
            _ <- paymentService.confirmTrip(user, trip)
            res <- Ok()
          } yield res

        case GET -> Root / "authorize" / tripID asAuthed user =>
          for {
            trip <- tripStore.get(tripID)
            _ <- validator.validateUser(user, trip)
            paymentActions <- paymentService.AuthorizeTripPayments(tripID)
            res <- Ok(paymentActions.asJson)
          } yield res

        case POST -> Root / "failed" / tripID asAuthed _ =>
          for {
            _ <- Logger[F].debug("payments: failed: " + tripID)
            _ <- paymentService.clearPayments(tripID) //FIXME what if one payment went through?!
            res <- Ok()
          } yield res

          case POST -> Root / "method" / "remove" / methodId asAuthed _ =>
          for {
            _ <- paymentService.removeMethod(methodId)
            res <- Ok()
          } yield res
      }

    import AuthHelpers.SecuredRequestHandlerBubbleErrors
    val routes = securedRequestHandler.liftServiceBubbleErrors(
      maintainerEndpoints <+> adminEndpoints <+> userEndpoints
    )
    H.handle(routes)
  }
}
