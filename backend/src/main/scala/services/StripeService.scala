package services

import cats._
import cats.effect.{Async, Blocker, ContextShift}
import cats.syntax.all._
import cats.mtl.Raise
import com.stripe.Stripe
import com.stripe.exception.{CardException, SignatureVerificationException}
import com.stripe.model.identity.VerificationSession
import com.stripe.model._
import com.stripe.net.Webhook
import com.stripe.param.identity.VerificationSessionCreateParams
import com.stripe.param.identity.VerificationSessionCreateParams.Options.Document
import com.stripe.param.identity.VerificationSessionCreateParams._
import com.stripe.param._
import domain.payment.{PaymentError, StripeCardException}
import domain.users.User
import org.http4s.Request
import org.http4s.util.CaseInsensitiveString
import org.typelevel.log4cats.Logger

import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters._
import com.stripe.param.RefundCreateParams.Reason
import com.stripe.net.RequestOptions

class StripeService[F[_]: Logger](
    isLive: Boolean = false,
    blocker: Blocker
)(implicit
    ec: ExecutionContext,
    F: Async[F],
    cs: ContextShift[F],
    ME: MonadError[F, Throwable],
    FR: Raise[F, PaymentError]
) {

  val liveKey = "liveKey"
  val testKey = "testKey"
  //TODO test/live
  Stripe.apiKey = liveKey
  Stripe.setMaxNetworkRetries(2)

  def requestOpt: RequestOptions =
    RequestOptions
      .builder()
      .setApiKey(if (isLive) liveKey else testKey)
      .build()

  def setupIntent(customerID: String): F[SetupIntent] = {
    blocker.delay {
      SetupIntent.create(
        SetupIntentCreateParams
          .builder()
          .setCustomer(customerID)
          .build(),
        requestOpt
      )
    }
  }

  def payment(
      amount: Int,
      paymentMethodID: String,
      customerID: String,
      hold: Boolean = false,
      statementDescriptor: String = ""
  ): F[PaymentIntent] = {
    val captureMethod =
      if (hold) PaymentIntentCreateParams.CaptureMethod.MANUAL
      else PaymentIntentCreateParams.CaptureMethod.AUTOMATIC
    val params =
      PaymentIntentCreateParams
        .builder()
        .setCurrency("usd")
        .setAmount(amount)
        .setPaymentMethod(paymentMethodID)
        .setCustomer(customerID)
        .setConfirm(true)
        .setCaptureMethod(captureMethod)
        .setStatementDescriptorSuffix(statementDescriptor)
        .build()

    blocker
      .delay {
        PaymentIntent.create(params, requestOpt)
      }
      .handleErrorWith({ case err: CardException =>
        val msg = s"payment failed:error code: ${err.getCode}\n " +
          s"customer: $customerID, amount: $amount, paymentMethod: $paymentMethodID"
        FR.raise(StripeCardException(msg))
      })
  }

  def verificationSession(): F[VerificationSession] = {
    blocker.delay {
      val options = VerificationSessionCreateParams.Options
        .builder()
        .setDocument(
          Options.Document
            .builder()
            .addAllowedType(Document.AllowedType.DRIVING_LICENSE)
            .setRequireIdNumber(false)
            .setRequireLiveCapture(false)
            .setRequireMatchingSelfie(false)
            .build()
        )
        .build()

      VerificationSession
        .create(
          VerificationSessionCreateParams
            .builder()
            .setType(Type.DOCUMENT)
            .setOptions(options)
            .build(), requestOpt
        )
    }
  }

  def getVerificationSession(event: Event): F[Option[VerificationSession]] = {
    val deserializer = event.getDataObjectDeserializer
    if (!deserializer.getObject.isPresent) {
      Logger[F].error("handleWebhookEvent: no deserializer") *>
        F.pure(none[VerificationSession])
    } else if (
      event.getType == "identity.verification_session.verified" ||
      event.getType == "identity.verification_session.requires_input"
    ) {
      val stripeObject = deserializer.getObject.get
      F.pure(Option(stripeObject.asInstanceOf[VerificationSession]))
    } else
      Logger[F].error("handleWebhookEvent: unsupported event type: " + event.getType()) *>
        F.pure(none[VerificationSession])
  }

  def getEvent(
      payload: String,
      request: Request[F],
      remoteAddress: Option[String]
  ): F[Option[Event]] = {

    val sigHeader = request.headers.get(CaseInsensitiveString("Stripe-Signature"))
    val endpointSecret = "N/A"
    val testEndpointSecret = "N/A"

    if (remoteAddress.isEmpty || !allowedAddresses.contains(remoteAddress.get)) {
      Logger[F].error("validateHeader: Illegal remote address: " + remoteAddress) *>
        F.pure(none[Event])
    } else if (sigHeader.isEmpty) {
      Logger[F].error("validateHeader: No Stripe-Signature") *>
        F.pure(none[Event])
    } else {
      try {
        F.pure(Option(Webhook.constructEvent(payload, sigHeader.get.value, endpointSecret)))
      } catch {
        case e: SignatureVerificationException =>
          Logger[F].error("validateHeader: SignatureVerificationException: " + e.getMessage) *>
            F.pure(none[Event])
      }
    }
  }

  def chargeIncidental(
      paymentID: String,
      amount: Int,
      statementDescriptor: String
  ): F[Unit] = {
    blocker
      .delay {
        val intent = PaymentIntent.retrieve(paymentID, requestOpt)
        if (intent.getStatus == "requires_capture")
          intent.capture(
            PaymentIntentCaptureParams
              .builder()
              .setStatementDescriptorSuffix(statementDescriptor)
              .setAmountToCapture(intent.getAmountCapturable - amount)
              .build(), requestOpt
          )
        else { // refund
          Refund.create(
            RefundCreateParams
              .builder()
              .setAmount(intent.getAmountReceived - amount)
              .setPaymentIntent(intent.getId)
              .build(), requestOpt
          )
        }
      }
      .flatMap(_ => F.unit)
  }

  def createCustomer(user: User): F[Customer] = {
    blocker.delay {
      Customer.create(
        CustomerCreateParams
          .builder()
          .setEmail(user.email)
          .setPhone(user.phone)
          .setName(user.name())
          .build(), requestOpt
      )
    }
  }

  def captureHoldExcept(
      paymentID: String,
      exceptAmount: Long,
      statementDescriptor: String
  ): F[Unit] = {
    blocker
      .delay {
        val intent = PaymentIntent.retrieve(paymentID, requestOpt)
        intent.getAmountCapturable - exceptAmount
      }
      .flatMap(amount => captureHold(paymentID, amount, statementDescriptor))
  }

  def captureHold(
      paymentID: String,
      amount: Long,
      statementDescriptor: String
  ): F[Unit] = {
    if (amount == 0) return cancelHold(paymentID)
    blocker
      .delay {
        val intent = PaymentIntent.retrieve(paymentID, requestOpt)
        intent.capture(
          PaymentIntentCaptureParams
            .builder()
            .setStatementDescriptorSuffix(statementDescriptor)
            .setAmountToCapture(amount)
            .build(), requestOpt
        )
      }
      .flatMap(_ => F.unit)
  }

  def cancelHold(paymentID: String): F[Unit] = {
    blocker
      .delay {
        val intent = PaymentIntent.retrieve(paymentID, requestOpt)
        intent.cancel();
      }
      .flatMap(_ => F.unit)
  }

  def refundPayment(paymentID: String, amount: Long = 0, statementDesc: String): F[Refund] =
    blocker.delay {
      println("refunding: " + amount)
      Refund.create(
        RefundCreateParams
          .builder()
          .setPaymentIntent(paymentID)
          .setAmount(amount)
          .setReason(Reason.REQUESTED_BY_CUSTOMER)
          .build(), requestOpt
      )
    }

  def refundPaymentExcept(
      paymentID: String,
      exceptAmount: Long = 0,
      statementDesc: String
  ): F[Refund] =
    blocker
      .delay {
        val intent = PaymentIntent.retrieve(paymentID, requestOpt)
        val refundAmount = intent.getAmount() - exceptAmount;
        println("refunding: " + refundAmount)
        refundAmount
      }
      .flatMap(amount => refundPayment(paymentID, amount, statementDesc))

  def retrievePayment(paymentID: String): F[PaymentIntent] =
    blocker.delay {
      PaymentIntent.retrieve(paymentID, requestOpt)
    }

  def retrievePaymentMethod(paymentID: String): F[String] =
    blocker.delay {
      val intent = PaymentIntent.retrieve(paymentID, requestOpt)
      intent.getPaymentMethod()
    }

  def updatePayment(
      intent: PaymentIntent,
      amount: Int,
      paymentMethod: Option[String] = None
  ): F[PaymentIntent] =
    blocker.delay {
      val params = PaymentIntentUpdateParams
        .builder()
        .setAmount(amount)
      intent.update(
        if (paymentMethod.isEmpty) params.build()
        else params.setPaymentMethod(paymentMethod.get).build()
      )
    }

  def retrievePayments(paymentIDs: List[String]): F[List[PaymentIntent]] =
    blocker.delay {
      paymentIDs.map(paymentID => {
        PaymentIntent.retrieve(paymentID, requestOpt)
      })
    }

  def confirmPayment(intentID: String): F[PaymentIntent] =
    confirmPayments(List(intentID)).map(_.head)

  def confirmPayments(paymentIDs: List[String]): F[List[PaymentIntent]] =
    blocker.delay {
      paymentIDs.map(paymentID => {
        val intent = PaymentIntent.retrieve(paymentID, requestOpt)
        if (intent.getStatus().equals("requires_confirmation")) {
          println("confirmPayments: requires_confirmation: " + intent.getStatus())
          intent.confirm(requestOpt)
        } else intent
      })
    }

  def removeMethod(methodId: String): F[PaymentMethod] =
    blocker.delay {
      val paymentMethod = PaymentMethod.retrieve(methodId, requestOpt);
      paymentMethod.detach(requestOpt)
    }

  def paymentMethods(customerID: String): F[List[PaymentMethod]] = {
    blocker.delay(
      PaymentMethod
        .list(
          PaymentMethodListParams
            .builder()
            .setCustomer(customerID)
            .setType(PaymentMethodListParams.Type.CARD)
            .build(), requestOpt
        )
        .getData
        .asScala
        .toList
    )
  }

  private val allowedAddresses = List(
    "3.18.12.63",
    "3.130.192.231",
    "13.235.14.237",
    "13.235.122.149",
    "35.154.171.200",
    "52.15.183.38",
    "54.187.174.169",
    "54.187.205.235",
    "54.187.216.72",
    "54.241.31.99",
    "54.241.31.102",
    "54.241.34.107",
    "0:0:0:0:0:0:0:1"
  )
}

// defer until 12hr before start
// hold or charge
// capture or return
