package services

import cats._
import cats.data._
import cats.effect.{Async, Blocker, ContextShift}
import cats.syntax.all._
import cats.mtl.Raise
import com.twilio.Twilio
import com.twilio.`type`.PhoneNumber
import domain.human.{Factor, HumanRequest, VerifyHumanResponse}
import domain.trips.Trip
import domain.users.{FraudRiskError, User, UserError}
import org.http4s.client.Client
import org.typelevel.log4cats.Logger
import io.circe.generic.auto._
import io.circe.{Json, parser}
import org.http4s.client.blaze._
import org.http4s.dsl.io.POST
import io.circe.syntax._
import org.http4s.Header
import org.http4s.implicits.http4sLiteralsSyntax
import org.http4s._
import org.http4s.implicits._
import org.http4s.circe.CirceEntityCodec._
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.multipart.{Multipart, Part}

import scala.concurrent.ExecutionContext
import org.http4s.Headers
import org.http4s.Header
import org.http4s.headers.Authorization
import org.http4s.BasicCredentials

import java.time.Instant
import domain.cars.Car
import com.twilio.rest.api.v2010.account.Message


class SmsService[F[_]: Logger](
  blocker: Blocker
)(implicit
    ec: ExecutionContext,
    F: Async[F],
    cs: ContextShift[F],
    ME: MonadError[F, Throwable],
    FR: Raise[F, UserError]
) {

  val accountSID = "sid"
  val authToken = "token"
  val from = new PhoneNumber("+000000000")

  Twilio.init(accountSID, authToken)

  def send(to: PhoneNumber, body: String): F[Message] = {
    blocker.delay {
      Message.creator(to, from, body).create()
    }
  }

  def sendVerification(user: User, token: String): F[Unit] = {
    val body = s"Your car rentals verification code is: $token"
    send(asPhone(user.phone), body).flatMap(_ => F.unit)
  }

  private def asPhone(phone: String): PhoneNumber =
    new PhoneNumber("+1" + phone.replaceAll("[-\\s\\(\\)]", ""))
}