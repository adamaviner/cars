package services

import cats._
import cats.data._
import cats.effect.{Async, ContextShift}
import cats.syntax.all._
import cats.mtl.Raise
import domain.human.{Factor, HumanRequest, VerifyHumanResponse}
import domain.trips.Trip
import domain.users.{FraudRiskError, User, UserError}
import org.http4s.client.Client
import org.typelevel.log4cats.Logger
import io.circe.generic.auto._
import io.circe.{Json, parser}
import org.http4s.client.blaze._
import org.http4s.dsl.io.POST
import io.circe.syntax._
import org.http4s.Header
import org.http4s.implicits.http4sLiteralsSyntax
import org.http4s._
import org.http4s.implicits._
import org.http4s.circe.CirceEntityCodec._
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.multipart.{Multipart, Part}

import scala.concurrent.ExecutionContext
import org.http4s.Headers
import org.http4s.Header
import org.http4s.headers.Authorization
import org.http4s.BasicCredentials
import java.time.Instant
import domain.cars.Car
import stores.firestore.FireUserStore
import domain.trips.TripInspection
import domain.cars.Location

case class MailRequest(
    from: String = "From <service@mail.com>",
    template: String,
    to: String = "",
    subject: String,
    additionalTo: List[String] = List()
)

class MailService[F[_]: Logger](
    client: Client[F],
    userStore: FireUserStore[F]
)(implicit
    ec: ExecutionContext,
    F: Async[F],
    cs: ContextShift[F],
    ME: MonadError[F, Throwable],
    FR: Raise[F, UserError]
) {

  val baseURI = uri"https://api.mailgun.net/v3/"
  val messagesURI = baseURI / "messages"
  val apiKey = "apiKey"

  case class RequestVariables(
      token: Option[String] = None,
      name: Option[String] = None
  )

  case class InspectionVars(
      tripID: String = "",
      hoursLate: Int = 0
  )

  case class CarVariables(
      carAddress: String = "",
      carName: String = "",
      basePrice: String = "",
      adderName: String = "",
      vin: String = ""
  )

  case class TripVariables(
      dates: String = "",
      name: String = "",
      tripID: String = "",
      orderDate: String = "",
      carAddress: String = "",
      startDate: String = "",
      endDate: String = "",
      carImage: String = "",
      carName: String = "",
      tripPrice: String = "",
      basePrice: String = "",
      taxes: String = "",
      discounts: String = "",
      protection: String = "",
      deposit: String = "",
      totalPrice: String = "",
      updateFee: String = "",
      refunded: String = "",
      incidental: String = "",
      // Sender_Address: String = "",
      // Sender_City: String = "Los Angeles",
      // Sender_State: String = "California",
      // Sender_Zip: String = "",
      tripType: String = "",
      manualDescription: String = "",
      unsubscribe: String = ""
  ) {
    def weblink: String = "http://localhost:8080/trip/" + tripID
  }

  object TripVariables {
    def from(
        user: User,
        trip: Trip,
        car: Car,
        inspection: TripInspection = TripInspection("")
    ): TripVariables = {
      val tripReq = trip.asTripRequest
      val detailedPrice = tripReq.detailedPrice(car, Some(user))
      val updateFee = tripReq.updateFee(car)
      from(trip).copy(
        name = user.firstName,
        carImage = car.transformedHeroImage,
        carName = car.label,
        basePrice = detailedPrice.basePrice / 100.0 + "",
        discounts = detailedPrice.discount / 100.0 + "",
        protection = detailedPrice.protection / 100.0 + "",
        deposit = detailedPrice.deposit / 100.0 + "",
        totalPrice = detailedPrice.total / 100.0 + "",
        taxes = detailedPrice.tax / 100.0 + "",
        updateFee = if (updateFee == 0) "FREE" else "$" + updateFee / 100.0,
        refunded = (detailedPrice.total - updateFee) / 100.0 + "",
        incidental = inspection.chargeAmount / 100.0 + "",
      )
    }

    def from(trip: Trip): TripVariables = {
      val dates = Trip.humanFormat(trip.startDate) + " - " + Trip.humanFormat(trip.endDate)
      TripVariables(
        dates = dates,
        tripID = trip.id,
        orderDate = Trip.humanFormat(Instant.now().getEpochSecond),
        startDate = Trip.humanFormat(trip.startDate),
        endDate = Trip.humanFormat(trip.endDate),
        tripPrice = "$" + trip.price / 100.0,
        tripType = trip.tripTypeString,
        manualDescription = trip.manualDescription
      )
    }
  }

  def send(request: MailRequest, vars: Json): F[Unit] = {
    val authHeaders = Authorization(BasicCredentials("api", apiKey))
    val multipartRequest = multipart(request, vars.noSpaces)
//    val multipartReq = multipart(request, vars)
    val req = Request[F](Method.POST, messagesURI)
      .withEntity(multipartRequest)
      .withHeaders(multipartRequest.headers.put(authHeaders))

    client
      .expect[Json](req)
      .adaptError({ case e: Exception =>
        println("Exception: " + e)
        println("msg: " + e.getMessage)
        e
      })
      .flatMap(_ => F.unit)
  }

  def sendToCustodiansAndAdmins(request: MailRequest, vars: Json): F[Unit] = {
    userStore
      .getCustodianAndAdminEmails()
      .flatMap(recepients => {
        val req = MailRequest(
          template = request.template,
          to = recepients.head,
          additionalTo = recepients.tail,
          subject = request.subject
        )
        send(req, vars)
      })
  }

  def sendMailVerification(user: User, token: String): F[Unit] = {
    //TODO subject copy
    val subject = "Welcome to Rental Cars, please verify your email"
    val req = MailRequest(template = "confirm.email", to = user.email, subject = subject)
    val vars = RequestVariables(token = Some(token), name = Some(user.firstName))
    send(req, vars.asJson)
  }

  def sendPasswordReset(user: User, token: String): F[Unit] = {
    val subject = "Password reset"
    val req = MailRequest(template = "reset", to = user.email, subject = subject)
    val vars = RequestVariables(token = Some(token), name = Some(user.firstName))
    send(req, vars.asJson)
  }

  def sendBookingInfo(user: User, trip: Trip, car: Car, location: Location): F[Unit] = {
    //TODO subject
    val vars = TripVariables.from(user, trip, car).copy(carAddress = location.address)
    val subject = s"Trip booked for ${vars.dates}"
    val req = MailRequest(template = "confirm.trip", to = user.email, subject = subject)
    send(req, vars.asJson)
  }

  def sendIncidentalCharge(
      user: User,
      trip: Trip,
      car: Car,
      inspection: TripInspection
  ): F[Unit] = {
    val vars = TripVariables.from(user, trip, car, inspection)
    val subject = s"Incidental charges for your trip"
    val req = MailRequest(template = "incidental", to = user.email, subject = subject)
    send(req, vars.asJson)
  }

  def sendTripCancelled(user: User, trip: Trip, car: Car): F[Unit] = {
    //TODO subject
    sendTripCancelledToCustodians(user, trip, car)
    val vars = TripVariables.from(user, trip, car)
    val subject = s"Trip for ${vars.dates} Cancelled"
    val req = MailRequest(template = "cancel.trip", to = user.email, subject = subject)
    send(req, vars.asJson)
  }

  // def sendPaymentRequest(user: User, trip: Trip, car: Car): F[Unit] = {
  //   println("sendPaymentRequest")
  //   val subject = s"Payment required for upcoming trip"
  //   val req = MailRequest(template = "trip.payment", to = user.email, subject = subject)
  //   val vars = TripVariables.from(user, trip, car)
  //   send(req, vars.asJson)
  // }

  //TODO needed?
  def sendVerificationSuccess(user: User): F[Unit] = {
    //TODO style
    val subject = s"Your license has been verified, you are ready to rent a car!"
    val req = MailRequest(template = "license.verified", to = user.email, subject = subject)
    val vars = RequestVariables(name = Some(user.firstName))
    send(req, vars.asJson)
  }

  def sendVerificationFailure(user: User): F[Unit] = {
    //TODO style
    val subject = s"Problem verifying your license"
    val req = MailRequest(template = "license.error", to = user.email, subject = subject)
    val vars = RequestVariables(name = Some(user.firstName))
    send(req, vars.asJson)
  }

  def sendLateForInspection(trip: Trip): F[Unit] = {
    val subject = s"Trip is late for inspection"
    val request = MailRequest(template = "inspection.late", subject = subject)
    val vars = TripVariables.from(trip = trip)
    sendToCustodiansAndAdmins(request, vars.asJson)
    // userStore
    //   .getAdminEmails()
    //   .flatMap(recepients => {
    //     val req = MailRequest(
    //       template = "inspection.late",
    //       to = recepients.head,
    //       additionalTo = recepients.tail,
    //       subject = subject
    //     )
    //     val vars = TripVariables.from(trip = trip)
    //     send(req, vars.asJson)
    //   })
  }

  def sendCarAdded(car: Car, location: Location, user: User): F[Unit] = {
    val subject = s"custodian ${user.firstName} ${user.lastName} added a car"
    val req = MailRequest(template = "car.added", subject = subject)
    val vars = CarVariables(
      carAddress = location.label,
      carName = car.label,
      basePrice = "$" + car.price / 100.0,
      adderName = user.name(),
      vin = car.vin,
    )
    sendToCustodiansAndAdmins(req, vars.asJson)
  }

  def sendTripBookedToCustodians(user: User, trip: Trip, car: Car, location: Location): F[Unit] = {
    val subject = s"user ${user.firstName} ${user.lastName} booked a trip"
    val vars = TripVariables.from(user, trip, car).copy(carAddress = location.address)
    val req = MailRequest(template = "admin.trip.booked", to = user.email, subject = subject)
    sendToCustodiansAndAdmins(req, vars.asJson)
  }

   def sendTripCancelledToCustodians(user: User, trip: Trip, car: Car): F[Unit] = {
    //TODO subject
    val vars = TripVariables.from(user, trip, car)
    val subject = s"User ${user.name} cancelled a trip"
    val req = MailRequest(template = "admin.trip.cancelled", to = user.email, subject = subject)
    sendToCustodiansAndAdmins(req, vars.asJson)
  }

  def sendManualBookingToCustodians(user: User, trip: Trip, car: Car): F[Unit] = {
    val subject = s"custodian ${user.firstName} ${user.lastName} manually scheduled a trip"
    val vars = TripVariables.from(user, trip, car)//.copy(carAddress = location.address)
    val req = MailRequest(template = "admin.trip.manual", to = user.email, subject = subject)
    sendToCustodiansAndAdmins(req, vars.asJson)
  }

  private def multipart(r: MailRequest, vars: String): Multipart[F] = {
    val additionalTos: List[Part[F]] =
      r.additionalTo.map(recipient => Part.formData[F]("to", recipient))
    val parts: List[Part[F]] = List(
      Part.formData[F]("template", r.template),
      Part.formData[F]("subject", r.subject),
      Part.formData[F]("to", r.to),
      Part.formData[F]("from", r.from),
      Part.formData[F]("h:X-Mailgun-Variables", vars)
    ).concat(additionalTos)
    Multipart[F](
      parts.toVector
    )
  }
}
