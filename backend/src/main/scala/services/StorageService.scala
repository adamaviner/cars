package services

import cats._
import cats.data._
import cats.effect.{Async, ConcurrentEffect, ContextShift}
import cats.syntax.all._
import cats.mtl.Raise
import com.google.cloud.storage.{Blob, Bucket, StorageException}
import domain.human.{Factor, HumanRequest, VerifyHumanResponse}
import domain.trips.Trip
import domain.users.{FraudRiskError, User, UserError}
import org.http4s.client.Client
import org.typelevel.log4cats.Logger
import io.circe.generic.auto._
import io.circe.{Json, parser}
import org.http4s.client.blaze._
import org.http4s.dsl.io.POST
import io.circe.syntax._
import org.http4s.Header
import org.http4s.implicits.http4sLiteralsSyntax
import org.http4s._
import org.http4s.implicits._
import org.http4s.circe.CirceEntityCodec._
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.multipart.{Multipart, Part}

import scala.concurrent.ExecutionContext
import org.http4s.Headers
import org.http4s.Header
import org.http4s.headers.Authorization
import org.http4s.BasicCredentials

import java.io.InputStream
import scala.util.Random
import cats.effect.Blocker


class StorageService[F[_]: Async: Parallel: ConcurrentEffect: Logger](
  bucket: Bucket,
  blocker: Blocker
)(implicit
    ec: ExecutionContext,
    F: Async[F],
    cs: ContextShift[F],
    ME: MonadError[F, Throwable],
    FR: Raise[F, UserError]
) {

  def uploadPhoto(path: String, is: InputStream, filename: Option[String]): F[Blob] = {
    val name = s"$path/" + filename.getOrElse(Random.nextInt(10000))
    Logger[F].debug(s"uploadPhoto: path $path, name: $name is: $is") *> blocker
      .delay(bucket.create(name, is))
      .handleErrorWith {
        case err: StorageException => {
          Logger[F].error(s"uploadPhoto: path $path, name: $name storageException: $err") *> ME
            .raiseError(err) //TODO more robust, maybe retry if fails
        }
      }
  }

  def uploadPhotos(path: String, m: Multipart[F]): F[List[String]] = {
    val blobs = m.parts.parTraverse(part =>
      part.body
        .through(fs2.io.toInputStream)
        .evalMap(is => uploadPhoto(path, is, part.filename))
        .compile
        .lastOrError
    )

    blobs.map(_.map(_.getMediaLink).toList)
  }

}
