package services

import cats._
import cats.data._
import cats.effect.{Async, ContextShift}
import cats.syntax.all._
import cats.mtl.Raise
import domain.human.{Factor, HumanRequest, VerifyHumanResponse}
import domain.users.{FraudRiskError, UserError}
import org.http4s.client.Client
import org.typelevel.log4cats.Logger
import io.circe.generic.auto._
import io.circe.{Json, parser}
import org.http4s.client.blaze._
import org.http4s.dsl.io.POST
import io.circe.syntax._
import org.http4s.Header
import org.http4s.implicits.http4sLiteralsSyntax
import org.http4s.circe.CirceEntityCodec._
import org.http4s.client.dsl.Http4sClientDsl

import scala.concurrent.ExecutionContext

class HumanVerificationService[F[_]: Logger](
    client: Client[F]
)(implicit
    ec: ExecutionContext,
    F: Async[F],
    cs: ContextShift[F],
    ME: MonadError[F, Throwable],
    FR: Raise[F, UserError]
) {

  private val dsl = new Http4sClientDsl[F] {}
  import dsl._

  def verify(human: HumanRequest): F[Unit] = {
    val minScore = 700
    //TODO switch to real endpoint - https://api.microbilt.com/IDVerify/GetReport
    val req = POST(human.asJson, uri"https://apitest.microbilt.com/IDVerify/GetReport")
      .map(r => r.putHeaders(Header("Authorization", "Bearer token")))
    client
      .expect[Json](req)
      .adaptError({ case e: Exception =>
        println("Exception: " + e)
        e
      })
      .flatMap(resp => {
//        val json = parser.parse(resp).getOrElse(Json.Null)
        val scoreOpt = VerifyHumanResponse.parseJson(resp)
        scoreOpt match {
          case None =>
            Logger[F].error("verify: no subjects in response") *>
              FR.raise(FraudRiskError("no subject"))
          case Some(score) =>
            if (score.Value < minScore)
              Logger[F].error(s"verify: Fraud score too low: ${score.Value}") *>
                FR.raise(FraudRiskError("Fraud score too low"))
            else if (score.Alert.nonEmpty)
              Logger[F].error(s"verify: alerts: ${score.Alert}") *>
                FR.raise(FraudRiskError("Alerts"))
            else if (score.Factor.nonEmpty)
              Logger[F].debug(s"verify: factors: ${score.Factor}")
            else F.unit
        }
      })

  }
}
