package services

import cats._
import cats.data._
import cats.syntax.all._
import cats.MonadError
import cats.effect.{Async, Blocker, ContextShift}
import cats.mtl.Raise
import com.stripe.Stripe
import com.stripe.model.{Customer, PaymentIntent, SetupIntent, StripeError}
import com.stripe.param.{CustomerCreateParams, PaymentIntentCreateParams, SetupIntentCreateParams}
import domain.users.UserModel.UserID
import domain.payment.{
  AlreadyCharged,
  BadUserTrip,
  CardDetails,
  DeferredPayment,
  IntentError,
  InvalidStatus,
  NoCustomer,
  Payment,
  PaymentError
}
import domain.trips.Trip
import domain.users.User
import org.typelevel.log4cats.Logger
import stores.firestore.{FirePaymentStore, FireTripStore, FireUserStore, FireLocationStore}
import domain.TimeConversions.LongWithDaysBetween

import java.time.Instant
import java.time.temporal.ChronoUnit
import scala.concurrent.ExecutionContext
import stores.firestore.FireCarStore
import domain.payment.PaymentType
import domain.payment.PaymentNotFound
import domain.payment.NoPaymentMethods
import domain.payment.PaymentAction
import com.stripe.model.PaymentMethod

class PaymentService[F[_]: Logger: Parallel](
    stripe: StripeService[F],
    paymentStore: FirePaymentStore[F],
    tripStore: FireTripStore[F],
    userStore: FireUserStore[F],
    carStore: FireCarStore[F],
    locationStore: FireLocationStore[F],
    mailService: MailService[F]
)(implicit
    ec: ExecutionContext,
    F: Async[F],
    cs: ContextShift[F],
    ME: MonadError[F, Throwable],
    FR: Raise[F, PaymentError]
) {

  val daysBeforeTripStartToCharge = 1 //days
  val collateralAmount = 30000 // hold for any damage or additional costs

  def setup(user: User): F[SetupIntent] = {
    for {
      customerID <- getStripeID(user)
      setupIntent <- stripe.setupIntent(customerID)
    } yield setupIntent
  }

  private def payment(
      user: User,
      trip: Trip,
      hold: Boolean = false,
      amount: Int = 0,
      statementDescriptor: String = "trip",
      paymentMethod: Option[String] = None
  ): F[PaymentIntent] = {
    def getPaymentMethodOrNot(customerID: String): F[String] = {
      if (paymentMethod.nonEmpty) paymentMethod.get.pure[F]
      else
        stripe
          .paymentMethods(customerID)
          .flatMap(methods =>
            methods match {
              case head :: next => head.getId.pure[F]
              case Nil          => FR.raise(NoPaymentMethods(customerID))
            }
          )
    }
    val price = trip.price
    for {
      customerID <- paymentStore.getStripeId(user.id)
      _ <- Logger[F].debug(s"payment: customer: $customerID for trip: $trip")

      paymentMethodID <- getPaymentMethodOrNot(customerID)

      paymentIntent <- stripe.payment(
        price,
        paymentMethodID,
        customerID,
        hold,
        statementDescriptor
      )
    } yield paymentIntent
  }

  def chargeTrip(user: User, trip: Trip, paymentMethod: Option[String]): F[Payment] = {
    val hold = trip.canHold
    val paymentIntent = payment(user, trip = trip, paymentMethod = paymentMethod, hold = hold)
    paymentIntent.flatMap(pIntent =>
      Logger[F].debug(s"charging trip: trip: $trip") *>
        paymentStore
          .add(
            createPayment(pIntent, user, trip).copy(
              paymentType = trip.paymentType
            )
          )
    )
  }

  def removeMethod(methodId: String): F[PaymentMethod] =
    stripe.removeMethod(methodId: String)

  def confirmPayment(tripID: String): F[Unit] =
    for {
      _ <- Logger[F].info("confirmPayment: tripID: " + tripID)
      payment <- paymentStore.getTripPayment(tripID)
      intent <- stripe.retrievePayment(payment.paymentID)
      _ <-
        if (intent.getStatus != "succeeded" && intent.getStatus != "requires_capture")
          FR.raise(InvalidStatus(s"not success: tripID: $tripID - " + intent))
        else F.unit
      _ <- paymentStore.markPaymentAsDone(payment)
      _ <- tripStore.payed(tripID)
    } yield {}

  def confirmPaymentAndTrip(user: User, trip: Trip): F[Unit] =
    confirmPayment(trip.id) *> confirmTrip(user, trip)

  def clearPayments(tripID: String): F[Unit] =
    paymentStore.clearPayments(tripID)

  def payForTripNow(
      user: User,
      trip: Trip,
      paymentMethod: Option[String]
  ): F[Option[PaymentAction]] = {
    // val intent = for {
    //   payment <- chargeTrip(user, trip, paymentMethod)
    // } yield payment.paymentIntent.get
    val intent = chargeTrip(user, trip, paymentMethod).map(_.paymentIntent.get)
    intent.flatMap(getIntentAction)
  }

  def updateTripPayment(
      user: User,
      trip: Trip,
      paymentMethod: Option[String],
      updatePrice: Int
  ): F[Option[PaymentAction]] = {
    // TODO optimize for when it's less then current charge, we could leave the hold as is, but might be confusing, better to change
    // we can just refund some of the first payment and stay at current price,
    // tryPartialRefund(trip, updatePrice).flatMap(success =>
    //   if (success) none[PaymentAction].pure[F]
    //   else
    for {
      method <-
        if (paymentMethod.nonEmpty) paymentMethod.get.pure[F]
        else
          paymentStore
            .getTripPayment(trip.id)
            .flatMap(payment => stripe.retrievePaymentMethod(payment.paymentID))
      _ <- refundTrip(trip.id, updatePrice) //TODO copy "update"?
      action <- payForTripNow(user, trip, Some(method))
    } yield action
    // )
  }

  def tryPartialRefund(trip: Trip, updatePrice: Int): F[Boolean] = {
    val newPriceInCent = updatePrice
    paymentStore
      .getTripPayment(trip.id)
      .flatMap(tripPayment => {
        if (
          tripPayment.paymentType == PaymentType.Hold ||
          tripPayment.amount < updatePrice
        )
          false.pure[F]
        else
          for {
            tripPayment <- paymentStore.getTripPayment(trip.id)
            intent <- stripe.retrievePayment(tripPayment.paymentID)
            success <-
              if (intent.getAmount > newPriceInCent)
                refundTrip(tripPayment.tripID, updatePrice) *> true.pure[F]
              else false.pure[F]
          } yield success
      })
  }

  def AuthorizeTripPayments(tripID: String): F[Option[PaymentAction]] = {
    for {
      payment <- paymentStore.getTripPayment(tripID)
      intent <- stripe.retrievePayment(payment.paymentID)
      action <- getIntentAction(intent)
    } yield action
  }

  private def getIntentAction(intent: PaymentIntent): F[Option[PaymentAction]] = {
    val status = intent.getStatus
    if (status == "succeeded" || status == "requires_capture") none[PaymentAction].pure[F]
    else if (
      status == "requires_action" && intent.getNextAction.getType
        .equals("use_stripe_sdk") || status == "requires_payment_method"
    )
      Option(
        PaymentAction(
          intent.getClientSecret,
          requiresPaymentMethod = status == "requires_payment_method"
        )
      ).pure[F]
    else
      FR.raise(
        InvalidStatus("handleIntentStatus: status: " + status + " intent: " + intent)
      )
  }

  def confirmTrip(user: User, trip: Trip): F[Unit] =
    for {
      _ <- tripStore.confirmBooking(trip.id)
      car <- carStore.getCar(trip.vin)
      location <- locationStore.get(car.locationID)
      _ <- mailService.sendBookingInfo(user, trip, car, location)
      _ <- mailService.sendTripBookedToCustodians(user, trip, car, location)
    } yield {}

  def refundTrip(
      tripID: String,
      exceptAmount: Int = 0,
      statementDesc: String = "Refund"
  ): F[Unit] = {
    for {
      tripPayment <- paymentStore.getTripPayment(tripID)
      _ <-
        if (tripPayment.paymentType == PaymentType.Charge)
          stripe.refundPaymentExcept(tripPayment.paymentID, exceptAmount, statementDesc)
        else stripe.captureHold(tripPayment.paymentID, exceptAmount, statementDesc)
      _ <- paymentStore.markPaymentAsRefunded(tripPayment.id)
    } yield {}
  }

  def refundCollateral(
      tripID: String,
      exceptIncidentalCharge: Int = 0,
      statementDesc: String = "Incidental"
  ): F[Unit] = {
    for {
      tripPayment <- paymentStore.getTripPayment(tripID)
      collateralAmount = tripPayment.collateralAmount
      refundAmount = (collateralAmount - exceptIncidentalCharge)
      tripPaymentID = tripPayment.paymentID
      _ <-
        if (tripPayment.paymentType == PaymentType.Charge)
          stripe.refundPayment(tripPaymentID, refundAmount, statementDesc)
        else stripe.captureHold(tripPaymentID, refundAmount, statementDesc)
    } yield {}
  }

  def chargeIncidental(
      tripID: String,
      amount: Int,
      statementDescriptor: String
  ): F[Unit] =
    refundCollateral(tripID, amount, statementDescriptor)

  def getCards(user: User): F[List[CardDetails]] =
    for {
      customerID <- getStripeID(user)
      methods <- stripe.paymentMethods(customerID)
      cards = methods.map(m => CardDetails.fromCard(m.getId, m.getCard))
    } yield cards

  private def createPayment(pIntent: PaymentIntent, user: User, trip: Trip): Payment =
    createPayment(pIntent, user.id, trip.id)

  private def createPayment(pIntent: PaymentIntent, userID: UserID, tripID: String): Payment =
    Payment(
      "",
      tripID,
      userID,
      pIntent.getCustomer,
      pIntent.getId,
      pIntent.getAmount.toInt,
      collateralAmount = collateralAmount,
      requiresAction = pIntent.getStatus.equals("requires_action"),
      paymentIntent = Some(pIntent)
    )

  private def getTrip(tripID: String, user: User): F[Trip] = {
    for {
      trip <- tripStore.get(tripID)
      _ <- if (trip.userID != user.id) FR.raise(BadUserTrip(tripID)) else F.unit
    } yield trip
  }

  def attachStripeCustomer(user: User): F[String] = {
    getStripeID(user)
  }

  private def getStripeID(user: User): F[String] = {
    paymentStore
      .getStripeId(user.id)
      .recoverWith { case e: NoCustomer =>
        for {
          _ <- Logger[F].debug("no customer")
          customer <- stripe.createCustomer(user)
          _ <- paymentStore.setStripeId(user.id, customer.getId)
        } yield customer.getId;
      }
  }
}
