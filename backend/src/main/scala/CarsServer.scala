import cats._
import cats.data._
import cats.effect._
import cats.syntax.all._
import cats.mtl.Handle.handleForApplicativeError
import fs2.Stream
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.{Router, Server, middleware}
import domain.auth.AuthHelpers
import domain.cars.{CarError, CarHttpErrorHandler}
import domain.users.UserModel.UserID
import endpoint.{
  CarEndpoints,
  HttpErrorHandler,
  PaymentEndpoints,
  TextEndpoints,
  TripEndpoints,
  UserEndpoints,
  LocationEndpoints
}
import org.http4s.server.middleware.{CORS, CORSConfig}
import org.http4s.{Header, HttpRoutes, Request, Status}
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import stores.firestore.{
  FireAuthStore,
  FireCarStore,
  FirePaymentStore,
  FireTokenStore,
  FireTripStore,
  FireUserStore,
  FirestoreDB,
  FireLocationStore,
  TextStore
}
import stores.inmemory.InmemoryUserStore
import tsec.authentication.{AugmentedJWT, SecuredRequestHandler}
import tsec.common.SecureRandomId
import tsec.mac.jca.HMACSHA256
import tsec.passwordhashers.jca.BCrypt

import java.io.FileInputStream
import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.global
import com.olegpy.meow.hierarchy._
import domain.payment.{PaymentError, PaymentHttpErrorHandler, PaymentValidator}
import domain.trips.{TripError, TripHttpErrorHandler, TripValidator}
import domain.users.{UserError, UserHttpErrorHandler, UserValidator}
import services.{
  HumanVerificationService,
  MailService,
  PaymentService,
  SmsService,
  StorageService,
  StripeService
}

import scala.concurrent.duration.DurationInt

object CarsServer {

  private val originConfig = CORSConfig(
    anyOrigin = false,
    allowedOrigins = Set(
      "http://localhost:8080",
    ),
    allowCredentials = true,
    maxAge = 1.day.toSeconds
  )

  def create[F[_]: ContextShift: ConcurrentEffect: Parallel: Timer](implicit
      ec: ExecutionContext,
      blocker: Blocker
  ): Resource[F, Server[F]] = {
    //    type F[A] EitherT[F, CarError, A]

    Resource.eval(Slf4jLogger.fromName[F](name = "debugLogger")).flatMap { implicit logger =>
      implicit def paymentHttpErrorHandler: HttpErrorHandler[F, PaymentError] =
        new PaymentHttpErrorHandler[F]
      implicit def tripHttpErrorHandler: HttpErrorHandler[F, TripError] =
        new TripHttpErrorHandler[F]
      implicit def carHttpErrorHandler: HttpErrorHandler[F, CarError] = new CarHttpErrorHandler[F]
      implicit def userHttpErrorHandler: HttpErrorHandler[F, UserError] =
        new UserHttpErrorHandler[F]

      for {
        db <- FirestoreDB[F]()
        client <- BlazeClientBuilder[F](ec).resource
        bucket = FirestoreDB.bucket
        userStore = new FireUserStore[F](db, FirestoreDB.secureBucket, blocker)
        tokenStore = new FireTokenStore[F](db)
        locationStore = new FireLocationStore[F](db)
        tripStore = new FireTripStore[F](db)
        paymentStore = new FirePaymentStore[F](db)
        carStore = new FireCarStore[F](db)
        authStore = FireAuthStore[F, SecureRandomId, UserID](db, s => SecureRandomId.coerce(s.id))
        textStore = new TextStore[F](db)

        storageService = new StorageService[F](bucket, blocker)
        humanVerificationService = new HumanVerificationService[F](client)
        mailService = new MailService[F](client, userStore)
        smsService = new SmsService[F](blocker)
        stripeService = new StripeService[F](isLive = true, blocker)
        
        paymentService = new PaymentService[F](
          stripeService,
          paymentStore,
          tripStore,
          userStore,
          carStore,
          locationStore,
          mailService
        )

        tripValidator = new TripValidator[F]()
        userValidator = new UserValidator[F](humanVerificationService)
        paymentValidator = new PaymentValidator[F]()

        authenticator = AuthHelpers.bearerTokenAuthenticator[F, UserID](authStore, userStore)
        srHandler = SecuredRequestHandler(authenticator)
        crypt = BCrypt.syncPasswordHasher[F]

        httpApp = Router(
          "/users" -> UserEndpoints
            .endpoints(
              userStore,
              userValidator,
              tokenStore,
              mailService,
              smsService,
              stripeService,
              paymentService,
              crypt,
              srHandler
            ),
          "/cars" -> CarEndpoints
            .endpoints(carStore, tripStore, locationStore, storageService, mailService, srHandler),
          "/locations" -> new LocationEndpoints().endpoints(locationStore, srHandler),
          "/trips" -> TripEndpoints
            .endpoints(
              tripStore,
              tripValidator,
              carStore,
              userStore,
              mailService,
              paymentService,
              storageService,
              srHandler
            ),
          "/payments" -> new PaymentEndpoints()
            .endpoints(paymentStore, paymentValidator, paymentService, tripStore, srHandler),
          "/texts" -> new TextEndpoints().endpoints(textStore, srHandler)
        ).orNotFound

        corsHttpApp = middleware.CORS(httpApp, originConfig)
        // With Middlewares in place
        finalHttpApp = middleware.Logger.httpApp(logHeaders = true, logBody = true)(corsHttpApp)

        server <- BlazeServerBuilder[F](global)
          .bindHttp(sys.env.getOrElse("PORT", "8082").toInt, "0.0.0.0") //TODO change port?
          .withHttpApp(finalHttpApp)
          .resource
        _ <- Resource.eval(Logger[F].debug(s"starting"))
      } yield server
    }
  }
}
