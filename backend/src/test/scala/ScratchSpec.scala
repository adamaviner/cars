import cats.effect.{Blocker, ContextShift, IO}
import com.google.cloud.Timestamp
import domain.users.User
import io.circe.Json
import org.http4s.client.{Client, JavaNetClientBuilder}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import stores.firestore.FirestoreDB

import scala.concurrent.{Await, ExecutionContext}
import stores.firestore.FirestoreIO._
import cats._
import cats.data._
import cats.syntax.all._
import org.http4s.{BasicCredentials, Method, Request, _}
import org.http4s.headers.Authorization
import org.http4s.multipart.{Multipart, Part}

import java.util.concurrent.Executors
import org.http4s.circe.CirceEntityCodec._
import org.http4s.implicits.http4sLiteralsSyntax
import org.http4s._
import org.http4s.client.dsl.io._
import org.http4s.implicits._

import scala.concurrent.duration.Duration
case class MailRequest(
    from: String = "Excited User <mailgun@carrentals.com>",
    text: String,
    to: String,
    subject: String
){
    def toMap(): Map[String, String] =
      Map("from" -> from,
        "text" -> text,
        "to" -> to,
        "subject" -> subject
      )
}

class ScratchSpec extends AnyFlatSpec with should.Matchers {
  implicit lazy val testEc: ExecutionContext = ExecutionContext.Implicits.global
  implicit val contextShiftIO: ContextShift[IO] = IO.contextShift(testEc)

  val db = FirestoreDB.instance()

  def createClient(): Client[IO] = {
    val blockingPool = Executors.newFixedThreadPool(5)
    val blocker = Blocker.liftExecutorService(blockingPool)
    JavaNetClientBuilder[IO](blocker).create
  }

  "Scratch" should "form" in {
     val baseURI =
      uri"https://api.mailgun.net/v3/sandbox50e0138bad17413a93c11d84e3979214.mailgun.org"
    val messagesURI = baseURI / "messages"
    val apiKey = "8b2f11107a83d5e93a9bddcabb50c1f2-1d8af1f4-bf3b9158"

//    val mailgun = new Mailgun("sandbox50e0138bad17413a93c11d84e3979214.mailgun.org", apiKey)
//    val response = mailgun.send(Message.simple(
//      from = EmailAddress("service@rentalcars.com", "Excite"),
//      to = EmailAddress("adamaviner@gmail.com", "Joe User"),
//      subject = "Mailgun4s Rules!",
//      text = "This is the testing text"
//    ))
//
//    val result = Await.result(response, Duration.Inf)
//    println(s"Result: $result")

    val request = MailRequest(text = "text", to = "adamaviner@gmail.com", subject = "subject")
    val authHeaders = Authorization(BasicCredentials("api", apiKey))
//    val varHeaders = Header("X-Mailgun-Variable", vars.asJson.noSpaces)
    val client = createClient()

    val requestURI = messagesURI.withQueryParams(request.toMap)
    val multipart = Multipart[IO](
      Vector(
        Part.formData("template", "confirm"),
        Part.formData("subject", "This is subject."),
        Part.formData("to", "adamaviner@gmail.com"),
        Part.formData("from", "service@rentalcars.com"),
        Part.formData("h:X-Mailgun-Variables", """{"token": "test"}""")
      ))
      // val req = Method.POST(multipart, messagesURI, headers = multipart.headers.put(authHeaders))
      // .authHeaders
    val req = Request[IO](Method.POST, messagesURI)
    .withEntity(multipart)
    .withHeaders(multipart.headers.put(authHeaders))

    val res = client.expect[Json](req)
      .adaptError({
        case e: Exception =>
          println("Exception: " + e)
          println("msg: " + e.getMessage)
          e
      }).unsafeRunSync()

    println("res: " + res)
  }

  "Scratch" should "temp test" ignore { //TODO move to different spec
    val baseURI =
      uri"https://api.mailgun.net/v3/sandbox50e0138bad17413a93c11d84e3979214.mailgun.org"
    val messagesURI = baseURI / "messages"
    val apiKey = "8b2f11107a83d5e93a9bddcabb50c1f2-1d8af1f4-bf3b9158"

//    val mailgun = new Mailgun("sandbox50e0138bad17413a93c11d84e3979214.mailgun.org", apiKey)
//    val response = mailgun.send(Message.simple(
//      from = EmailAddress("service@rentalcars.com", "Excite"),
//      to = EmailAddress("adamaviner@gmail.com", "Joe User"),
//      subject = "Mailgun4s Rules!",
//      text = "This is the testing text"
//    ))
//
//    val result = Await.result(response, Duration.Inf)
//    println(s"Result: $result")

    val request = MailRequest(text = "text", to = "adamaviner@gmail.com", subject = "subject")
    val authHeaders = Authorization(BasicCredentials("api", apiKey))
//    val varHeaders = Header("X-Mailgun-Variable", vars.asJson.noSpaces)
    val client = createClient()

    val requestURI = messagesURI.withQueryParams(request.toMap)
    val multipart = Multipart[IO](
      Vector(
        Part.formData("text", "This is text."),
        Part.formData("subject", "This is subject."),
        Part.formData("to", "adamaviner@gmail.com"),
        Part.formData("from", "service@rentalcars.com"),
      ))
    val req = Request[IO](Method.POST, requestURI)
      .withHeaders(authHeaders)
//      .withEntity(multipart)

    val res = client.expect[Json](req)
      .adaptError({
        case e: Exception =>
          println("Exception: " + e)
          println("msg: " + e.getMessage)
          e
      }).unsafeRunSync()

    println("res: " + res)
  }
}
