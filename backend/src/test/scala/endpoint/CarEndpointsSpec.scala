package endpoint

import domain.users.{User, UserStore, UserUpdate}
import org.http4s.{Header, HttpApp, MediaType, Uri}
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import tsec.authentication.{Authenticator, BearerTokenAuthenticator, JWTAuthenticator, SecuredRequestHandler, TSecBearerToken}
import tsec.mac.jca.HMACSHA256
import tsec.passwordhashers.jca.BCrypt

import scala.concurrent.duration._
import cats.syntax.all._
import cats.effect._
import domain.auth.{AuthHelpers, LoginRequest, SignupRequest}
import domain.users.UserModel.UserID
import org.http4s.implicits._
import org.http4s.circe.CirceEntityCodec._
import io.circe.syntax._
import io.circe.generic.auto._
import org.http4s.circe._
import org.typelevel.log4cats.SelfAwareStructuredLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import stores.firestore.{FireAuthStore, FireCarStore, FireTripStore, FireUserStore, FirestoreDB}
import stores.inmemory.{AuthMemoryStore, InmemoryUserStore}
import tsec.common.SecureRandomId
import com.olegpy.meow.hierarchy._
import cats.mtl._
import cats.data._
import cats.syntax.all._
import domain.cars.{CarError, CarHttpErrorHandler}
import org.http4s.headers.`Content-Type`
import org.http4s.multipart.{Multipart, Part}

import java.net.URL
import java.util.concurrent.Executors
import scala.concurrent.{ExecutionContext, ExecutionContextExecutorService}
import scala.util.Random
import cats.mtl.Handle.handleForApplicativeError

import java.io.File
import stores.firestore.FireLocationStore
import services.StorageService

class CarEndpointsSpec extends AnyFlatSpec
  with should.Matchers
  with Http4sDsl[IO]
  with Http4sClientDsl[IO]
  with LoginTest {
  val customBlockingEC: ExecutionContextExecutorService =
    ExecutionContext.fromExecutorService(
      Executors.newCachedThreadPool((r: Runnable) => {
        val t = new Thread(r)
        t.setName(s"custom-blocking-ec-${t.getName}")
        t
      })
    )
  implicit val blocker: Blocker = Blocker.liftExecutionContext(customBlockingEC)
  import scala.concurrent.ExecutionContext.Implicits.global
  implicit val cs: ContextShift[IO] = IO.contextShift(global)

  def routes(inmemory: Boolean = false): HttpApp[IO] = {
    implicit def carHttpErrorHandler: HttpErrorHandler[IO, CarError] = new CarHttpErrorHandler[IO]
    implicit def unsafeLogger[F[_]: Sync]: SelfAwareStructuredLogger[F] =
      Slf4jLogger.getLoggerFromName[F](name = "debugLogger")

    val db = FirestoreDB.instance()
    val carBucket = FirestoreDB.bucket
    val userStore = if (inmemory) InmemoryUserStore[IO] else new FireUserStore[IO](db,FirestoreDB.secureBucket ,blocker)
    val authStore = FireAuthStore[IO, SecureRandomId, UserID](
      db, s => SecureRandomId.coerce(s.id))

    //    if (inmemory)
    //      AuthMemoryStore[IO, UserID, TSecBearerToken](s => SecureRandomId.coerce(s.id))

    val auth = AuthHelpers.bearerTokenAuthenticator[IO, UserID](authStore, userStore)

    val securedRequestHandler = SecuredRequestHandler(auth)
    val endpoint = CarEndpoints.endpoints(
      FireCarStore[IO](db),
      new FireTripStore[IO](db),
      new FireLocationStore[IO](db),
      new StorageService[IO](carBucket, blocker),
      securedRequestHandler,
    )

    Router(("/cars", endpoint)).orNotFound
  }

  private val token = "Bearer 3b7b8b629c14cbf2afe197d218ab46bada92a91dcb7ba2277104743acd85ee19"
  private val adminAuthHeader = Header("Authorization", token)

  "CarEndpoints" should "upload photos" in {
    val endpoint = routes()

    val image: IO[File] = IO(new File("car.jpeg"))

    def multipart(file: File): Multipart[IO] = Multipart[IO](
      Vector(
        Part.fileData("car", file, blocker)
      )
    )

    val expectedURL = "https://www.googleapis.com/storage/v1/b/images_carrentals/o/asdf%2F" + "car.jpeg"
    val request =
      for {
        body <- image.map(multipart)
        req  <- POST(body, uri"/cars/uploadPhotos/asdf")
      } yield req.withHeaders(body.headers.put(adminAuthHeader))

    (for {
      req <- request
      resp <- endpoint.run(req)
      links <- resp.as[List[String]]
    } yield {
      resp.status shouldEqual Ok
      links should contain (expectedURL)
    }).unsafeRunSync()
  }

}
