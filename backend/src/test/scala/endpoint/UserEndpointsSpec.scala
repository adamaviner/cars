package endpoint

import cats.effect.IO
import domain.users.{User, UserError, UserHttpErrorHandler, UserStore, UserUpdate, UserValidator}
import org.http4s.{HttpApp, Uri}
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import tsec.authentication.{Authenticator, BearerTokenAuthenticator, JWTAuthenticator, SecuredRequestHandler, TSecBearerToken}
import tsec.mac.jca.HMACSHA256
import tsec.passwordhashers.jca.BCrypt

import scala.concurrent.duration._
import cats.syntax.all._
import cats.effect._
import domain.auth.{AuthHelpers, LoginRequest, SignupRequest}
import domain.users.UserModel.UserID
import org.http4s.implicits._
import org.http4s.circe.CirceEntityCodec._
import io.circe.syntax._
import io.circe.generic.auto._
import org.http4s.circe._
import org.typelevel.log4cats.SelfAwareStructuredLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import stores.firestore.{FireAuthStore, FireTokenStore, FireUserStore, FirestoreDB}
import stores.inmemory.{AuthMemoryStore, InmemoryUserStore}
import tsec.common.SecureRandomId

import scala.util.Random
import com.olegpy.meow.hierarchy._
import cats.mtl.Handle.handleForApplicativeError

import java.util.concurrent.Executors
import scala.concurrent.{ExecutionContext, ExecutionContextExecutorService}
import org.http4s.client.blaze.BlazeClientBuilder
import services.HumanVerificationService
import org.http4s.client.JavaNetClientBuilder


class UserEndpointsSpec extends AnyFlatSpec
  with should.Matchers
  with Http4sDsl[IO]
  with Http4sClientDsl[IO]
  with LoginTest {

  val customBlockingEC: ExecutionContextExecutorService =
    ExecutionContext.fromExecutorService(
      Executors.newCachedThreadPool((r: Runnable) => {
        val t = new Thread(r)
        t.setName(s"custom-blocking-ec-${t.getName}")
        t
      })
    )
  implicit val blocker: Blocker = Blocker.liftExecutionContext(customBlockingEC)

  import scala.concurrent.ExecutionContext.Implicits.global

  implicit val cs: ContextShift[IO] = IO.contextShift(global)

//  def userRoutes(inmemory: Boolean = false): HttpApp[IO] = {
//    implicit def userHttpErrorHandler: HttpErrorHandler[IO, UserError] = new UserHttpErrorHandler[IO]
//
//    implicit def unsafeLogger[F[_] : Sync]: SelfAwareStructuredLogger[F] =
//      Slf4jLogger.getLoggerFromName[F](name = "debugLogger")
//
//    val db = FirestoreDB.instance()
//    val userStore = new FireUserStore[IO](db, FirestoreDB.secureBucket, blocker)
//    val authStore = FireAuthStore[IO, SecureRandomId, UserID](
//      db, s => SecureRandomId.coerce(s.id))
//    val tokenStore = new FireTokenStore[IO](db)
//
//    val client = JavaNetClientBuilder[IO](blocker).create
//    val humanVerificationService = new HumanVerificationService[IO](client)
//    val userValidator = new UserValidator[IO](humanVerificationService)
//
//    //    if (inmemory)
//    //      AuthMemoryStore[IO, UserID, TSecBearerToken](s => SecureRandomId.coerce(s.id))
//
//    val auth = AuthHelpers.bearerTokenAuthenticator[IO, UserID](authStore, userStore)
//
//    val securedRequestHandler = SecuredRequestHandler(auth)
//
//    val usersEndpoint = UserEndpoints.endpoints(
//      userStore,
//      userValidator,
//      tokenStore,
//      BCrypt.syncPasswordHasher[IO],
//      securedRequestHandler,
//    )
//
//    Router(("/users", usersEndpoint)).orNotFound
//  }
//
//  private def signupRequest = SignupRequest(Random.nextString(6) + "@gmail", "pass", "0525782231", "Clan", "Cularia", "2018-05-05")
//
//  private def adminLoginRequest = LoginRequest("adamaviner@gmail.com", "unclefucker")
//
//  "UserEndpoints" should "create user and log in" ignore {
//    val userEndpoint = userRoutes()
//
//    //    val (_, auth) = signUpAndLogInAsUser(signupRequest, userEndpoint).unsafeRunSync()
//    //    auth shouldBe defined
//  }
//
//  ignore should "route authed user" in {
//    val userEndpoint = userRoutes()
//
//    (for {
//      loginResp <- signUpAndLogInAsUser(signupRequest, userEndpoint)
//      (_, auth) = loginResp
//      verifyReq <- POST("hello", uri"/users/sms/verify")
//      verifyReqAuth = verifyReq.putHeaders(auth.get)
//      resp <- userEndpoint.run(verifyReqAuth)
//      adminReq <- POST("hello", uri"/users/admin/userid")
//      adminReqAuth = adminReq.putHeaders(auth.get)
//      adminResp <- userEndpoint.run(adminReqAuth)
//    } yield {
//      resp.status shouldEqual NotImplemented
//      adminResp.status shouldEqual Unauthorized
//    }).unsafeRunSync()
//  }
//
//  it should "stream license image" in {
//    val userEndpoint = userRoutes()
//    val idOfUserWithLicense = "2rgKH1OdBjpSQbKZgIT5"
//
//    (for {
//      loginResp <- login(adminLoginRequest, userEndpoint)
//      (_, auth) = loginResp
//      req <- GET(uri"/users/admin/license/2rgKH1OdBjpSQbKZgIT5")
//      reqAuth = req.putHeaders(auth.get)
//      resp <- userEndpoint.run(reqAuth)
//    } yield {
//      resp.status shouldEqual Ok
//      resp.bodyText.compile.lastOrError.unsafeRunSync() shouldEqual "test"
//    }).unsafeRunSync()
//  }
//
//  ignore should "route authed admin" in {
//    val userEndpoint = userRoutes()
//
//    (for {
//      loginResp <- login(adminLoginRequest, userEndpoint)
//      (_, auth) = loginResp
//      verifyReq <- POST("hello", uri"/users/sms/verify")
//      verifyReqAuth = verifyReq.putHeaders(auth.get)
//      resp <- userEndpoint.run(verifyReqAuth)
//      adminReq <- PUT("hello", uri"/users/admin/adsf")
//      adminReqAuth = adminReq.putHeaders(auth.get)
//      adminResp <- userEndpoint.run(adminReqAuth)
//    } yield {
//      resp.status shouldEqual NotImplemented
//      adminResp.status shouldEqual NotFound
//    }).unsafeRunSync()
//  }
//
//  ignore should "update user" in {
//    val userEndpoint = userRoutes()
//
//    (for {
//      loginResp <- signUpAndLogInAsUser(signupRequest, userEndpoint)
//      (createdUser, auth) = loginResp
//      userToUpdate = UserUpdate(id = createdUser.id, lastName = Some(createdUser.lastName.reverse))
//      updateUser <- PUT(userToUpdate, uri"/users/update")
//      updateUserAuth = updateUser.putHeaders(auth.get)
//      resp <- userEndpoint.run(updateUserAuth)
//      updatedUser <- resp.as[User]
//    } yield {
//      resp.status shouldEqual Ok
//      updatedUser.lastName shouldEqual createdUser.lastName.reverse
//      createdUser.id shouldEqual updatedUser.id
//    }).unsafeRunSync()
//  }
//
//  ignore should "verify user sms token" in {
//    val userEndpoint = userRoutes()
//
//    (for {
//      loginResp <- signUpAndLogInAsUser(signupRequest, userEndpoint)
//      (createdUser, auth) = loginResp
//      verifyReq <- POST("hello", uri"/users/sms/verify")
//      verifyReqAuth = verifyReq.putHeaders(auth.get)
//      resp <- userEndpoint.run(verifyReqAuth)
//    } yield {
//      resp.status shouldEqual NotImplemented
//    }).unsafeRunSync()
//  }
}
