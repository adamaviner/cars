// package stores

// import cats._
// import cats.data._
// import cats.syntax.all._
// import cats.effect.{Blocker, ContextShift, IO, Resource, Sync}
// import cats.mtl.Handle.handleForApplicativeError
// import com.google.auth.oauth2.GoogleCredentials
// import com.google.cloud.firestore.Firestore
// import com.google.firebase.cloud.FirestoreClient
// import com.google.firebase.{FirebaseApp, FirebaseOptions}
// import com.olegpy.meow.hierarchy._
// import domain.auth.AuthHelpers
// import domain.trips.{CarIsReservedForDate, Trip, TripRequest}
// import domain.users.UserModel.UserID
// import org.scalatest.Succeeded
// import org.scalatest.flatspec.AnyFlatSpec
// import org.scalatest.matchers.should
// import org.typelevel.log4cats.SelfAwareStructuredLogger
// import org.typelevel.log4cats.slf4j.Slf4jLogger
// import stores.firestore.{FireAuthStore, FireTripStore, FireUserStore, FirestoreDB}
// import tsec.authentication.SecuredRequestHandler
// import tsec.common.SecureRandomId

// import java.io.FileInputStream
// import scala.concurrent.ExecutionContext
// import java.time.{Duration, Instant, ZoneId, ZonedDateTime}
// import java.time.temporal.{ChronoUnit, TemporalAmount}

// class FireTripSpec extends AnyFlatSpec with should.Matchers {
//   implicit lazy val testEc: ExecutionContext = ExecutionContext.Implicits.global
//   implicit val contextShiftIO: ContextShift[IO] = IO.contextShift(testEc)

//   private val db = try {
//     FirebaseApp.initializeApp()
//     FirestoreClient.getFirestore()
//   } catch {
//     case e: IllegalStateException => FirestoreClient.getFirestore()
//   }

//   implicit def unsafeLogger[F[_] : Sync]: SelfAwareStructuredLogger[F] =
//     Slf4jLogger.getLoggerFromName[F](name = "debugLogger")

//   val tripStore = new FireTripStore[IO](db)
//   val runAll = false

//   "FireTripStore" should "add trip from request" ignore {
//     assume(runAll)
//     val now = Instant.now().getEpochSecond
//     val trip = Trip(
//       startDate = now,
//       endDate = Instant.now().plusSeconds(60 * 60 * 24 * 5).getEpochSecond)

//     val action = for {
//       tripWithId <- tripStore.add(trip)
//       _ <- tripStore.delete(tripWithId.id)
//     } yield {
//       tripWithId.id should not be ""
//       tripWithId.startDate shouldBe now
//     }

//     action.handleErrorWith {
//       e: Throwable => {
//         println("Oh no! " + e)
//         fail(e)
//       }
//     }.unsafeRunSync()
//   }

//   it should "not be able to schedule 2 trips at same time" ignore {
//     assume(runAll)
//     val now = Instant.now()
//     val trip = TripRequest(vin = "vin",
//       startDate = now.getEpochSecond,
//       endDate = now.plusSeconds(1).getEpochSecond)
//     val conflictingTrip = trip.copy(startDate = now.minusSeconds(1).getEpochSecond)

//     val action = tripStore.schedule(trip).flatMap(tripRes =>
//       tripStore.schedule(conflictingTrip).handleErrorWith {
//         case CarIsReservedForDate(msg) => {
//           tripStore.delete(tripRes.id) *>
//             IO.raiseError(CarIsReservedForDate(msg))
//         }
//       }
//     )

//     assertThrows[CarIsReservedForDate] {
//       action.unsafeRunSync()
//     }
//   }

//   it should "be able to schedule 2 trips at same time when one is not active" ignore {
//     assume(runAll)
//     val now = Instant.now()
//     val trip = TripRequest(vin = "vin",
//       startDate = now.getEpochSecond,
//       endDate = now.plusSeconds(1).getEpochSecond)
//     val conflictingTrip = trip.copy(startDate = now.minusSeconds(1).getEpochSecond)

//     val action = tripStore.schedule(trip).flatMap(tripRes => {
//       for {
//         _ <- tripStore.markInactive(List(tripRes.id))
//         conflictingDateTrip <- tripStore.schedule(conflictingTrip)
//       } yield {
//         tripStore.delete(List(tripRes.id, conflictingDateTrip.id))
//       }
//     }
//     )

//     action.flatten.unsafeRunSync()
//   }

//   private def tryToScheduleBoth(trip: TripRequest, trip2: TripRequest): IO[Unit] = {
//     tripStore.schedule(trip).flatMap(tripRes =>
//       tripStore.schedule(trip2).handleErrorWith {
//         case CarIsReservedForDate(msg) => {
//           tripStore.delete(tripRes.id) *>
//             IO.raiseError(CarIsReservedForDate(msg))
//         }
//       }.flatMap(trip2Res => tripStore.delete(List(tripRes.id, trip2Res.id)))
//     )
//   }

//   private def tryToScheduleAndUpdate(trip: TripRequest, endDate: Long): IO[Unit] = {
//     tripStore.schedule(trip).flatMap(tripRes =>
//       tripStore.update(tripRes.copy(endDate = endDate)).handleErrorWith {
//         case CarIsReservedForDate(msg) => {
//           tripStore.delete(tripRes.id) *>
//             IO.raiseError(CarIsReservedForDate(msg))
//         }
//       }.flatMap(trip2Res => {
//         trip2Res.id shouldEqual tripRes.id
//         trip2Res.endDate shouldEqual endDate
//         tripStore.delete(List(tripRes.id))
//       })
//     )
//   }

//   import scala.concurrent.duration.FiniteDuration
//   import scala.concurrent.duration._

//   implicit class TemporalAmountConversion(duration: FiniteDuration) {
//     def asJava: java.time.Duration = java.time.Duration.of(duration.toMillis, ChronoUnit.MILLIS)
//   }

//   private val zoneId = ZoneId.of("America/Los_Angeles") //TODO hardcoded Los Angeles as cars don't have other locations yet
//   private val businessHourZoned = ZonedDateTime.of(2021, 1, 1, 10, 0, 0, 0, zoneId)
//   private val businessHour = Instant.from(businessHourZoned)

//   it should "fail with conflict if end date of previous leaves no time for turnover" ignore {
// //    assume(runAll)
//     val trip = TripRequest(vin = "vin",
//       startDate = businessHour.getEpochSecond,
//       endDate = businessHour.plusSeconds(1).getEpochSecond)

//     val startDate = businessHour.minus(48.hours.asJava)
//     // end's in non business hour and leaves less than 24 hours
//     val nonBusinessEndDate = businessHour.minus(13.hour.asJava)
//     val conflictingTrip = trip.copy(
//       startDate = startDate.getEpochSecond,
//       endDate = nonBusinessEndDate.getEpochSecond
//     )

//     val nonConflictingTrip = conflictingTrip.copy(
//       endDate = businessHour.minus(23.hour.asJava).getEpochSecond
//     )

//     val nonConflictingTrip2 = conflictingTrip.copy(
//       endDate = businessHour.minus(25.hour.asJava).getEpochSecond
//     )

//     assertThrows[CarIsReservedForDate] {
//       tryToScheduleBoth(trip, conflictingTrip).unsafeRunSync()
//     }

//     assertThrows[CarIsReservedForDate] {
//       tryToScheduleBoth(conflictingTrip, trip).unsafeRunSync()
//     }

//     tryToScheduleBoth(trip, nonConflictingTrip).unsafeRunSync()
//     tryToScheduleBoth(trip, nonConflictingTrip2).unsafeRunSync()
//   }

//   it should "fail with conflict if start date of previous leaves no time for turnover" ignore {
// //        assume(runAll)
//     val trip = TripRequest(vin = "vin",
//       startDate = businessHour.getEpochSecond,
//       endDate = businessHour.plusSeconds(1).getEpochSecond)

//     val startDate = businessHour.plus(11.hour.asJava)
//     // end's in non business hour and leaves less than 24 hours
//     val nonBusinessEndDate = businessHour.plus(48.hour.asJava)
//     val conflictingTrip = trip.copy(
//       startDate = startDate.getEpochSecond,
//       endDate = nonBusinessEndDate.getEpochSecond
//     )

//     val nonConflictingTrip = conflictingTrip.copy(
//       startDate = businessHour.plus(13.hour.asJava).getEpochSecond
//     )

//     assertThrows[CarIsReservedForDate] {
//       tryToScheduleBoth(trip, conflictingTrip).unsafeRunSync()
//     }

//     assertThrows[CarIsReservedForDate] {
//       tryToScheduleBoth(conflictingTrip, trip).unsafeRunSync()
//     }

//     tryToScheduleBoth(trip, nonConflictingTrip).unsafeRunSync()
//     tryToScheduleBoth(nonConflictingTrip, trip).unsafeRunSync()

//     tryToScheduleAndUpdate(trip, trip.endDate + 60)
//   }
// }
