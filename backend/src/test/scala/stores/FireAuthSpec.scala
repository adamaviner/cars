package stores

import cats.data.EitherT
import cats.effect.{Async, Blocker, ContextShift, IO, Resource, Sync}
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.Timestamp
import com.google.cloud.firestore.Firestore
import com.google.firebase.{FirebaseApp, FirebaseOptions}
import com.google.firebase.cloud.FirestoreClient
import domain.auth.AuthHelpers
import domain.users.User
import domain.users.UserModel.UserID
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import stores.firestore.FirestoreIO.{ApiFutureDecorator, FS}
import stores.firestore.{FireAuthStore, FireUserStore, FirestoreDB}
import tsec.authentication.{AugmentedJWT, SecuredRequestHandler}
import tsec.common.SecureRandomId
import tsec.mac.jca.HMACSHA256

import java.util.Calendar
import scala.beans.BeanProperty
import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.global
import stores.firestore.FirestoreIO._
import tsec.jws.mac.JWTMac
import tsec.jwt.JWTClaims

import java.io.FileInputStream
import java.time.Instant
import scala.concurrent.duration.FiniteDuration
import com.olegpy.meow.hierarchy._
import cats.mtl.Handle.handleForApplicativeError
import org.typelevel.log4cats.SelfAwareStructuredLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger


class FireAuthSpec extends AnyFlatSpec with should.Matchers {
  implicit lazy val testEc: ExecutionContext = ExecutionContext.Implicits.global
  implicit val contextShiftIO: ContextShift[IO] = IO.contextShift(testEc)

  private val dbResource: Resource[IO, Firestore] = FirestoreDB[IO]()

  def db: Firestore = {
    val serviceAccountPath = "src/main/resources/serviceAccount.json"
    val fis = new FileInputStream(serviceAccountPath)
    val credentials = GoogleCredentials.fromStream(fis)

    val options = FirebaseOptions.builder()
      .setCredentials(credentials)
      .build()

    try {
      FirebaseApp.initializeApp(options)
      FirestoreClient.getFirestore()
    } catch {
      case e: IllegalStateException => FirestoreClient.getFirestore()
    }
  }
  implicit def unsafeLogger[F[_]: Sync]: SelfAwareStructuredLogger[F] =
    Slf4jLogger.getLoggerFromName[F](name = "debugLogger")

  val userStore = new FireUserStore[IO](db, FirestoreDB.secureBucket, Blocker.liftExecutionContext(testEc))

  //  "FireAuth" should "init DB" in {
  //    dbResource.use { db =>
  //      val _ = AuthStore[IO, SecureRandomId, TestIdentity](
  //        db, s => SecureRandomId.coerce(s.id))
  //      IO.unit
  //    }.unsafeRunSync()
  //  }

  "FireAuth" should "put valid auth" in {
    //    dbResource.use { db =>
    val store = FireAuthStore[IO, SecureRandomId, UserID](
      db, s => s.id)
    val authenticator = AuthHelpers.bearerTokenAuthenticator[IO, UserID](store, userStore)
    val auth = SecuredRequestHandler(authenticator).authenticator

    val action = for {
      token <- auth.create(SecureRandomId.Strong.generate)
      write <- store.put(token)
    } yield write

    action.handleErrorWith {
      e: Throwable => {
        println("Oh no! " + e)
        fail(e)
      }
    }.unsafeRunSync()
  }

  it should "find existing Auth" in {
    val store = FireAuthStore[IO, SecureRandomId, UserID](
      db, s => s.id)
    val authenticator = AuthHelpers.bearerTokenAuthenticator[IO, UserID](store, userStore)
    val auth = SecuredRequestHandler(authenticator).authenticator

    val tokenID = SecureRandomId.coerce("7b9641d0345630fa12b8903e36ca5d1240c00b7383e459ed0d8970692436b151")
    store.get(tokenID).value.map { token =>
      token should not be empty
    }
    val action = for {
      token <- store.get(tokenID).value
    } yield token

    action.handleErrorWith {
      e: Throwable => {
        println("Oh no! " + e)
        fail(e)
      }
    }.unsafeRunSync()
  }
}
