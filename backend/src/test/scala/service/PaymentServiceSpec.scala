// package service

// import cats._
// import cats.data._
// import cats.syntax.all._
// import utils.TimeUtils.{DurationInt, PeriodInt}
// import cats.effect._
// import cats.mtl.Handle.handleForApplicativeError
// import com.olegpy.meow.hierarchy._
// import domain.auth.AuthHelpers
// import domain.cars.{Car, CarError, CarHttpErrorHandler}
// import domain.payment.{DeferredPayment, Payment}
// import domain.trips.{Trip, TripRequest}
// import domain.users.UserModel.UserID
// import io.circe.generic.auto._
// import org.http4s.circe.CirceEntityCodec._
// import org.http4s.client.dsl.Http4sClientDsl
// import org.http4s.dsl.Http4sDsl
// import org.http4s.implicits._
// import org.http4s.multipart.{Multipart, Part}
// import org.http4s.{Header, HttpApp}
// import org.scalatest.{Assertion, BeforeAndAfter}
// import org.scalatest.flatspec.AnyFlatSpec
// import org.scalatest.matchers.should
// import org.typelevel.log4cats.{Logger, SelfAwareStructuredLogger}
// import org.typelevel.log4cats.slf4j.Slf4jLogger
// import services.{PaymentService, StripeService}
// import stores.firestore._
// import stores.inmemory.InmemoryUserStore
// import tsec.authentication.SecuredRequestHandler
// import tsec.common.SecureRandomId

// import java.io.File
// import java.time
// import java.time.Clock.systemUTC
// import java.time.{Instant, ZoneId}
// import java.time.temporal.ChronoUnit
// import java.util.concurrent.Executors
// import scala.concurrent.{ExecutionContext, ExecutionContextExecutorService}
// import scala.util.Random

// case class App(
//     tripStore: FireTripStore[IO],
//     userStore: FireUserStore[IO],
//     carStore: FireCarStore[IO],
//     paymentStore: FirePaymentStore[IO],
//     paymentService: PaymentService[IO],
//     stripeService: StripeService[IO]
// )

// // FIXME PaymentService doesn't take a clock so it can't be mocked
// class PaymentServiceSpec extends AnyFlatSpec with BeforeAndAfter with should.Matchers {
//   val customBlockingEC: ExecutionContextExecutorService =
//     ExecutionContext.fromExecutorService(
//       Executors.newCachedThreadPool((r: Runnable) => {
//         val t = new Thread(r)
//         t.setName(s"custom-blocking-ec-${t.getName}")
//         t
//       })
//     )
//   implicit val blocker: Blocker = Blocker.liftExecutionContext(customBlockingEC)
//   import scala.concurrent.ExecutionContext.Implicits.global
//   implicit val cs: ContextShift[IO] = IO.contextShift(global)

//   implicit def unsafeLogger[F[_]: Sync]: SelfAwareStructuredLogger[F] =
//     Slf4jLogger.getLoggerFromName[F](name = "debugLogger")

//   def initApp(clock: time.Clock = systemUTC): App = {

//     val db = FirestoreDB.instance()
//     val userStore = new FireUserStore[IO](db, FirestoreDB.secureBucket, blocker)
//     val tripStore = new FireTripStore[IO](db)
//     val carStore = new FireCarStore[IO](db)
//     val paymentStore = new FirePaymentStore[IO](db, clock)

//     val stripeService = new StripeService[IO](blocker)
//     val paymentService = new PaymentService[IO](stripeService, paymentStore, tripStore, userStore, carStore)
//     App(tripStore, userStore, carStore, paymentStore, paymentService, stripeService)
//   }

//   def testCar: Car =
//     Car("testVin." + Random.nextLong(1000), price = 10)

//   val adminUserID = "cCRwgCasKN5rYJLnfSeL"

//   def clean(
//       app: App,
//       car: Car,
//       trip: Trip,
//       payments: List[Payment],
//       deferred: List[DeferredPayment]
//   ): IO[Unit] = {
//     for {
//       _ <- app.carStore.delete(car.vin)
//       _ <- app.tripStore.delete(trip.id)
//       _ <- payments.parTraverse(payment => app.paymentStore.delete(payment))
//       _ <- deferred.parTraverse(deferred => app.paymentStore.deleteDeferred(deferred))
//     } yield {}
//   }

//   after {
//     clean()
//   }

//   def clean(): Unit = {
//     val app = initApp()
//     val action = for {
//       cars <- app.carStore.list(1000, 0)
//       testCars = cars.filter(_.vin.contains("testVin"))
//       _ <- testCars.parTraverse(car => app.carStore.delete(car.vin))
//       trips <- app.tripStore.list(1000, 0)
//       testTripIds = trips
//         .filter(_.userID == adminUserID)
//         .filter(_.vin.contains("testVin."))
//         .map(_.id)

//       _ <- Logger[IO].debug(s"testTrips: $testTripIds")
//       _ <- testTripIds.parTraverse(tripID => app.tripStore.delete(tripID))
//       userPayments <- app.paymentStore.userPayments(adminUserID)
//       userDefers <- app.paymentStore.userDeferred(adminUserID)
//       testPayments = testTripIds.flatMap(tripID => userPayments.filter(_.tripID == tripID))
//       testDefers = testTripIds.flatMap(tripID => userDefers.filter(_.tripID == tripID))

//       _ <- Logger[IO].debug(s"testPayments: $testPayments")
//       _ <- testPayments.parTraverse(payment => app.paymentStore.delete(payment))
//       _ <- testDefers.parTraverse(d => app.paymentStore.deleteDeferred(d))
//     } yield {}

//     action.unsafeRunSync()
//   }

//   "PaymentService" should "charge now when less then 1 day to trip" in {
//     clean()
//     val immediateChargeStartDate = Instant.now.plus(23.hours).getEpochSecond
//     val endDate = Instant.now.plus(5.days).getEpochSecond
//     testDates(immediateChargeStartDate, endDate, expectChargeNow = true, expectDeposit = false)
//   }

//   ignore should "create deferred when more than 1 day to trip" in {
//     val deferredChargeStartDate = Instant.now.plus(2.days).getEpochSecond
//     val endDate = Instant.now.plus(5.days).getEpochSecond
//     testDates(deferredChargeStartDate, endDate, expectChargeNow = false, expectDeposit = false)
//   }

//   ignore should "reap deferred charges" in {
//     val deferredChargeStartDate = Instant.now.plus(2.days).getEpochSecond
//     val endDate = Instant.now.plus(5.days).getEpochSecond
//     testDates(deferredChargeStartDate, endDate, expectChargeNow = false, expectDeposit = false)

//     val app = initApp(time.Clock.fixed(Instant.now.plus(1.days).plus(1.second), ZoneId.systemDefault()))
//     app.paymentService.reapDeferredCharges().map(payments => {
//       val testPayments = payments.filter(_.userID == adminUserID)
//       testPayments should have length 2
//     }).unsafeRunSync()
//   }

//   ignore should "create deposit when not enough time to capture a hold" in {
//     val maxHoldDays = 7.days
//     val immediateChargeStartDate = Instant.now.plus(23.hours).getEpochSecond
//     val endDate = Instant.now.plus(maxHoldDays).getEpochSecond
//     testDates(immediateChargeStartDate, endDate, expectChargeNow = true, expectDeposit = true)
//   }


//   def testDates(startDate: Long, endDate: Long, expectChargeNow: Boolean, expectDeposit: Boolean): Unit = {
//     val app = initApp()
//     val depositAmount = 300000

//     val action = for {
//       user <- app.userStore.doGet(adminUserID)
//       car <- app.carStore.add(testCar)
//       trip <- app.tripStore.schedule(
//         tripRequestWithPrice(car, adminUserID, startDate, endDate)
//       )
//       _ <- Logger[IO].debug(s"trip: $trip")
//       depositedNow <- app.paymentService.scheduleHoldOrDeposit(user, trip, depositAmount)
//       chargedNow <- app.paymentService.scheduleCharge(user, trip)
//       payments <- app.paymentStore.userPayments(user.id)
//       tripPayments = payments.filter(_.tripID == trip.id)
//       _ <- Logger[IO].debug(s"tripPayments: $tripPayments")

//       deferredPayments <- app.paymentStore.userDeferred(user.id)
//       tripDeferredPayments = deferredPayments.filter(_.tripID == trip.id)
//     } yield {
// //      clean(app, car, trip, tripPayments, tripDeferredPayments)
//       depositedNow shouldBe expectChargeNow
//       chargedNow shouldBe expectChargeNow

//       if (expectChargeNow) {
//         tripDeferredPayments should have length 0
//         tripPayments.length shouldBe 2

//         val chargeOpt = tripPayments.find(p => !p.held && !p.deposited)
//         chargeOpt should not be empty
//         val charge = chargeOpt.get
//         charge.charged shouldBe expectChargeNow
//         app.stripeService
//           .retrievePayment(charge.paymentID)
//           .map(paymentIntent => {
//             paymentIntent.getStatus shouldBe "succeeded"
//             paymentIntent.getAmountReceived shouldBe trip.price
//           })

//         if (expectDeposit) {
//           val deposit = tripPayments.find(_.deposited)
//           deposit should not be empty
//           deposit.get.charged shouldBe true
//           app.stripeService
//             .retrievePayment(deposit.get.paymentID)
//             .map(paymentIntent => {
//               paymentIntent.getStatus shouldBe "succeeded"
//               paymentIntent.getAmountReceived shouldBe depositAmount
//             })
//         } else {
//           val hold = tripPayments.find(_.held)
//           hold should not be empty
//         }
//       } else {
//         tripDeferredPayments should have length 2
//       }
//     }

//     action
// //      .handleErrorWith { e: Throwable =>
// //        {
// //          println("Oh no! " + e)
// //          fail(e)
// //        }
// //      }
//       .flatMap(_ => IO.unit)
//       .unsafeRunSync()
//   }

//   def tripRequestWithPrice(
//       car: Car,
//       userID: UserID,
//       startDate: Long,
//       endDate: Long
//   ): TripRequest = {
//     val req = TripRequest(car.vin, userID, startDate, endDate)
//     req.copy(price = req.detailedPrice(car).basePrice)
//   }
// }
